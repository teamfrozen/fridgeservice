﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FridgeService.Domain
{
    [Table("Product_In_Fridge")]
    public class ProductInFridge
    {
        [Key]
        public Guid ProductInFridgeId { get; set; }
        public Guid FridgeId { get; set; }
        public Fridge Fridge { get; set; }
        public Guid ProductId { get; set; }
        public Product Product { get; set; }
        public DateTime DepositDate { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}
