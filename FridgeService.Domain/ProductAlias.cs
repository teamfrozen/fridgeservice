﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FridgeService.Domain
{
    [Table("Product_Alias")]
    public class ProductAlias
    {
        [Key]
        public Guid AliasId { get; set; }
        public string Name { get; set; }
        public Guid ProductTypeId { get; set; }
        public ProductType ProductType { get; set; }
    }
}
