﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FridgeService.Domain
{
    [Table("Product")]
    public class Product
    {
        [Key]
        public Guid ProductId { get; set; }
        public string Name { get; set; }
        public Guid? ProductTypeId { get; set; }
        public ProductType ProductType { get; set; }
        public double? Quantity { get; set; }
        public long? ShelfLifeDuration { get; set; }
        public List<ProductInFridge> ProductInFridges { get; set; }
        public List<UserFavoriteProduct> UserFavorites { get; set; }
    }
}
