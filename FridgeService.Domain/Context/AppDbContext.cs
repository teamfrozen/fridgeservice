﻿using Microsoft.EntityFrameworkCore;

namespace FridgeService.Domain.Context
{
    public class AppDbContext : DbContext, IAppDbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }
        public AppDbContext()
        {
        }
        public DbSet<Fridge> Fridges { get; set; }
        public DbSet<Measure> Measures { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductAlias> ProductAliases { get; set; }
        public DbSet<ProductInFridge> ProductsInFridge { get; set; }
        public DbSet<ProductType> ProductTypes { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserFavoriteProduct> FavoriteProducts { get; set; }
    }
}
