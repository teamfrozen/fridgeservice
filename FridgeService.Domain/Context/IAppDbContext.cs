﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.Domain.Context
{
    public interface IAppDbContext
    {
        public DatabaseFacade Database { get; }
        DbSet<Fridge> Fridges { get; set; }
        DbSet<Measure> Measures { get; set; }
        DbSet<ProductAlias> ProductAliases { get; set; }
        DbSet<Product> Products { get; set; }
        DbSet<ProductInFridge> ProductsInFridge { get; set; }
        DbSet<ProductType> ProductTypes { get; set; }
        DbSet<UserFavoriteProduct> FavoriteProducts { get; set; }
        DbSet<User> Users { get; set; }
        Task<int> SaveChangesAsync(CancellationToken token = default);
        EntityEntry<TEntity> Entry<TEntity>(TEntity entity)
            where TEntity : class;
    }
}