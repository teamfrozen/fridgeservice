﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FridgeService.Domain
{
    [Table("Product_Type")]
    public class ProductType
    {
        [Key]
        public Guid ProductTypeId { get; set; }
        public string Name { get; set; }
        public long? AvgShelfLifeDuration { get; set; }
        public Guid? MeasureId { get; set; }
        public Measure Measure { get; set; }
        public string ProductImagePath { get; set; }
        public List<ProductAlias> ProductAliases { get; set; }
        public List<Product> Products { get; set; }
    }
}
