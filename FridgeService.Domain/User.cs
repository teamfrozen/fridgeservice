﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FridgeService.Domain
{
    [Table("User")]
    public class User
    {
        [Key]
        public Guid UserId { get; set; }
        public Guid FridgeId { get; set; }
        public Fridge Fridge { get; set; }
        public List<UserFavoriteProduct> FavoriteProducts { get; set; }
    }
}
