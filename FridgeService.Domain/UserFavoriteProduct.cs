﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FridgeService.Domain
{
    [Table("Favorite_Product")]
    public class UserFavoriteProduct
    {
        [Key]
        public Guid UserProductId { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
        public Guid ProductId { get; set; }
        public Product Product { get; set; }
    }
}
