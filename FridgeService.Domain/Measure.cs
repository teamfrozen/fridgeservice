﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FridgeService.Domain
{
    [Table("Measure")]
    public class Measure
    {
        [Key]
        public Guid MeasureId { get; set; }
        public string Name { get; set; }
        public Measure(string name)
        {
            Name = name;
        }
    }
}
