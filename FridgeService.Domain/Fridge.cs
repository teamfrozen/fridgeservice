﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FridgeService.Domain
{
    [Table("Fridge")]
    public class Fridge
    {
        [Key]
        public Guid FridgeId { get; set; }
        public List<User> Users { get; set; }
        public List<ProductInFridge> ProductsInFridge { get; set; } // TODO: Check if works
    }
}
