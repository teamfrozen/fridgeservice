# FridgeService ![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/teamfrozen/fridgeservice?branch=main) [![coverage report](https://gitlab.com/teamfrozen/fridgeservice/badges/main/coverage.svg)](https://gitlab.com/teamfrozen/fridgeservice/-/commits/main)

The FridgeService is responsible for all the work which is done with products in user's fridges. The interconnection with other modules is done according to REST-API. The documentation of all endpoints is available on http://localhost:8001/swagger/index.html

## Specifically, the service is responsible for:

- Binding a user to a fridge
- Adding/Removing products from user's fridge (2 ways to add product: manually and from the receipt)
- Selecting/Deselecting products as favorites
- Adding/Updating/Removing product types
- Adding/Updating/Removing products themselves
- Infering shelf life of products
- Showing info about products in fridge, products near the expiry date, favorite out-of-stock products

## Prerequisites for installing FridgeService:

- Docker (last version preferrable) with Docker-Compose tool
- OS: Mac OS, Unix or Windows 10

## The following steps must be performed to install the service and run it as a standalone app:

1. Download and unzip (or simply clone) the repository
2. Enter with the command-line the downloaded repository
3. Run 'docker-compose -f docker-compose.local.yml build'
4. Run 'docker-compose -f docker-compose.local.yml up'
5. Feel free to use. All the module configuration is presented in docker-compose.yml and appconfig.json

## The following steps must be performed to install the service and run it as a microservice of the SmartFridge system:

1. Download and unzip (or simply clone) the repository
2. Enter with the command-line the downloaded repository
3. Run 'docker-compose build'
4. Run 'docker-compose up'
5. Feel free to use. All the module configuration is presented in docker-compose.yml and appconfig.json