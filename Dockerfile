#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["FridgeService/FridgeService.WebApi.csproj", "FridgeService/"]
COPY ["FridgeService.Domain/FridgeService.Domain.csproj", "FridgeService.Domain/"]
COPY ["FridgeService.Transport/FridgeService.Transport.csproj", "FridgeService.Transport/"]
RUN dotnet restore "FridgeService/FridgeService.WebApi.csproj"
COPY . .
WORKDIR "/src/FridgeService"
RUN dotnet build "FridgeService.WebApi.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "FridgeService.WebApi.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
VOLUME ["/wwwroot/Images"]
ENTRYPOINT ["dotnet", "FridgeService.WebApi.dll"]