using FridgeService.Domain.Context;
using FridgeService.WebApi;
using FridgeService.WebApi.Domain.Fridges;
using FridgeService.WebApi.Domain.Fridges.Services;
using FridgeService.WebApi.Domain.Product_Types.Services;
using FridgeService.WebApi.Domain.Products.Services;
using FridgeService.WebApi.Options;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.Net;

namespace FridgeService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<IAppDbContext, AppDbContext>(options =>
                options.UseSnakeCaseNamingConvention()
                .UseSqlServer(Configuration.GetConnectionString("Default")));
            services.AddMemoryCache(options => options.ExpirationScanFrequency = new TimeSpan(0, 15, 0));

            services.Configure<ProductTypeSettings>(Configuration.GetSection(nameof(ProductTypeSettings)));
            services.Configure<ExpirationSettings>(Configuration.GetSection(nameof(ExpirationSettings)));

            services.AddTransient<FridgeStore>();
            services.AddTransient<IShelfLifeInferingService, ShelfLifeInferingService>();
            services.AddTransient<IProductTypeInferingService, ProductTypeInferingService>();
            services.AddTransient<IImageUploadingService, ImageUploadingService>();

            services.AddMediatR(typeof(Startup).Assembly);
            services.AddAutoMapper(typeof(MappingProfile));

            services.AddHealthChecks()
                .AddSqlServer(Configuration.GetConnectionString("Default"));

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "FridgeService", Version = "v1" });
                c.IncludeXmlComments("FridgeService.WebApi.xml");
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IAppDbContext context)
        {
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "FridgeService v1"));
            if (env.IsDevelopment())
            {
                context.Database.EnsureCreated();
            }

            app.UseExceptionHandler(errorApp =>
            {
                errorApp.Run(async context =>
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        context.Response.ContentType = "text";
                        var error = context.Features.Get<IExceptionHandlerPathFeature>()?.Error;

                        if (error != null)
                        {
                            await context.Response.WriteAsync(error.Message);
                        }
                    });
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseStaticFiles();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/health");
            });
        }
    }
}
