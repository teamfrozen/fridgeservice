﻿namespace FridgeService.WebApi.Options
{
    /// <summary>
    /// Default Expiration Period (days)
    /// </summary>
    /// <param name="DefaultExpirationTermInDays">Number of days</param>
    public sealed class ExpirationSettings
    {
        public int DefaultExpirationTermInDays { get; set; }

        public ExpirationSettings()
        {
                
        }

        public ExpirationSettings(int defaultExpirationTermInDays)
        {
            DefaultExpirationTermInDays = defaultExpirationTermInDays;
        }
    }
}
