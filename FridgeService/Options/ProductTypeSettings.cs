﻿using System;

namespace FridgeService.WebApi.Options
{
    /// <summary>
    /// Default Settings for Product Type
    /// </summary>
    /// <param name="UndefinedTypeId">Id of undefined type</param>
    /// <param name="DefaultImage">Default image path</param>
    public sealed class ProductTypeSettings
    {
        public Guid UndefinedTypeId { get; set; }
        public string DefaultImage { get; set; }

        public ProductTypeSettings()
        {
        }

        public ProductTypeSettings(Guid undefinedTypeId, string defaultImage)
        {
            UndefinedTypeId = undefinedTypeId;
            DefaultImage = defaultImage;
        }
    }
}
