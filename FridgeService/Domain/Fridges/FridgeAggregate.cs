﻿using FridgeService.Domain;
using System;
using System.Collections.Concurrent;
using System.Linq;

namespace FridgeService.WebApi.Domain.Fridges
{
    /// <summary>
    /// Aggregate representing a fridge
    /// </summary>
    public class FridgeAggregate
    {
        /// <summary>
        /// Products in the fridge
        /// </summary>
        public ConcurrentDictionary<Guid, ProductInFridge> Products { get; set; }

        /// <summary>
        /// Fridge for which aggregate is built
        /// </summary>
        public Guid FridgeId { get; set; }

        public FridgeAggregate(Fridge fridge)
        {
            FridgeId = fridge.FridgeId;
            Products = new ConcurrentDictionary<Guid, ProductInFridge>(fridge.ProductsInFridge.ToDictionary(x => x.ProductInFridgeId));
        }

        public bool TryAddProductToFridge(ProductInFridge productToBeAdded)
        {
            return Products.TryAdd(productToBeAdded.ProductInFridgeId, productToBeAdded);
        }

        public bool TryRemoveProductFromFridge(Guid productId)
        {
            return Products.TryRemove(productId, out _);
        }

        public ProductInFridge UpdateProductInfo(ProductInFridge updatedProduct)
        {
            return Products.AddOrUpdate(updatedProduct.ProductInFridgeId, updatedProduct, (k, v) => updatedProduct);
        }
    }
}
