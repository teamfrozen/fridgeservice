﻿using FridgeService.Domain;
using FridgeService.Transport.Responses;
using FridgeService.WebApi.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;

namespace FridgeService.WebApi.Domain.Fridges.Services
{
    public class ShelfLifeInferingService : IShelfLifeInferingService
    {
        private readonly ExpirationSettings expirationSettings;

        public ShelfLifeInferingService(IOptions<ExpirationSettings> expirationSettings)
        {
            this.expirationSettings = expirationSettings.Value;
        }

        public DateTime GetExpirationDate(Product product)
        {
            var expirationTerm = GetExpirationTerm(product);
            return DateTime.Today.AddTicks(expirationTerm.GetTicks());
        }

        public ExpirationTerm GetExpirationTerm(Product product)
        {
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }

            if (product.ShelfLifeDuration.HasValue)
            {
                return new ExpirationTerm(product.ShelfLifeDuration.Value);
            }
            if (product.ProductType != null && product.ProductType.AvgShelfLifeDuration.HasValue)
            {
                return new ExpirationTerm(product.ProductType.AvgShelfLifeDuration.Value);
            }

            var defaultExpirationTerm = GetDefaultExpirationTerm();
            return defaultExpirationTerm;
        }

        public ExpirationTerm GetExpirationTerm(ProductType productType)
        {
            if (productType is null)
            {
                throw new ArgumentNullException(nameof(productType));
            }

            if (productType.AvgShelfLifeDuration.HasValue)
            {
                return new ExpirationTerm(productType.AvgShelfLifeDuration.Value);
            }

            var defaultExpirationTerm = GetDefaultExpirationTerm();
            return defaultExpirationTerm;
        }

        public ExpirationTerm GetDefaultExpirationTerm()
        {
            return new ExpirationTerm(expirationSettings.DefaultExpirationTermInDays, 0);
        }
    }
}
