﻿using FridgeService.Domain;
using FridgeService.Transport.Responses;
using System;

namespace FridgeService.WebApi.Domain.Fridges.Services
{
    public interface IShelfLifeInferingService
    {
        DateTime GetExpirationDate(Product product);
        ExpirationTerm GetExpirationTerm(Product product);
        ExpirationTerm GetExpirationTerm(ProductType productType);
        ExpirationTerm GetDefaultExpirationTerm();
    }
}