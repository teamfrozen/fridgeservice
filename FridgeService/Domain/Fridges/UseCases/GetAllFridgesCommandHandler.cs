﻿using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Fridges.UseCases
{
    public class GetAllFridgesCommandHandler : IRequestHandler<GetAllFridgesCommand, IEnumerable<FridgeInfo>>
    {
        private readonly IAppDbContext context;

        public GetAllFridgesCommandHandler(IAppDbContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<FridgeInfo>> Handle(GetAllFridgesCommand request, CancellationToken cancellationToken)
        {
            return (await context.Users
                .ToListAsync())
                .GroupBy(x => x.FridgeId)
                .Select(x => new FridgeInfo() { FridgeId = x.Key, Users = x.Select(x => x.UserId) });
        }
    }
}
