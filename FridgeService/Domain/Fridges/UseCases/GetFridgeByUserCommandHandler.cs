﻿using AutoMapper;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Fridges.UseCases
{
    public class GetFridgeByUserCommandHandler : IRequestHandler<GetFridgeByUserCommand, FridgeInfo>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;

        public GetFridgeByUserCommandHandler(
            IAppDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<FridgeInfo> Handle(GetFridgeByUserCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var userFridge = await context.Users
                .Include(x => x.Fridge)
                .ThenInclude(x => x.Users)
                .FirstOrDefaultAsync(x => x.UserId == request.UserId);

            return mapper.Map<FridgeInfo>(userFridge?.Fridge);
        }
    }
}
