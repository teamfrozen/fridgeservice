﻿using AutoMapper;
using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using FridgeService.WebApi.Domain.Fridges.Services;
using FridgeService.WebApi.Domain.Products.Services;
using FridgeService.WebApi.Exceptions;
using FridgeService.WebApi.Extensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Fridges.UseCases
{
    public class AddProductManuallyToFridgeCommandHandler : IRequestHandler<AddProductManuallyToFridgeCommand, ProductInFridgeInfo>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;
        private readonly IShelfLifeInferingService shelfLifeInferingService;
        private readonly FridgeStore fridgeStore;
        private readonly IProductTypeInferingService productTypeInferingService;

        public AddProductManuallyToFridgeCommandHandler(
            IAppDbContext context,
            IMapper mapper,
            IShelfLifeInferingService shelfLifeInferingService,
            IProductTypeInferingService productTypeInferingService,
            FridgeStore fridgeStore)
        {
            this.context = context;
            this.mapper = mapper;
            this.shelfLifeInferingService = shelfLifeInferingService;
            this.productTypeInferingService = productTypeInferingService;
            this.fridgeStore = fridgeStore;
        }

        public async Task<ProductInFridgeInfo> Handle(AddProductManuallyToFridgeCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            if (request.ExpirationDate.HasValue && request.ExpirationDate < DateTime.Today)
            {
                throw new ExpirationDateEarlierThanTodayException();
            }

            var fridgeOwner = await context.Users
                .Include(x => x.FavoriteProducts)
                .FirstOrDefaultAsync(x => x.UserId == request.UserId);
            if (fridgeOwner is null)
            {
                throw new EntityDoesNotExistException<User>();
            }

            var fridge = await fridgeStore.GetAsync(fridgeOwner.FridgeId);

            var addedProduct = request.ProductId.HasValue
                ? await AddSelectedProductToFridge(request, fridge)
                : await AddManuallyTypedProductToFridge(request, fridge);

            return await PrepareOutput(fridgeOwner, addedProduct);
        }

        private async Task<ProductInFridge> AddSelectedProductToFridge(AddProductManuallyToFridgeCommand request, FridgeAggregate fridge)
        {
            var productToBeAddedToFridge = await context.Products
                .Include(x => x.ProductType)
                    .ThenInclude(x => x.Measure)
                .FirstOrDefaultAsync(x => x.ProductId == request.ProductId);

            if (productToBeAddedToFridge is null)
            {
                throw new EntityDoesNotExistException<Product>();
            }

            var productExpirationDate = request.ExpirationDate ?? shelfLifeInferingService.GetExpirationDate(productToBeAddedToFridge);

            return await fridgeStore.AddProductToFridge(fridge, productToBeAddedToFridge, productExpirationDate);
        }

        private async Task<ProductInFridge> AddManuallyTypedProductToFridge(AddProductManuallyToFridgeCommand request, FridgeAggregate fridge)
        {
            if (string.IsNullOrWhiteSpace(request.ProductName))
            {
                throw new ArgumentEmptyException(nameof(request.ProductName));
            }

            var productType = request.ProductTypeId.HasValue
                ? await context.ProductTypes.FindAsync(request.ProductTypeId.Value)
                : await productTypeInferingService.GetProductType(request.ProductName);

            if (productType is null)
            {
                throw new EntityDoesNotExistException<ProductType>();
            }

            var productByName = await context.Products
                .Where(x => x.ProductTypeId == productType.ProductTypeId)
                .FirstOrDefaultAsync(x => x.Name == request.ProductName);

            var productToBeAddedToFridge = productByName ?? await AddNewProduct(request.ProductName, productType, request.ExpirationDate, request.Quantity);
            var productExpirationDate = request.ExpirationDate ?? shelfLifeInferingService.GetExpirationDate(productToBeAddedToFridge);

            return await fridgeStore.AddProductToFridge(fridge, productToBeAddedToFridge, productExpirationDate);
        }

        private async Task<Product> AddNewProduct(string productName, ProductType productType, DateTime? expirationDate, double? quantity)
        {
            var expirationTermInTicks = expirationDate.HasValue
                ? (expirationDate.Value - DateTime.Today).Ticks
                : shelfLifeInferingService.GetExpirationTerm(productType).GetTicks();

            var product = new Product
            {
                Name = productName,
                ShelfLifeDuration = expirationTermInTicks,
                ProductTypeId = productType.ProductTypeId,
                Quantity = quantity
            };

            var addedProduct = context.Products.Add(product);
            await context.SaveChangesAsync();

            return addedProduct.Entity;
        }

        private async Task<ProductInFridgeInfo> PrepareOutput(User fridgeOwner, ProductInFridge product)
        {
            await context.LoadAdditionalInfoForOutput(product);

            var productForOutput = mapper.Map<ProductInFridgeInfo>(product);
            productForOutput.IsFavorite = fridgeOwner.FavoriteProducts.Select(x => x.ProductId).Contains(productForOutput.ProductId);

            return productForOutput;
        }
    }
}
