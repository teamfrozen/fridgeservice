﻿using AutoMapper;
using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.RequestModels;
using FridgeService.Transport.Responses;
using FridgeService.WebApi.Domain.Fridges.Services;
using FridgeService.WebApi.Domain.Products.Services;
using FridgeService.WebApi.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Fridges.UseCases
{
    public class AddProductFromReceiptToFridgeCommandHandler : IRequestHandler<AddProductFromReceiptToFridgeCommand, ResultOfAddingProductsFromReceipt>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;
        private readonly IShelfLifeInferingService shelfLifeInferingService;
        private readonly FridgeStore fridgeStore;
        private readonly IProductTypeInferingService productTypeInferingService;

        public AddProductFromReceiptToFridgeCommandHandler(
            IAppDbContext context,
            IMapper mapper,
            IShelfLifeInferingService shelfLifeInferingService,
            IProductTypeInferingService productTypeInferingService,
            FridgeStore fridgeStore)
        {
            this.context = context;
            this.mapper = mapper;
            this.shelfLifeInferingService = shelfLifeInferingService;
            this.productTypeInferingService = productTypeInferingService;
            this.fridgeStore = fridgeStore;
        }
        public async Task<ResultOfAddingProductsFromReceipt> Handle(AddProductFromReceiptToFridgeCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var fridgeOwner = await context.Users
                .Include(x => x.FavoriteProducts)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.UserId == request.UserId);
            if (fridgeOwner is null)
            {
                throw new EntityDoesNotExistException<User>();
            }

            var fridge = await fridgeStore.GetAsync(fridgeOwner.FridgeId);

            var errorList = new List<string>(request.ProductsToBeAdded.Count);
            var productList = new List<ProductInFridgeInfo>(request.ProductsToBeAdded.Count);
            foreach (var product in request.ProductsToBeAdded)
            {
                if (string.IsNullOrWhiteSpace(product.ProductName))
                {
                    errorList.Add("The product name cannot be empty");
                }
                else
                {
                    try
                    {
                        var productAdded = mapper.Map<ProductInFridgeInfo>(await AddSingleProduct(fridge, product));
                        productAdded.IsFavorite = fridgeOwner.FavoriteProducts.Any(x => x.ProductId == productAdded.ProductId);
                        productList.Add(productAdded);
                    }
                    catch (Exception ex)
                    {
                        errorList.Add(product.ProductName + ": " + ex.Message);
                    }
                }
            }

            return new ResultOfAddingProductsFromReceipt
            {
                AddedProducts = productList,
                Errors = errorList
            };
        }

        private async Task<ProductInFridge> AddSingleProduct(FridgeAggregate fridge, ProductToBeAddedFromReceiptToFridge productInfo)
        {
            var productType = await productTypeInferingService.GetProductType(productInfo.ProductName);

            var productByName = await context.Products
                .Where(x => x.ProductTypeId == productType.ProductTypeId)
                .FirstOrDefaultAsync(x => x.Name == productInfo.ProductName);

            var productToBeAddedToFridge = productByName ?? await AddNewProduct(productInfo.ProductName, productType, productInfo.ExpirationDate, productInfo.Quantity);
            var productExpirationDate = productInfo.ExpirationDate ?? shelfLifeInferingService.GetExpirationDate(productToBeAddedToFridge);

            return await fridgeStore.AddProductToFridge(fridge, productToBeAddedToFridge, productExpirationDate);
        }

        private async Task<Product> AddNewProduct(string productName, ProductType productType, DateTime? expirationDate, double? quantity)
        {
            var expirationTermInTicks = expirationDate.HasValue
                ? (expirationDate.Value - DateTime.Today).Ticks
                : shelfLifeInferingService.GetExpirationTerm(productType).GetTicks();

            var product = new Product
            {
                Name = productName,
                ShelfLifeDuration = expirationTermInTicks,
                ProductTypeId = productType.ProductTypeId,
                Quantity = quantity
            };

            var addedProduct = context.Products.Add(product);
            await context.SaveChangesAsync();

            return addedProduct.Entity;
        }
    }
}
