﻿using AutoMapper;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Fridges.UseCases
{
    public class GetProductInFridgeCommandHandler : IRequestHandler<GetProductInFridgeCommand, ProductInFridgeInfo>
    {
        private readonly IMapper mapper;
        private readonly IAppDbContext context;

        public GetProductInFridgeCommandHandler(IAppDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<ProductInFridgeInfo> Handle(GetProductInFridgeCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var fridgeOwner = await context.Users
                .Include(x => x.FavoriteProducts)
                .FirstOrDefaultAsync(x => x.UserId == request.UserId);
            if (fridgeOwner is null)
            {
                return null;
            }

            var productInFridge = await context.ProductsInFridge
                .Include(x => x.Product)
                    .ThenInclude(x => x.ProductType)
                        .ThenInclude(x => x.Measure)
                .FirstOrDefaultAsync(x => x.ProductInFridgeId == request.ProductInFridgeId);
            if (productInFridge is null)
            {
                return null;
            }

            var productForOutput = mapper.Map<ProductInFridgeInfo>(productInFridge);
            productForOutput.IsFavorite = fridgeOwner.FavoriteProducts
                .Select(x => x.ProductId)
                .Contains(productForOutput.ProductId);

            return productForOutput;
        }
    }
}
