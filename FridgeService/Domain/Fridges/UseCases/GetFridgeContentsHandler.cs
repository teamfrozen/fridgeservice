﻿using AutoMapper;
using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Fridges.UseCases
{
    public class GetFridgeContentsHandler : IRequestHandler<GetFridgeContentsCommand, IEnumerable<ProductInFridgeInfo>>
    {
        private readonly IMapper mapper;
        private readonly FridgeStore fridgeStore;
        private readonly IAppDbContext context;

        public GetFridgeContentsHandler(
            IAppDbContext context,
            IMapper mapper,
            FridgeStore fridgeStore)
        {
            this.context = context;
            this.mapper = mapper;
            this.fridgeStore = fridgeStore;
        }

        public async Task<IEnumerable<ProductInFridgeInfo>> Handle(GetFridgeContentsCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var fridgeOwner = await context.Users
                .Include(x => x.FavoriteProducts)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.UserId == request.UserId);
            if (fridgeOwner is null)
            {
                return null;
            }

            var fridgeAggregate = await fridgeStore.GetAsync(fridgeOwner.FridgeId);
            var productsInFridge = fridgeAggregate.Products.Values.OrderBy(x => x.ExpirationDate);

            return PrepareOutput(fridgeOwner, productsInFridge);
        }

        private IEnumerable<ProductInFridgeInfo> PrepareOutput(User fridgeOwner, IOrderedEnumerable<ProductInFridge> productsInFridge)
        {
            var userFavoriteProductIds = fridgeOwner.FavoriteProducts
                .Select(x => x.ProductId);

            var productsForOutput = productsInFridge
                            .Select(x => mapper.Map<ProductInFridgeInfo>(x))
                            .ToList();
            productsForOutput.ForEach(x => x.IsFavorite = userFavoriteProductIds.Contains(x.ProductId));

            return productsForOutput;
        }
    }
}
