﻿using AutoMapper;
using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using FridgeService.WebApi.Exceptions;
using FridgeService.WebApi.Extensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Fridges.UseCases
{
    public class UpdateFridgeProductExpirationDateCommandHandler : IRequestHandler<UpdateFridgeProductExpirationDateCommand, ProductInFridgeInfo>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;
        private readonly FridgeStore fridgeStore;

        public UpdateFridgeProductExpirationDateCommandHandler(IAppDbContext context,
            IMapper mapper,
            FridgeStore fridgeStore)
        {
            this.mapper = mapper;
            this.fridgeStore = fridgeStore;
            this.context = context;
        }

        public async Task<ProductInFridgeInfo> Handle(UpdateFridgeProductExpirationDateCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var fridgeOwner = await context.Users
                .Include(x => x.FavoriteProducts)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.UserId == request.UserId);
            if (fridgeOwner is null)
            {
                throw new EntityDoesNotExistException<User>();
            }

            var fridgeProduct = await context.ProductsInFridge.FindAsync(request.ProductInFridgeId);
            if (fridgeProduct is null)
            {
                throw new ProductDoesNotExistInFridgeException();
            }

            var fridgeAggregate = await fridgeStore.GetAsync(fridgeProduct.FridgeId);
            var updatedProduct = await fridgeStore.UpdateProductExpirationDateAsync(fridgeAggregate, request.ProductInFridgeId, request.TargetExpirationDate);

            return await PrepareOutput(fridgeOwner, updatedProduct);
        }

        private async Task<ProductInFridgeInfo> PrepareOutput(User fridgeOwner, ProductInFridge product)
        {
            await context.LoadAdditionalInfoForOutput(product);

            var productForOutput = mapper.Map<ProductInFridgeInfo>(product);
            productForOutput.IsFavorite = fridgeOwner.FavoriteProducts.Select(x => x.ProductId).Contains(productForOutput.ProductId);

            return productForOutput;
        }
    }
}
