﻿using AutoMapper;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Fridges.UseCases
{
    public class GetFridgeCommandHandler : IRequestHandler<GetFridgeCommand, FridgeInfo>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;

        public GetFridgeCommandHandler(
            IAppDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<FridgeInfo> Handle(GetFridgeCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var fridge = await context.Fridges
                .Include(x => x.Users)
                .FirstOrDefaultAsync(x => x.FridgeId == request.FridgeId);

            return mapper.Map<FridgeInfo>(fridge);
        }
    }
}
