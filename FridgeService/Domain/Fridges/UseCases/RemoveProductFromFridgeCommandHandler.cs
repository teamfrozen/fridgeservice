﻿using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.WebApi.Exceptions;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Fridges.UseCases
{
    public class RemoveProductFromFridgeCommandHandler : IRequestHandler<RemoveProductFromFridgeCommand, Unit>
    {
        private readonly FridgeStore fridgeStore;
        private readonly IAppDbContext context;

        public RemoveProductFromFridgeCommandHandler(
            IAppDbContext context,
            FridgeStore fridgeStore)
        {
            this.fridgeStore = fridgeStore;
            this.context = context;
        }

        public async Task<Unit> Handle(RemoveProductFromFridgeCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var productInFridge = await context.ProductsInFridge.FindAsync(request.ProductInFridgeId);
            if (productInFridge is null)
            {
                throw new ProductDoesNotExistInFridgeException();
            }

            var fridge = await fridgeStore.GetAsync(productInFridge.FridgeId);

            await fridgeStore.RemoveProductFromFridgeAsync(fridge, request.ProductInFridgeId);

            return Unit.Value;
        }
    }
}
