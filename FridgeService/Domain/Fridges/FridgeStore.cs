﻿using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.WebApi.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Fridges
{
    public class FridgeStore
    {
        private readonly IMemoryCache memoryCache;
        private readonly IAppDbContext context;

        public FridgeStore(
            IMemoryCache memoryCache,
            IAppDbContext context)
        {
            this.memoryCache = memoryCache;
            this.context = context;
        }

        public async Task<FridgeAggregate> GetAsync(Guid fridgeId)
        {
            // Stores reference types; thus, the question of using Locking Pattern for getting aggregate is open            
            if (memoryCache.TryGetValue(fridgeId, out FridgeAggregate fridgeAggregateFromCache))
            {
                return fridgeAggregateFromCache;
            }

            var fridgeFromDb = await context.Fridges
                .Include(x => x.ProductsInFridge)
                    .ThenInclude(x => x.Product)
                    .ThenInclude(x => x.ProductType)
                    .ThenInclude(x => x.Measure)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.FridgeId == fridgeId);

            if (fridgeFromDb == null)
            {
                throw new EntityDoesNotExistException<Fridge>();
            }

            var fridgeAggregateFromDb = new FridgeAggregate(fridgeFromDb);
            memoryCache.Set(fridgeFromDb.FridgeId, fridgeAggregateFromDb);

            return fridgeAggregateFromDb;
        }

        public async Task<ProductInFridge> AddProductToFridge(FridgeAggregate aggregate, Product product, DateTime expirationDate)
        {
            if (aggregate is null)
            {
                throw new ArgumentNullException(nameof(aggregate));
            }
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }

            if (expirationDate < DateTime.Today)
            {
                throw new ExpirationDateEarlierThanTodayException();
            }

            var productToBeAddedToFridge = new ProductInFridge
            {
                Product = product,
                DepositDate = DateTime.Now,
                ExpirationDate = expirationDate,
                FridgeId = aggregate.FridgeId
            };

            var addedProduct = context.ProductsInFridge.Add(productToBeAddedToFridge);
            await context.SaveChangesAsync();

            aggregate.TryAddProductToFridge(addedProduct.Entity);

            return addedProduct.Entity;
        }

        public async Task RemoveProductFromFridgeAsync(FridgeAggregate aggregate, Guid productInFridgeId)
        {
            var removedFromCache = aggregate.TryRemoveProductFromFridge(productInFridgeId);
            if (removedFromCache)
            {
                var fridgeProductToRemoveFromDb = await context.ProductsInFridge.FindAsync(productInFridgeId);
                if (fridgeProductToRemoveFromDb != null)
                {
                    context.ProductsInFridge.Remove(fridgeProductToRemoveFromDb);
                    await context.SaveChangesAsync();
                }
            }
            else
            {
                throw new ProductDoesNotExistInFridgeException();
            }
        }

        public async Task<ProductInFridge> UpdateProductExpirationDateAsync(FridgeAggregate aggregate, Guid productInFridgeid, DateTime targetExpirationDate)
        {
            var productToUpdate = await context.ProductsInFridge.FindAsync(productInFridgeid);
            if (productToUpdate is null)
            {
                throw new ProductDoesNotExistInFridgeException();
            }

            if (targetExpirationDate < DateTime.Today)
            {
                throw new ExpirationDateEarlierThanTodayException();
            }

            productToUpdate.ExpirationDate = targetExpirationDate;
            var updatedProduct = context.ProductsInFridge.Update(productToUpdate);
            await context.SaveChangesAsync();

            return aggregate.UpdateProductInfo(updatedProduct.Entity);
        }

        public async Task RemoveFridge(FridgeAggregate aggregate)
        {
            var fridgeToRemove = await context.Fridges
                .FindAsync(aggregate.FridgeId);

            if (fridgeToRemove != null)
            {
                await context.Entry(fridgeToRemove)
                    .Collection(x => x.ProductsInFridge)
                    .LoadAsync();

                memoryCache.Remove(aggregate.FridgeId);
                context.Fridges.Remove(fridgeToRemove);

                await context.SaveChangesAsync();
            }
            else
            {
                throw new EntityDoesNotExistException<Fridge>();
            }
        }
    }
}
