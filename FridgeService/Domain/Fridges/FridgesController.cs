﻿using FridgeService.Transport.Commands;
using FridgeService.Transport.RequestModels;
using FridgeService.Transport.Responses;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Fridges
{
    /// <summary>
    /// Controller for working with Fridges
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class FridgesController : ControllerBase
    {
        private readonly IMediator mediator;

        public FridgesController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        /// <summary>
        /// Get all fridges in the system
        /// </summary>
        /// <returns>Collection of Fridge-UserOwners data</returns>
        [HttpGet]
        public async Task<IEnumerable<FridgeInfo>> GetFridges()
        {
            return await mediator.Send(new GetAllFridgesCommand());
        }

        /// <summary>
        /// Get Fridge by Id
        /// </summary>
        /// <param name="fridgeId">Fridge Id</param>
        /// <returns>Fridge-UserOwners data</returns>
        /// <response code="404">Fridge Not Found</response>
        [HttpGet("{fridgeId}/Owners")]
        public async Task<ActionResult<FridgeInfo>> GetFridge(Guid fridgeId)
        {
            var fridgeInfo = await mediator.Send(new GetFridgeCommand(fridgeId));
            return fridgeInfo is null ? NotFound() : fridgeInfo;
        }

        /// <summary>
        /// Get fridge contents for a specific user (with marking of favorite products)
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns>All the products in user's fridge</returns>
        /// <response code="404">User Not Found</response>
        [HttpGet("OwnerContents/{userId}")]
        public async Task<ActionResult<IEnumerable<ProductInFridgeInfo>>> GetFridgeContents(Guid userId)
        {
            var fridgeContents = await mediator.Send(new GetFridgeContentsCommand(userId));
            return fridgeContents is null ? NotFound() : Ok(fridgeContents);
        }

        /// <summary>
        /// Add a single product to the fridge (manually)
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="productInfo">Information about the product and User Id</param>
        /// <returns>Info about added product</returns>
        /// <response code="400">Incorrect Product Input</response>
        [HttpPost("OwnerContents/{userId}/AddProductManually")]
        public async Task<ProductInFridgeInfo> AddProductManuallyToFridge(Guid userId, ProductToBeAddedManuallyToFridge productInfo)
        {
            return await mediator.Send(new AddProductManuallyToFridgeCommand(productInfo, userId));
        }

        /// <summary>
        /// Add all the products from the Receipt (if a product cannot be added, it is skipped and then mentioned in Error list)
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="productsInfo">Information about a product including name, quantity and expiration date</param>
        /// <returns>List of added products and List of Errors</returns>
        [HttpPost("OwnerContents/{userId}/AddProductsFromReceipt")]
        public async Task<ResultOfAddingProductsFromReceipt> AddListOfProductsToFridge(Guid userId, ICollection<ProductToBeAddedFromReceiptToFridge> productsInfo)
        {
            return await mediator.Send(new AddProductFromReceiptToFridgeCommand(productsInfo, userId));
        }

        /// <summary>
        /// Get information about a particular product in the fridge
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="productInFridgeId">Id of a product in the fridge</param>
        /// <returns>Product in fridge info</returns>
        /// <response code="404">User or Product Not Found</response>
        [HttpGet("OwnerContents/{userId}/{productInFridgeId}")]
        public async Task<ActionResult<ProductInFridgeInfo>> GetProductInFridge(Guid userId, Guid productInFridgeId)
        {
            var productInfo = await mediator.Send(new GetProductInFridgeCommand(userId, productInFridgeId));

            return productInfo == null ? NotFound() : productInfo;
        }

        /// <summary>
        /// Set product's expiration date
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="productInFridgeId">Product In Fridge Id</param>
        /// <param name="targetExpirationDate">Product expiration date</param>
        /// <returns>Info about updated product</returns>
        /// <response code="400">Incorrect Input</response>
        [HttpPost("OwnerContents/{userId}/{productInFridgeId}/SetExpiratonDate")]
        public async Task<ProductInFridgeInfo> UpdateProductExpirationDate(Guid userId, Guid productInFridgeId, [FromBody] DateTime targetExpirationDate)
        {
            return await mediator.Send(new UpdateFridgeProductExpirationDateCommand(userId, productInFridgeId, targetExpirationDate));
        }

        /// <summary>
        /// Remove a product from the fridge
        /// </summary>
        /// <param name="productInFridgeId">Id of a product in the fridge</param>
        /// <param name="userId">User Id</param>
        /// <response code="400">Incorrect Input</response>
        [HttpDelete("OwnerContents/{userId}/{productInFridgeId}")]
        public async Task<Unit> RemoveProductFromFridge(Guid userId, Guid productInFridgeId)
        {
            return await mediator.Send(new RemoveProductFromFridgeCommand(productInFridgeId, userId));
        }
    }
}
