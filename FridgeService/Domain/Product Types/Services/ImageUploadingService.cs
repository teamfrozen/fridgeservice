﻿using FridgeService.WebApi.Domain.Product_Types.Models;
using FridgeService.WebApi.Exceptions;
using FridgeService.WebApi.Options;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Product_Types.Services
{
    public class ImageUploadingService : IImageUploadingService
    {
        private static readonly string[] availableExtensions = { ".jpg", ".png", ".jpeg", ".bmp", ".gif" };
        private static readonly string imageFolder = "Images/";

        private ProductTypeSettings productTypeSettings;
        private readonly IWebHostEnvironment environment;

        public ImageUploadingService(
            IOptions<ProductTypeSettings> productTypeSettings,
            IWebHostEnvironment environment)
        {
            this.productTypeSettings = productTypeSettings.Value;
            this.environment = environment;
        }

        public async Task<string> UploadProductTypeImage(ImageInfo image, string productTypeName)
        {
            if (image is null)
            {
                return productTypeSettings.DefaultImage;
            }

            if (string.IsNullOrWhiteSpace(productTypeName))
            {
                throw new ArgumentEmptyException(nameof(productTypeName));
            }

            var fileExtension = image.ImageName.Substring(image.ImageName.LastIndexOf('.')).ToLower();
            if (!availableExtensions.Contains(fileExtension, StringComparer.CurrentCultureIgnoreCase))
            {
                throw new IncorrectFileExtensionException(availableExtensions);
            }

            string imagePath = imageFolder + productTypeName + fileExtension;

            DirectoryInfo imageDirectory = new DirectoryInfo(Path.Combine(environment.WebRootPath, imageFolder));
            var files = imageDirectory.GetFiles(productTypeName + ".*");
            foreach (var file in files)
            {
                File.Delete(file.FullName);
            }

            await File.WriteAllBytesAsync(Path.Combine(environment.WebRootPath, imagePath), image.ImageContents);

            return imagePath;
        }
    }
}
