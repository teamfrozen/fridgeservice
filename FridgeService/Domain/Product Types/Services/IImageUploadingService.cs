﻿using FridgeService.WebApi.Domain.Product_Types.Models;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Product_Types.Services
{
    public interface IImageUploadingService
    {
        Task<string> UploadProductTypeImage(ImageInfo image, string productTypeName);
    }
}