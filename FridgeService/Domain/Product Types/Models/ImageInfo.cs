﻿namespace FridgeService.WebApi.Domain.Product_Types.Models
{
    public class ImageInfo
    {
        public byte[] ImageContents { get; set; }
        public string ImageName { get; set; }
    }
}
