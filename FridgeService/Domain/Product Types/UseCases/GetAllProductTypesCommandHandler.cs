﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Product_Types.UseCases
{
    public class GetAllProductTypesCommandHandler : IRequestHandler<GetAllProductTypesCommand, IEnumerable<ProductTypeShortInfo>>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;

        public GetAllProductTypesCommandHandler(IAppDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<ProductTypeShortInfo>> Handle(GetAllProductTypesCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            return await context.ProductTypes
                .ProjectTo<ProductTypeShortInfo>(mapper.ConfigurationProvider)
                .ToListAsync();
        }
    }
}
