﻿using AutoMapper;
using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using FridgeService.WebApi.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Product_Types.UseCases
{
    public class UpdateProductTypeShelfLifeCommandHandler : IRequestHandler<UpdateProductTypeShelfLifeCommand, ProductTypeDetailedInfo>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;

        public UpdateProductTypeShelfLifeCommandHandler(IAppDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<ProductTypeDetailedInfo> Handle(UpdateProductTypeShelfLifeCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            if (request.AverageShelfLifeDuration.Hours < 0 || request.AverageShelfLifeDuration.Days < 0)
            {
                throw new NegativeShelfLifeException();
            }

            var productTypeToBeUpdated = await context.ProductTypes
                .Include(x => x.Measure)
                .Include(x => x.ProductAliases)
                .FirstOrDefaultAsync(x => x.ProductTypeId == request.ProductTypeId);
            if (productTypeToBeUpdated is null)
            {
                throw new EntityDoesNotExistException<ProductType>();
            }

            productTypeToBeUpdated.AvgShelfLifeDuration = request.AverageShelfLifeDuration.GetTicks();
            context.ProductTypes.Update(productTypeToBeUpdated);

            await context.SaveChangesAsync();

            return mapper.Map<ProductTypeDetailedInfo>(productTypeToBeUpdated);
        }
    }
}
