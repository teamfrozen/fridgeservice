﻿using AutoMapper;
using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using FridgeService.WebApi.Domain.Fridges.Services;
using FridgeService.WebApi.Domain.Product_Types.Models;
using FridgeService.WebApi.Domain.Product_Types.Services;
using FridgeService.WebApi.Exceptions;
using FridgeService.WebApi.Extensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Product_Types.UseCases
{
    public class UpdateProductTypeCommandHandler : IRequestHandler<UpdateProductTypeCommand, ProductTypeDetailedInfo>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;
        private readonly IMemoryCache memoryCache;
        private readonly IShelfLifeInferingService shelfLifeInferingService;
        private readonly IImageUploadingService imageUploadingService;

        public UpdateProductTypeCommandHandler(
            IAppDbContext context,
            IMapper mapper,
            IMemoryCache memoryCache,
            IShelfLifeInferingService shelfLifeInferingService,
            IImageUploadingService imageUploadingService)
        {
            this.context = context;
            this.mapper = mapper;
            this.memoryCache = memoryCache;
            this.shelfLifeInferingService = shelfLifeInferingService;
            this.imageUploadingService = imageUploadingService;
        }

        public async Task<ProductTypeDetailedInfo> Handle(UpdateProductTypeCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            await ValidateInput(request);

            ImageInfo productTypeImage = null;
            if (request.ProductImageId.HasValue && !memoryCache.TryGetValue(request.ProductImageId.Value, out productTypeImage))
            {
                throw new ProductImageNotInCacheException();
            }
            var productTypeToUpdate = await context.ProductTypes.FindAsync(request.ProductTypeId);
            if (productTypeToUpdate is null)
            {
                throw new EntityDoesNotExistException<ProductType>();
            }

            var productImagePath = request.ProductImageId.HasValue
                ? await imageUploadingService.UploadProductTypeImage(productTypeImage, request.ProductTypeName)
                : productTypeToUpdate.ProductImagePath;

            var productTypeMeasureId = await GetProductTypeMeasureId(request);

            var avgShelfLifeDuration = request.AverageShelfLifeDuration.HasValue
                    ? request.AverageShelfLifeDuration.Value.GetTicks()
                    : shelfLifeInferingService.GetDefaultExpirationTerm().GetTicks();

            productTypeToUpdate.AvgShelfLifeDuration = avgShelfLifeDuration;
            productTypeToUpdate.MeasureId = productTypeMeasureId;
            productTypeToUpdate.ProductImagePath = productImagePath;
            productTypeToUpdate.Name = request.ProductTypeName;

            var updatedProduct = context.ProductTypes.Update(productTypeToUpdate).Entity;

            await UpdateProductAliases(request);

            await context.SaveChangesAsync();

            if (request.ProductImageId.HasValue)
            {
                memoryCache.Remove(request.ProductImageId.Value);
            }

            await context.LoadAdditionalInfoForOutput(updatedProduct);
            return mapper.Map<ProductTypeDetailedInfo>(updatedProduct);
        }

        private async Task UpdateProductAliases(UpdateProductTypeCommand request)
        {
            var existingProductAliases = await context.ProductAliases
                            .Where(x => x.ProductTypeId == request.ProductTypeId)
                            .ToListAsync();
            context.ProductAliases.RemoveRange(existingProductAliases);

            foreach (var addedProductAlias in request.ProductAliases)
            {
                context.ProductAliases.Add(new ProductAlias
                {
                    Name = addedProductAlias,
                    ProductTypeId = request.ProductTypeId
                });
            }
        }

        private async Task ValidateInput(UpdateProductTypeCommand request)
        {
            if (string.IsNullOrWhiteSpace(request.ProductTypeName))
            {
                throw new ArgumentEmptyException(nameof(request.ProductTypeName));
            }
            if (request.ProductAliases is null || !request.ProductAliases.Any())
            {
                throw new ArgumentEmptyException(nameof(request.ProductAliases));
            }
            if (request.AverageShelfLifeDuration.HasValue && (request.AverageShelfLifeDuration.Value.Days < 0 || request.AverageShelfLifeDuration.Value.Hours < 0))
            {
                throw new NegativeShelfLifeException();
            }
            if (context.ProductTypes.Any(x => x.Name.ToLower() == request.ProductTypeName.ToLower()))
            {
                throw new EntityAlreadyExistsException<ProductType>();
            }

            var aliasNamesIntersection = (await context.ProductAliases
                .Where(x => x.ProductTypeId != request.ProductTypeId)
                .Select(x => x.Name).ToListAsync())
                .Intersect(request.ProductAliases, StringComparer.CurrentCultureIgnoreCase);
            if (aliasNamesIntersection.Any())
            {
                throw new AliasesConnectedToOtherProductTypesException(request.ProductAliases);
            }
        }

        private async Task<Guid?> GetProductTypeMeasureId(UpdateProductTypeCommand request)
        {
            if (request.MeasureId.HasValue && request.MeasureId.Value != Guid.Empty)
            {
                if (!context.Measures.Any(x => x.MeasureId == request.MeasureId.Value))
                {
                    throw new EntityDoesNotExistException<Measure>();
                }
                return request.MeasureId.Value;
            }

            if (string.IsNullOrWhiteSpace(request.MeasureName))
            {
                return null;
            }

            var existingMeasureWithEnteredName = (await context.Measures
                .ToListAsync())
                .FirstOrDefault(x => string.Equals(x.Name, request.MeasureName, StringComparison.CurrentCultureIgnoreCase));
            if (existingMeasureWithEnteredName != null)
            {
                return existingMeasureWithEnteredName.MeasureId;
            }

            var measureToBeAdded = new Measure(request.MeasureName);
            var addedMeasure = context.Measures.Add(measureToBeAdded);
            return addedMeasure.Entity.MeasureId;
        }
    }
}
