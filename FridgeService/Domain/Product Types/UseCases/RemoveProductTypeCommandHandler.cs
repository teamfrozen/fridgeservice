﻿using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.WebApi.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Product_Types.UseCases
{
    public class RemoveProductTypeCommandHandler : IRequestHandler<RemoveProductTypeCommand, Unit>
    {
        private readonly IAppDbContext context;

        public RemoveProductTypeCommandHandler(IAppDbContext context)
        {
            this.context = context;
        }

        public async Task<Unit> Handle(RemoveProductTypeCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var productType = await context.ProductTypes
                .Include(x => x.ProductAliases)
                .Include(x => x.Products)
                    .ThenInclude(x => x.ProductInFridges)
                .Include(x => x.Products)
                    .ThenInclude(x => x.UserFavorites)
                .FirstOrDefaultAsync(x => x.ProductTypeId == request.ProductTypeId);

            if (productType is null)
            {
                throw new EntityDoesNotExistException<ProductType>();
            }
            if (productType.Products.Any(x => x.ProductInFridges.Any()))
            {
                throw new ImpossibleEntityRemovementException("Products of this type exist in some fridges");
            }
            if (productType.Products.Any(x => x.UserFavorites.Any()))
            {
                throw new ImpossibleEntityRemovementException("Products of this type exist in favorites list of some users");
            }

            productType.ProductAliases.ForEach(x => context.ProductAliases.Remove(x));
            context.ProductTypes.Remove(productType);

            await context.SaveChangesAsync();

            return Unit.Value;
        }
    }
}
