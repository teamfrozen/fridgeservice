﻿using AutoMapper;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Product_Types.UseCases
{
    public class GetProductTypeCommandHandler : IRequestHandler<GetProductTypeCommand, ProductTypeDetailedInfo>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;

        public GetProductTypeCommandHandler(IAppDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<ProductTypeDetailedInfo> Handle(GetProductTypeCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var productType = await context.ProductTypes
                .Include(x => x.Measure)
                .Include(x => x.ProductAliases)
                .FirstOrDefaultAsync(x => x.ProductTypeId == request.ProductTypeId);

            return mapper.Map<ProductTypeDetailedInfo>(productType);
        }
    }
}
