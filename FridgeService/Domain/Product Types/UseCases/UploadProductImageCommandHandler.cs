﻿using FridgeService.Transport.Commands;
using FridgeService.WebApi.Domain.Product_Types.Models;
using MediatR;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Product_Types.UseCases
{
    public class UploadProductImageCommandHandler : IRequestHandler<UploadProductImageCommand, Guid>
    {
        private readonly IMemoryCache memoryCache;

        public UploadProductImageCommandHandler(IMemoryCache memoryCache)
        {
            this.memoryCache = memoryCache;
        }

        public async Task<Guid> Handle(UploadProductImageCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var imageGuid = Guid.NewGuid();
            using (var memoryStream = new MemoryStream())
            {
                await request.ProductTypeImage.CopyToAsync(memoryStream, cancellationToken);

                var imageInfo = new ImageInfo
                {
                    ImageContents = memoryStream.ToArray(),
                    ImageName = request.ProductTypeImage.FileName
                };

                memoryCache.Set(imageGuid, imageInfo);
            }

            return imageGuid;
        }
    }
}
