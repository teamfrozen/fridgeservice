﻿using AutoMapper;
using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using FridgeService.WebApi.Domain.Fridges.Services;
using FridgeService.WebApi.Domain.Product_Types.Models;
using FridgeService.WebApi.Domain.Product_Types.Services;
using FridgeService.WebApi.Exceptions;
using FridgeService.WebApi.Extensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Product_Types.UseCases
{
    public class AddProductTypeCommandHandler : IRequestHandler<AddProductTypeCommand, ProductTypeDetailedInfo>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;
        private readonly IShelfLifeInferingService shelfLifeInferingService;
        private readonly IImageUploadingService imageUploadingService;
        private readonly IMemoryCache memoryCache;

        public AddProductTypeCommandHandler(
            IAppDbContext context,
            IMapper mapper,
            IMemoryCache memoryCache,
            IShelfLifeInferingService shelfLifeInferingService,
            IImageUploadingService imageUploadingService)
        {
            this.context = context;
            this.mapper = mapper;
            this.shelfLifeInferingService = shelfLifeInferingService;
            this.imageUploadingService = imageUploadingService;
            this.memoryCache = memoryCache;
        }

        public async Task<ProductTypeDetailedInfo> Handle(AddProductTypeCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            await ValidateInput(request);

            ImageInfo productTypeImage = null;
            if (request.ProductImageId.HasValue && !memoryCache.TryGetValue(request.ProductImageId.Value, out productTypeImage))
            {
                throw new ProductImageNotInCacheException();
            }

            var addedProductType = await AddProductType(request, productTypeImage);
            AddProductAliases(request, addedProductType.ProductTypeId);

            await context.SaveChangesAsync();

            if (request.ProductImageId.HasValue)
            {
                memoryCache.Remove(request.ProductImageId.Value);
            }

            await context.LoadAdditionalInfoForOutput(addedProductType);
            return mapper.Map<ProductTypeDetailedInfo>(addedProductType);
        }

        private void AddProductAliases(AddProductTypeCommand request, Guid productTypeId)
        {
            foreach (var alias in request.ProductAliases)
            {
                context.ProductAliases.Add(new ProductAlias
                {
                    Name = alias,
                    ProductTypeId = productTypeId
                });
            }
        }

        private async Task<ProductType> AddProductType(AddProductTypeCommand request, ImageInfo productTypeImage)
        {
            var productTypeMeasureId = await GetProductTypeMeasureId(request);

            var avgShelfLifeDuration = request.AverageShelfLifeDuration.HasValue
                    ? request.AverageShelfLifeDuration.Value.GetTicks()
                    : shelfLifeInferingService.GetDefaultExpirationTerm().GetTicks();

            var productImagePath = await imageUploadingService.UploadProductTypeImage(productTypeImage, request.ProductTypeName);

            var productTypeToBeAdded = new ProductType
            {
                AvgShelfLifeDuration = avgShelfLifeDuration,
                MeasureId = productTypeMeasureId,
                Name = request.ProductTypeName,
                ProductImagePath = productImagePath
            };
            var addedProductType = context.ProductTypes.Add(productTypeToBeAdded);
            return addedProductType.Entity;
        }

        private async Task ValidateInput(AddProductTypeCommand request)
        {
            if (request.ProductAliases is null || !request.ProductAliases.Any())
            {
                throw new ArgumentEmptyException(nameof(request.ProductAliases));
            }
            var aliasNamesIntersection = (await context.ProductAliases.Select(x => x.Name).ToListAsync())
                .Intersect(request.ProductAliases, StringComparer.CurrentCultureIgnoreCase);
            if (aliasNamesIntersection.Any())
            {
                throw new AliasesConnectedToOtherProductTypesException(request.ProductAliases);
            }
            if (request.AverageShelfLifeDuration.HasValue && (request.AverageShelfLifeDuration.Value.Days < 0 || request.AverageShelfLifeDuration.Value.Hours < 0))
            {
                throw new NegativeShelfLifeException();
            }
            if (context.ProductTypes.Any(x => x.Name.ToLower() == request.ProductTypeName.ToLower()))
            {
                throw new EntityAlreadyExistsException<ProductType>();
            }
        }

        private async Task<Guid?> GetProductTypeMeasureId(AddProductTypeCommand request)
        {
            if (request.MeasureId.HasValue && request.MeasureId.Value != Guid.Empty)
            {
                if (!context.Measures.Any(x => x.MeasureId == request.MeasureId.Value))
                {
                    throw new EntityDoesNotExistException<Measure>();
                }
                return request.MeasureId.Value;
            }

            if (string.IsNullOrWhiteSpace(request.MeasureName))
            {
                return null;
            }

            var existingMeasureWithEnteredName = (await context.Measures
                .ToListAsync())
                .FirstOrDefault(x => string.Equals(x.Name, request.MeasureName, StringComparison.CurrentCultureIgnoreCase));
            if (existingMeasureWithEnteredName != null)
            {
                return existingMeasureWithEnteredName.MeasureId;
            }

            var measureToBeAdded = new Measure(request.MeasureName);
            var addedMeasure = context.Measures.Add(measureToBeAdded);
            return addedMeasure.Entity.MeasureId;
        }
    }
}
