﻿using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Product_Types
{
    /// <summary>
    /// Controller for working with product types
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ProductTypesController : ControllerBase
    {
        private readonly IMediator mediator;

        public ProductTypesController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        /// <summary>
        /// Get all product types in the system
        /// </summary>
        /// <returns>Collection of all product types</returns>
        [HttpGet]
        public async Task<IEnumerable<ProductTypeShortInfo>> GetProductTypes()
        {
            return await mediator.Send(new GetAllProductTypesCommand());
        }

        /// <summary>
        /// Get detailed info about a particular product type
        /// </summary>
        /// <param name="productTypeId">Product Type Id</param>
        /// <returns>Detailed product type info</returns>
        /// <response code="404">Product type not found</response>
        [HttpGet("{productTypeId}")]
        public async Task<ActionResult<ProductTypeDetailedInfo>> GetProductType(Guid productTypeId)
        {
            var productTypeInfo = await mediator.Send(new GetProductTypeCommand(productTypeId));

            return productTypeInfo is null ? NotFound() : productTypeInfo;
        }

        /// <summary>
        /// Add a new product type to the system (presumes calls from admin only).
        /// IMPORTANT: The method must be called after "UploadImage" to add image: ProductImageId is the result of this action
        /// </summary>
        /// <param name="command">Information about a product type needed for adding</param>
        /// <returns>Detailed info about the added product type</returns>
        /// <response code="400">Incorrect Product Type Input</response>
        [HttpPost]
        public async Task<ProductTypeDetailedInfo> AddProductType(AddProductTypeCommand command)
        {
            return await mediator.Send(command);
        }

        /// <summary>
        /// Update information about product type (presumes calls from admin only).
        /// IMPORTANT: The method must be called after "UploadImage" to update image: ProductImageId is the result of this action
        /// </summary>
        /// <param name="command">Updated information about product type</param>
        /// <returns>Detailed info about the updated product type</returns>
        /// <response code="400">Incorrect Product Type Input</response>
        [HttpPut]
        public async Task<ProductTypeDetailedInfo> UpdateProductType(UpdateProductTypeCommand command)
        {
            return await mediator.Send(command);
        }

        /// <summary>
        /// Set avg shelf life for a product type (presumes calls from admin only)
        /// </summary>
        /// <param name="productTypeId">Product Type Id</param>
        /// <param name="averageShelfLifeDuration">Target average shelf life duration</param>
        /// <returns>Detailed info about the updated product type</returns>
        /// <response code="400">Incorrect Input</response>
        [HttpPost("{productTypeId}/SetShelfLife")]
        public async Task<ProductTypeDetailedInfo> UpdateShelfLife(Guid productTypeId, ExpirationTerm averageShelfLifeDuration)
        {
            return await mediator.Send(new UpdateProductTypeShelfLifeCommand(productTypeId, averageShelfLifeDuration));
        }

        /// <summary>
        /// Remove a product type (presumes calls from admin only)
        /// </summary>
        /// <param name="productTypeId">Product type Id</param>
        /// <response code="400">Product type cannot be removed</response>
        [HttpDelete("{productTypeId}")]
        public async Task<Unit> RemoveProductType(Guid productTypeId)
        {
            return await mediator.Send(new RemoveProductTypeCommand(productTypeId));
        }

        /// <summary>
        /// Upload product type image (needed for updating product type info)
        /// </summary>
        /// <param name="image">New product type image</param>
        /// <response code="400">Incorrect Image</response>
        [HttpPost("UploadImage")]
        public async Task<Guid> UploadProductTypeImage(IFormFile image)
        {
            return await mediator.Send(new UploadProductImageCommand(image));
        }
    }
}
