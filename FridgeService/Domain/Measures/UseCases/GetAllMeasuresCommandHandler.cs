﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Measures.UseCases
{
    public class GetAllMeasuresCommandHandler : IRequestHandler<GetAllMeasuresCommand, IEnumerable<MeasureShortInfo>>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;
        public GetAllMeasuresCommandHandler(IAppDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }
        public async Task<IEnumerable<MeasureShortInfo>> Handle(GetAllMeasuresCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            return await context.Measures
                .ProjectTo<MeasureShortInfo>(mapper.ConfigurationProvider)
                .ToListAsync();
        }
    }
}
