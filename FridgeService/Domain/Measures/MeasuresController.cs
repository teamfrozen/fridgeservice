﻿
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Measures
{
    [Route("api/[controller]")]
    [ApiController]
    public class MeasuresController : ControllerBase
    {
        private readonly IMediator mediator;

        public MeasuresController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        /// <summary>
        /// Get all available measures
        /// </summary>
        /// <returns>Collection of measures</returns>
        [HttpGet]
        public async Task<IEnumerable<MeasureShortInfo>> GetMeasures()
        {
            return await mediator.Send(new GetAllMeasuresCommand());
        }
    }
}
