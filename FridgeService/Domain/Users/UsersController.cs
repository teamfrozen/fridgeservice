﻿using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Users
{
    /// <summary>
    /// Controller for working with Users
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IMediator mediator;

        public UsersController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        /// <summary>
        /// Get all users with fridges in the system
        /// </summary>
        /// <returns>Collection of User-Fridge data</returns>
        [HttpGet]
        public async Task<IEnumerable<UserInfo>> GetUsers()
        {
            return await mediator.Send(new GetAllUsersCommand());
        }

        /// <summary>
        /// Get User by Id
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns>Fridge-UserOwners data</returns>
        /// <response code="404">User not found</response>
        [HttpGet("{userId}")]
        public async Task<ActionResult<UserInfo>> GetUser(Guid userId)
        {
            var userInfo = await mediator.Send(new GetUserCommand(userId));
            return userInfo is null ? NotFound() : userInfo;
        }

        /// <summary>
        /// Add a new user to the service. If FridgeId is null, new Fridge is assigned, otherwise the fridge with fridgeId is assigned to the user
        /// </summary>
        /// <returns>Information about added user</returns>
        /// <response code="400">Incorrect input</response>
        [HttpPost]
        public async Task<UserInfo> AddUser(AddUserCommand command)
        {
            return await mediator.Send(command);
        }

        /// <summary>
        /// Remove a user from the system
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <response code="400">User cannot be deleted</response>
        [HttpDelete("{userId}")]
        public async Task<Unit> RemoveUser(Guid userId)
        {
            return await mediator.Send(new RemoveUserCommand(userId));
        }

        /// <summary>
        /// Bind a user to a brand new fridge
        /// </summary>
        /// <param name="userId">User id</param>
        /// <returns>Fridge-UserOwners data</returns>
        /// <response code="400">User cannot be binded to new fridge</response>
        [HttpPost("BindUserToNewFridge")]
        public async Task<FridgeInfo> BindUserToNewFridge([FromBody] Guid userId)
        {
            return await mediator.Send(new BindUserToNewFridgeCommand(userId));
        }

        /// <summary>
        /// Bind a user to the existing fridge
        /// </summary>
        /// <param name="command">Information needed for binding: User Id, Fridge Id</param>
        /// <returns>Fridge-UserOwners data</returns>
        /// <response code="400">User cannot be binded to the fridge</response>
        [HttpPost("BindUserToExistingFridge")]
        public async Task<FridgeInfo> BindUserToExistingFridge(BindUserToExistingFridgeCommand command)
        {
            return await mediator.Send(command);
        }

        /// <summary>
        /// Get list of user favorite products
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns>Collection of user favorites</returns>
        /// <response code="404">User not found</response>
        [HttpGet("{userId}/FavoriteProducts")]
        public async Task<ActionResult<IEnumerable<FavoriteProductDetailedInfo>>> GetUserFavoriteProducts(Guid userId)
        {
            var favoriteProducts = await mediator.Send(new GetUserFavoritesCommand(userId));
            return favoriteProducts is null ? NotFound() : Ok(favoriteProducts);
        }

        /// <summary>
        /// Get user favorite products which are out of stock
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns>Collection of user favorites out of stock</returns>
        /// <response code="404">User not found</response>
        [HttpGet("{userId}/OutOfStockFavoriteProducts")]
        public async Task<ActionResult<IEnumerable<FavoriteProductDetailedInfo>>> GetUserOutOfStockFavoriteProducts(Guid userId)
        {
            var outOfStockFavoriteProducts = await mediator.Send(new GetUserOutOfStockFavoritesCommand(userId));
            return outOfStockFavoriteProducts is null ? NotFound() : Ok(outOfStockFavoriteProducts);
        }
    }
}
