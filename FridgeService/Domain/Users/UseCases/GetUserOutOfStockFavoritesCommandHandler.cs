﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using FridgeService.WebApi.Domain.Fridges;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Products.UseCases
{
    public class GetUserOutOfStockFavoritesCommandHandler : IRequestHandler<GetUserOutOfStockFavoritesCommand, IEnumerable<FavoriteProductDetailedInfo>>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;
        private readonly FridgeStore fridgeStore;

        public GetUserOutOfStockFavoritesCommandHandler(
            IAppDbContext context,
            IMapper mapper,
            FridgeStore fridgeStore)
        {
            this.context = context;
            this.mapper = mapper;
            this.fridgeStore = fridgeStore;
        }

        public async Task<IEnumerable<FavoriteProductDetailedInfo>> Handle(GetUserOutOfStockFavoritesCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var user = await context.Users
                .Include(x => x.FavoriteProducts)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.UserId == request.UserId);
            if (user is null)
            {
                return null;
            }

            if (user.FavoriteProducts.Count == 0)
            {
                return Enumerable.Empty<FavoriteProductDetailedInfo>();
            }

            var userFridge = await fridgeStore.GetAsync(user.FridgeId);
            var productInStockIds = userFridge.Products.Values.Select(x => x.ProductId);
            var favoriteProductIds = user.FavoriteProducts.Select(x => x.ProductId);

            var productOutOfStockIds = favoriteProductIds.Except(productInStockIds);

            return await context.Products
                .Where(x => productOutOfStockIds.Contains(x.ProductId))
                .ProjectTo<FavoriteProductDetailedInfo>(mapper.ConfigurationProvider)
                .ToListAsync();
        }
    }
}
