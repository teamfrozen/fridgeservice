﻿using AutoMapper;
using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using FridgeService.WebApi.Domain.Fridges;
using FridgeService.WebApi.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Users.UseCases
{
    public class BindUserToExistingFridgeCommandHandler : IRequestHandler<BindUserToExistingFridgeCommand, FridgeInfo>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;
        private readonly FridgeStore fridgeStore;

        public BindUserToExistingFridgeCommandHandler(
            IAppDbContext context,
            IMapper mapper,
            FridgeStore fridgeStore)
        {
            this.context = context;
            this.mapper = mapper;
            this.fridgeStore = fridgeStore;
        }

        public async Task<FridgeInfo> Handle(BindUserToExistingFridgeCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var user = await context.Users.FindAsync(request.UserId);
            if (user is null)
            {
                throw new EntityDoesNotExistException<User>();
            }

            if (!await context.Fridges.AnyAsync(x => x.FridgeId == request.FridgeId))
            {
                throw new EntityDoesNotExistException<Fridge>();
            }

            if (user.FridgeId != request.FridgeId)
            {
                var isFridgeMustBeRemoved = await context.Fridges
                    .Include(x => x.Users)
                    .AnyAsync(x => x.FridgeId == user.FridgeId && x.Users.Count == 1);

                var oldFridgeId = user.FridgeId;
                user.FridgeId = request.FridgeId;

                await context.SaveChangesAsync();

                if (isFridgeMustBeRemoved)
                {
                    var fridgeToRemove = await fridgeStore.GetAsync(oldFridgeId);
                    await fridgeStore.RemoveFridge(fridgeToRemove);
                }
            }

            var fridge = await context.Fridges
                .Include(x => x.Users)
                .FirstOrDefaultAsync(x => x.FridgeId == user.FridgeId);
            return mapper.Map<FridgeInfo>(fridge);
        }
    }
}
