﻿using AutoMapper;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Users.UseCases
{
    public class GetUserFavoritesCommandHandler : IRequestHandler<GetUserFavoritesCommand, IEnumerable<FavoriteProductDetailedInfo>>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;

        public GetUserFavoritesCommandHandler(
            IAppDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<FavoriteProductDetailedInfo>> Handle(GetUserFavoritesCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var user = await context.Users
                .Include(x => x.FavoriteProducts)
                    .ThenInclude(x => x.Product)
                        .ThenInclude(x => x.ProductType)
                            .ThenInclude(x => x.Measure)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.UserId == request.UserId);
            if (user is null)
            {
                return null;
            }

            return mapper.Map<IEnumerable<FavoriteProductDetailedInfo>>(user.FavoriteProducts.Select(x => x.Product));
        }
    }
}
