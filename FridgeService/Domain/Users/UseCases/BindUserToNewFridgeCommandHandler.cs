﻿using AutoMapper;
using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using FridgeService.WebApi.Domain.Fridges;
using FridgeService.WebApi.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Users.UseCases
{
    public class BindUserToNewFridgeCommandHandler : IRequestHandler<BindUserToNewFridgeCommand, FridgeInfo>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;
        private readonly FridgeStore fridgeStore;

        public BindUserToNewFridgeCommandHandler(
            IAppDbContext context,
            IMapper mapper,
            FridgeStore fridgeStore)
        {
            this.context = context;
            this.mapper = mapper;
            this.fridgeStore = fridgeStore;
        }

        public async Task<FridgeInfo> Handle(BindUserToNewFridgeCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var user = await context.Users.FindAsync(request.UserId);
            if (user is null)
            {
                throw new EntityDoesNotExistException<User>();
            }

            var oldFridgeId = user.FridgeId;

            user.Fridge = new Fridge();
            context.Users.Update(user);

            await context.SaveChangesAsync();

            var isFridgeMustBeRemoved = await context.Fridges
                    .Include(x => x.Users)
                    .AnyAsync(x => x.FridgeId == oldFridgeId && x.Users.Count == 0);

            if (isFridgeMustBeRemoved)
            {
                var fridgeToRemove = await fridgeStore.GetAsync(oldFridgeId);
                await fridgeStore.RemoveFridge(fridgeToRemove);
            }

            var fridge = await context.Fridges
                .Include(x => x.Users)
                .FirstOrDefaultAsync(x => x.FridgeId == user.FridgeId);
            return mapper.Map<FridgeInfo>(fridge);
        }
    }
}
