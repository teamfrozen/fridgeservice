﻿using AutoMapper;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Users.UseCases
{
    public class GetUserCommandHandler : IRequestHandler<GetUserCommand, UserInfo>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;

        public GetUserCommandHandler(
            IAppDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<UserInfo> Handle(GetUserCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var user = await context.Users
                .FindAsync(request.UserId);

            return mapper.Map<UserInfo>(user);
        }
    }
}
