﻿using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.WebApi.Domain.Fridges;
using FridgeService.WebApi.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Users.UseCases
{
    public class RemoveUserCommandHandler : IRequestHandler<RemoveUserCommand, Unit>
    {
        private readonly IAppDbContext context;
        private readonly FridgeStore fridgeStore;

        public RemoveUserCommandHandler(
            IAppDbContext context,
            FridgeStore fridgeStore)
        {
            this.context = context;
            this.fridgeStore = fridgeStore;
        }

        public async Task<Unit> Handle(RemoveUserCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var userToRemove = await context.Users
                .Include(x => x.FavoriteProducts)
                .Include(x => x.Fridge)
                    .ThenInclude(x => x.Users)
                .FirstOrDefaultAsync(x => x.UserId == request.UserId);
            if (userToRemove is null)
            {
                throw new EntityDoesNotExistException<User>();
            }

            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    if (userToRemove.FavoriteProducts.Any())
                    {
                        userToRemove.FavoriteProducts.ForEach(x => context.FavoriteProducts.Remove(x));
                    }
                    context.Users.Remove(userToRemove);
                    await context.SaveChangesAsync();

                    if (userToRemove.Fridge != null && userToRemove.Fridge.Users.Count == 0)
                    {
                        context.Entry(userToRemove.Fridge).State = EntityState.Detached;
                        var fridgeToRemove = await fridgeStore.GetAsync(userToRemove.FridgeId);
                        await fridgeStore.RemoveFridge(fridgeToRemove);
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                }
            }

            return Unit.Value;
        }
    }
}
