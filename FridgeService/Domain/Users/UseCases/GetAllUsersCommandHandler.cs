﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Users.UseCases
{
    public class GetAllUsersCommandHandler : IRequestHandler<GetAllUsersCommand, IEnumerable<UserInfo>>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;

        public GetAllUsersCommandHandler(
            IAppDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<UserInfo>> Handle(GetAllUsersCommand request, CancellationToken cancellationToken)
        {
            return await context.Users
                .ProjectTo<UserInfo>(mapper.ConfigurationProvider)
                .ToListAsync();
        }
    }
}
