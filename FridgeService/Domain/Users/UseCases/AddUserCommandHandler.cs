﻿using AutoMapper;
using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using FridgeService.WebApi.Exceptions;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Users.UseCases
{
    public class AddUserCommandHandler : IRequestHandler<AddUserCommand, UserInfo>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;

        public AddUserCommandHandler(
            IAppDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<UserInfo> Handle(AddUserCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var userExists = context.Users.Any(x => x.UserId == request.UserId);
            if (userExists)
            {
                throw new EntityAlreadyExistsException<User>();
            }
            if (request.UserId == Guid.Empty)
            {
                throw new ArgumentEmptyException(nameof(request.UserId));
            }

            return request.FridgeId.HasValue
                ? await AddUserConnectedToExistingFridge(request)
                : await AddUserConnectedToNewFridge(request);
        }

        private async Task<UserInfo> AddUserConnectedToNewFridge(AddUserCommand request)
        {
            var fridge = context.Fridges.Add(new Fridge());

            var userToBeAdded = new User
            {
                UserId = request.UserId,
                FridgeId = fridge.Entity.FridgeId
            };

            context.Users.Add(userToBeAdded);
            await context.SaveChangesAsync();

            return mapper.Map<UserInfo>(userToBeAdded);
        }

        private async Task<UserInfo> AddUserConnectedToExistingFridge(AddUserCommand request)
        {
            var fridgeExists = context.Fridges.Any(x => x.FridgeId == request.FridgeId);
            if (!fridgeExists)
            {
                throw new EntityDoesNotExistException<Fridge>();
            }

            var userToBeAdded = new User
            {
                UserId = request.UserId,
                FridgeId = request.FridgeId.Value
            };

            context.Users.Add(userToBeAdded);
            await context.SaveChangesAsync();

            return mapper.Map<UserInfo>(userToBeAdded);
        }
    }
}
