﻿using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Products
{
    /// <summary>
    /// Controller for working with products
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IMediator mediator;

        public ProductsController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        /// <summary>
        /// Get all the products in the system
        /// </summary>
        /// <returns>All the products in system</returns>
        [HttpGet]
        public async Task<IEnumerable<ProductShortInfo>> GetProducts()
        {
            return await mediator.Send(new GetProductsCommand());
        }

        /// <summary>
        /// Get detailed information about a particular product
        /// </summary>
        /// <param name="productId">Product Id</param>
        /// <returns>Detailed product info</returns>
        /// <response code="404">Product not found</response>
        [HttpGet("{productId}")]
        public async Task<ActionResult<ProductDetailedInfo>> GetProduct(Guid productId)
        {
            var productInfo = await mediator.Send(new GetProductCommand(productId));

            return productInfo is null ? NotFound() : productInfo;
        }

        /// <summary>
        /// Get all the products of a particular type
        /// </summary>
        /// <param name="productTypeId">Product type id</param>
        /// <returns>Products of the type</returns>
        /// <response code="404">Product type not found</response>
        [HttpGet("OfType/{productTypeId}")]
        public async Task<ActionResult<IEnumerable<ProductShortInfo>>> GetProductsByType(Guid productTypeId)
        {
            var productsOfType = await mediator.Send(new GetProductsCommand(productTypeId));
            return productsOfType is null ? NotFound() : Ok(productsOfType);
        }

        /// <summary>
        /// Add a new product to the system (for debugging and possible future interactions with other services only)
        /// </summary>
        /// <param name="command">Information about the product needed for adding it</param>
        /// <returns>Detailed info about the added product</returns>
        /// <response code="400">Incorrect product info</response>
        [HttpPost]
        public async Task<ProductDetailedInfo> AddProduct(AddProductCommand command)
        {
            return await mediator.Send(command);
        }

        /// <summary>
        /// Update an existing product (for debugging and possible future interactions with other services only)
        /// </summary>
        /// <param name="command">Updated information about the product</param>
        /// <returns>Detailed info about the updated product</returns>
        /// <response code="400">Incorrect product info</response>
        [HttpPut]
        public async Task<ProductDetailedInfo> UpdateProduct(UpdateProductCommand command)
        {
            return await mediator.Send(command);
        }

        /// <summary>
        /// Remove a product from the system (for debugging and possible future interactions with other services only)
        /// </summary>
        /// <param name="productId">Product Id</param>
        /// <response code="400">Product cannot be removed</response>
        [HttpDelete("{productId}")]
        public async Task<Unit> RemoveProduct(Guid productId)
        {
            return await mediator.Send(new RemoveProductCommand(productId));
        }

        /// <summary>
        /// Select a product as a favorite for a user
        /// </summary>
        /// <param name="productId">Product Id</param>
        /// <param name="userId">User Id</param>
        /// <returns>Information about current state of the favoriteness of the product</returns>
        /// <response code="400">Incorrect input</response>
        [HttpPost("{productId}/AddToUserFavorites/{userId}")]
        public async Task<FavoriteProductShortInfo> SelectProductToBeFavorite(Guid productId, Guid userId)
        {
            return await mediator.Send(new SelectFavoriteProductCommand(userId, productId));
        }

        /// <summary>
        /// Remove a product from user favorites
        /// </summary>
        /// <param name="productId">Product Id</param>
        /// <param name="userId">User Id</param>
        /// <returns>Information about current state of the favoriteness of the product</returns>
        /// <response code="400">Incorrect input</response>
        [HttpPost("{productId}/RemoveFromUserFavorites/{userId}")]
        public async Task<FavoriteProductShortInfo> DeselectFavoriteProduct(Guid productId, Guid userId)
        {
            return await mediator.Send(new DeselectFavoriteProductCommand(userId, productId));
        }
    }
}
