﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Products.UseCases
{
    public class GetProductsCommandHandler : IRequestHandler<GetProductsCommand, IEnumerable<ProductShortInfo>>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;

        public GetProductsCommandHandler(
            IAppDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<ProductShortInfo>> Handle(GetProductsCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            return request.ProductTypeId.HasValue
                ? await GetProductsByType(request.ProductTypeId.Value)
                : await GetAllProducts();
        }

        private async Task<IEnumerable<ProductShortInfo>> GetAllProducts()
        {
            return await context.Products
                                .OrderBy(x => x.Name)
                                .ProjectTo<ProductShortInfo>(mapper.ConfigurationProvider)
                                .AsNoTracking()
                                .ToListAsync();
        }

        private async Task<IEnumerable<ProductShortInfo>> GetProductsByType(Guid productTypeId)
        {
            var productType = await context.ProductTypes.FindAsync(productTypeId);
            if (productType is null)
            {
                return null;
            }

            return await context.Products
                                .Where(x => x.ProductTypeId == productTypeId)
                                .OrderBy(x => x.Name)
                                .ProjectTo<ProductShortInfo>(mapper.ConfigurationProvider)
                                .AsNoTracking()
                                .ToListAsync();
        }
    }
}
