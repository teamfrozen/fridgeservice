﻿using AutoMapper;
using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using FridgeService.WebApi.Domain.Fridges.Services;
using FridgeService.WebApi.Domain.Products.Services;
using FridgeService.WebApi.Exceptions;
using FridgeService.WebApi.Extensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Products.UseCases
{
    public class UpdateProductCommandHandler : IRequestHandler<UpdateProductCommand, ProductDetailedInfo>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;
        private readonly IShelfLifeInferingService shelfLifeInferingService;
        private readonly IProductTypeInferingService productTypeInferingService;

        public UpdateProductCommandHandler(
            IAppDbContext context,
            IMapper mapper,
            IShelfLifeInferingService shelfLifeInferingService,
            IProductTypeInferingService productTypeInferingService)
        {
            this.context = context;
            this.mapper = mapper;
            this.shelfLifeInferingService = shelfLifeInferingService;
            this.productTypeInferingService = productTypeInferingService;
        }

        public async Task<ProductDetailedInfo> Handle(UpdateProductCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            if (string.IsNullOrWhiteSpace(request.ProductName))
            {
                throw new ArgumentEmptyException(nameof(request.ProductName));
            }
            if (request.Quantity.HasValue && request.Quantity < 0)
            {
                throw new NegativeProductQuantityException();
            }
            if (request.ShelfLifeDuration.HasValue && (request.ShelfLifeDuration.Value.Hours < 0 || request.ShelfLifeDuration.Value.Days < 0))
            {
                throw new NegativeShelfLifeException();
            }

            var productToUpdate = await context.Products
                .FirstOrDefaultAsync(x => x.ProductId == request.ProductId);
            if (productToUpdate is null)
            {
                throw new EntityDoesNotExistException<Product>();
            }

            var productType = request.ProductTypeId.HasValue
                ? await context.ProductTypes.FirstOrDefaultAsync(x => x.ProductTypeId == request.ProductTypeId.Value)
                : await productTypeInferingService.GetProductType(request.ProductName);
            if (productType is null)
            {
                throw new EntityDoesNotExistException<ProductType>();
            }

            var productAlreadyExists = (await context.Products
                .Where(x => x.ProductTypeId == productType.ProductTypeId && x.ProductId != request.ProductId)
                .ToListAsync())
                .Any(x => x.Name.Equals(request.ProductName, StringComparison.CurrentCultureIgnoreCase));
            if (productAlreadyExists)
            {
                throw new EntityAlreadyExistsException<Product>();
            }

            var shelfLifeInTicks = request.ShelfLifeDuration.HasValue
                ? request.ShelfLifeDuration.Value.GetTicks()
                : shelfLifeInferingService.GetExpirationTerm(productType).GetTicks();

            productToUpdate.ProductTypeId = productType.ProductTypeId;
            productToUpdate.Quantity = request.Quantity;
            productToUpdate.Name = request.ProductName;
            productToUpdate.ShelfLifeDuration = shelfLifeInTicks;
            context.Products.Update(productToUpdate);

            await context.SaveChangesAsync(cancellationToken);

            await context.LoadAdditionalInfoForOutput(productToUpdate);
            return mapper.Map<ProductDetailedInfo>(productToUpdate);
        }
    }
}
