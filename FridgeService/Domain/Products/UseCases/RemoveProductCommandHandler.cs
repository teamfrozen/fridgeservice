﻿using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.WebApi.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Products.UseCases
{
    public class RemoveProductCommandHandler : IRequestHandler<RemoveProductCommand, Unit>
    {
        private readonly IAppDbContext context;

        public RemoveProductCommandHandler(IAppDbContext context)
        {
            this.context = context;
        }

        public async Task<Unit> Handle(RemoveProductCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var productToRemove = await context.Products
                .Include(x => x.ProductInFridges)
                .Include(x => x.UserFavorites)
                .FirstOrDefaultAsync(x => x.ProductId == request.ProductId);
            if (productToRemove is null)
            {
                throw new EntityDoesNotExistException<Product>();
            }
            if (productToRemove.ProductInFridges.Any())
            {
                throw new ImpossibleEntityRemovementException("Product exists in some fridges");
            }
            if (productToRemove.UserFavorites.Any())
            {
                throw new ImpossibleEntityRemovementException("Product exists in favorites list of some users");
            }

            context.Products.Remove(productToRemove);
            await context.SaveChangesAsync();

            return Unit.Value;
        }
    }
}
