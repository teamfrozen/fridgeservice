﻿using AutoMapper;
using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using FridgeService.WebApi.Domain.Fridges.Services;
using FridgeService.WebApi.Domain.Products.Services;
using FridgeService.WebApi.Exceptions;
using FridgeService.WebApi.Extensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Products.UseCases
{
    public class AddProductCommandHandler : IRequestHandler<AddProductCommand, ProductDetailedInfo>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;
        private readonly IShelfLifeInferingService shelfLifeInferingService;
        private readonly IProductTypeInferingService productTypeInferingService;

        public AddProductCommandHandler(
            IAppDbContext context,
            IMapper mapper,
            IShelfLifeInferingService shelfLifeInferingService,
            IProductTypeInferingService productTypeInferingService)
        {
            this.context = context;
            this.mapper = mapper;
            this.shelfLifeInferingService = shelfLifeInferingService;
            this.productTypeInferingService = productTypeInferingService;
        }

        public async Task<ProductDetailedInfo> Handle(AddProductCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            if (string.IsNullOrWhiteSpace(request.ProductName))
            {
                throw new ArgumentEmptyException(nameof(request.ProductName));
            }
            if (request.Quantity.HasValue && request.Quantity < 0)
            {
                throw new NegativeProductQuantityException();
            }
            if (request.ShelfLifeDuration.HasValue && (request.ShelfLifeDuration.Value.Hours < 0 || request.ShelfLifeDuration.Value.Days < 0))
            {
                throw new NegativeShelfLifeException();
            }

            var productType = request.ProductTypeId.HasValue
                ? await context.ProductTypes.FindAsync(request.ProductTypeId.Value)
                : await productTypeInferingService.GetProductType(request.ProductName);
            if (productType is null)
            {
                throw new EntityDoesNotExistException<ProductType>();
            }

            var productAlreadyExists = (await context.Products
                .Where(x => x.ProductTypeId == productType.ProductTypeId)
                .ToListAsync())
                .Any(x => x.Name.Equals(request.ProductName, StringComparison.CurrentCultureIgnoreCase));
            if (productAlreadyExists)
            {
                throw new EntityAlreadyExistsException<Product>();
            }

            var addedProduct = await AddNewProduct(request.ProductName, productType, request.ShelfLifeDuration, request.Quantity);

            await context.LoadAdditionalInfoForOutput(addedProduct);

            return mapper.Map<ProductDetailedInfo>(addedProduct);
        }

        private async Task<Product> AddNewProduct(string productName, ProductType productType, ExpirationTerm? shelfLifeDuration, double? quantity)
        {
            var shelfLifeInTicks = shelfLifeDuration.HasValue
                ? shelfLifeDuration.Value.GetTicks()
                : shelfLifeInferingService.GetExpirationTerm(productType).GetTicks();

            var product = new Product
            {
                Name = productName,
                ShelfLifeDuration = shelfLifeInTicks,
                ProductTypeId = productType.ProductTypeId,
                Quantity = quantity
            };

            var addedProduct = context.Products.Add(product);
            await context.SaveChangesAsync();

            return addedProduct.Entity;
        }
    }
}
