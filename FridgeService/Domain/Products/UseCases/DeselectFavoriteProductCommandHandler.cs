﻿using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using FridgeService.WebApi.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Products.UseCases
{
    public class DeselectFavoriteProductCommandHandler : IRequestHandler<DeselectFavoriteProductCommand, FavoriteProductShortInfo>
    {
        private readonly IAppDbContext context;

        public DeselectFavoriteProductCommandHandler(IAppDbContext context)
        {
            this.context = context;
        }

        public async Task<FavoriteProductShortInfo> Handle(DeselectFavoriteProductCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var user = await context.Users
                .Include(x => x.FavoriteProducts)
                .FirstOrDefaultAsync(x => x.UserId == request.UserId);
            if (user is null)
            {
                throw new EntityDoesNotExistException<User>();
            }

            var productToBecomeFavorite = await context.Products.FindAsync(request.ProductId);
            if (productToBecomeFavorite is null)
            {
                throw new EntityDoesNotExistException<Product>();
            }

            var favoriteProduct = user.FavoriteProducts.FirstOrDefault(x => x.ProductId == productToBecomeFavorite.ProductId);
            if (favoriteProduct != null)
            {
                context.FavoriteProducts.Remove(favoriteProduct);

                await context.SaveChangesAsync();
            }

            return new FavoriteProductShortInfo(request.ProductId, isFavorite: false);
        }
    }
}
