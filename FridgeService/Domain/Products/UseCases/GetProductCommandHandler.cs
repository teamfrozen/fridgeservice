﻿using AutoMapper;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Products.UseCases
{
    public class GetProductCommandHandler : IRequestHandler<GetProductCommand, ProductDetailedInfo>
    {
        private readonly IAppDbContext context;
        private readonly IMapper mapper;

        public GetProductCommandHandler(
            IAppDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<ProductDetailedInfo> Handle(GetProductCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var product = await context.Products
                .Include(x => x.ProductType)
                    .ThenInclude(x => x.Measure)
                .FirstOrDefaultAsync(x => x.ProductId == request.ProductId);

            return mapper.Map<ProductDetailedInfo>(product);
        }
    }
}
