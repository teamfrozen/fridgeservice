﻿using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using FridgeService.WebApi.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Products.UseCases
{
    public class SelectFavoriteProductCommandHandler : IRequestHandler<SelectFavoriteProductCommand, FavoriteProductShortInfo>
    {
        private readonly IAppDbContext context;

        public SelectFavoriteProductCommandHandler(IAppDbContext context)
        {
            this.context = context;
        }

        public async Task<FavoriteProductShortInfo> Handle(SelectFavoriteProductCommand request, CancellationToken cancellationToken)
        {
            if (request is null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var user = await context.Users
                .Include(x => x.FavoriteProducts)
                .FirstOrDefaultAsync(x => x.UserId == request.UserId);
            if (user is null)
            {
                throw new EntityDoesNotExistException<User>();
            }

            var productToBecomeFavorite = await context.Products.FindAsync(request.ProductId);
            if (productToBecomeFavorite is null)
            {
                throw new EntityDoesNotExistException<Product>();
            }

            var alreadyFavoriteProduct = user.FavoriteProducts.FirstOrDefault(x => x.ProductId == productToBecomeFavorite.ProductId);
            if (alreadyFavoriteProduct is null)
            {
                var userFavoriteProduct = new UserFavoriteProduct
                {
                    ProductId = productToBecomeFavorite.ProductId,
                    UserId = user.UserId
                };
                context.FavoriteProducts.Add(userFavoriteProduct);

                await context.SaveChangesAsync();
            }

            return new FavoriteProductShortInfo(request.ProductId, isFavorite: true);
        }
    }
}
