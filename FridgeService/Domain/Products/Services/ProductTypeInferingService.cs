﻿using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.WebApi.Exceptions;
using FridgeService.WebApi.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Products.Services
{
    public class ProductTypeInferingService : IProductTypeInferingService
    {
        private readonly IAppDbContext context;
        private readonly ProductTypeSettings productTypeSettings;

        public ProductTypeInferingService(
            IAppDbContext context,
            IOptions<ProductTypeSettings> productTypeSettings)
        {
            this.context = context;
            this.productTypeSettings = productTypeSettings.Value;
        }

        public async Task<ProductType> GetProductType(string productName)
        {
            if (string.IsNullOrWhiteSpace(productName))
            {
                throw new ArgumentEmptyException(nameof(productName));
            }

            var productTypes = await context.ProductTypes
                .Include(x => x.ProductAliases)
                .AsNoTracking()
                .ToListAsync();

            foreach (var productType in productTypes)
            {
                foreach (var productAlias in productType.ProductAliases)
                {
                    if (productName.Contains(productAlias.Name, StringComparison.CurrentCultureIgnoreCase))
                    {
                        return productType;
                    }
                }
            }

            return productTypes.FirstOrDefault(x => x.ProductTypeId == productTypeSettings.UndefinedTypeId);
        }
    }
}
