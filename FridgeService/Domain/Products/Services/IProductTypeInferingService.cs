﻿using FridgeService.Domain;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Domain.Products.Services
{
    public interface IProductTypeInferingService
    {
        Task<ProductType> GetProductType(string productName);
    }
}