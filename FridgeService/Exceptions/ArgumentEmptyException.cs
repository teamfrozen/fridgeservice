﻿using FridgeService.WebApi.Extensions;
using System;

namespace FridgeService.WebApi.Exceptions
{
    /// <summary>
    /// The exception that is thrown when an argument passed by user is empty or null
    /// </summary>
    public class ArgumentEmptyException : Exception
    {
        /// <summary>
        /// Initializes a new instance of an exception with parameter name which is empty
        /// </summary>
        /// <param name="parameterName">Parameter name</param>
        public ArgumentEmptyException(string parameterName)
            : base($"The {parameterName.PrepareMemberNameForOutput()} cannot be empty")
        {

        }
    }
}
