﻿using FridgeService.WebApi.Extensions;
using System;

namespace FridgeService.WebApi.Exceptions
{
    /// <summary>
    /// The exception that is thrown when an entity does not exist in database and, thus, cannot be used
    /// </summary>
    /// <typeparam name="T">Type of entity</typeparam>
    public class EntityDoesNotExistException<T> : Exception
    {
        /// <summary>
        /// Initializes a new instance of an exception
        /// </summary>
        public EntityDoesNotExistException()
            : base($"The {typeof(T).Name.PrepareMemberNameForOutput()} does not exist")
        {

        }
    }
}
