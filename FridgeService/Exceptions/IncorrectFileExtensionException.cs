﻿using System;
using System.Collections.Generic;

namespace FridgeService.WebApi.Exceptions
{
    /// <summary>
    /// The exception than is thrown when a chosen image file has unsupported/incorrect extension
    /// </summary>
    public class IncorrectFileExtensionException : Exception
    {
        /// <summary>
        /// Initializes a new instance of an exception with a collection of all available extensions
        /// </summary>
        /// <param name="availableFileExtensions">Collection of all available file extensions for an image file</param>
        public IncorrectFileExtensionException(IEnumerable<string> availableFileExtensions)
            : base("The file has incorrect extension. Possible extensions include " + string.Join(", ", availableFileExtensions))
        {

        }
    }
}
