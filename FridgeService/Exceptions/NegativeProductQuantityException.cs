﻿using System;

namespace FridgeService.WebApi.Exceptions
{
    /// <summary>
    /// The exception that is thrown when entered product quantity is negative
    /// </summary>
    public class NegativeProductQuantityException : Exception
    {
        /// <summary>
        /// Initializes a new instance of an exception
        /// </summary>
        public NegativeProductQuantityException()
            : base("Product quantity cannot be negative")
        {

        }
    }
}
