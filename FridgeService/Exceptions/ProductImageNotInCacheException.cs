﻿using System;

namespace FridgeService.WebApi.Exceptions
{
    /// <summary>
    /// The exception that is thrown when a product image is not in cache and should be reuploaded
    /// </summary>
    public class ProductImageNotInCacheException : Exception
    {
        /// <summary>
        /// Initializes a new instance of an exception
        /// </summary>
        public ProductImageNotInCacheException()
            : base("Product image does not exist in cache, try upload it again")
        {

        }
    }
}
