﻿using System;

namespace FridgeService.WebApi.Exceptions
{
    /// <summary>
    /// The exception that is thrown when the entered shelf life of a product or a product type is negative
    /// </summary>
    public class NegativeShelfLifeException : Exception
    {
        /// <summary>
        /// Initializes a new instance of an exception
        /// </summary>
        public NegativeShelfLifeException()
            : base("Shelf life must be positive")
        {

        }
    }
}
