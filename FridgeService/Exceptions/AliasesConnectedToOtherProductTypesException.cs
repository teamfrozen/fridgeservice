﻿using System;
using System.Collections.Generic;

namespace FridgeService.WebApi.Exceptions
{
    /// <summary>
    /// The exception that is thrown when product type aliases which user inputs are already connected to other product types,
    /// that is why they can be used for current product type
    /// </summary>
    public class AliasesConnectedToOtherProductTypesException : Exception
    {
        /// <summary>
        /// Initializes a new instance of an exception with the list of product aliases which are used by other product types
        /// </summary>
        /// <param name="productAliases">Product aliases used by other product types</param>
        public AliasesConnectedToOtherProductTypesException(IEnumerable<string> productAliases)
            : base($"Product aliases {string.Join(',', productAliases)} have already been connected to other product types")
        {

        }
    }
}
