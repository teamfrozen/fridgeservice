﻿using System;

namespace FridgeService.WebApi.Exceptions
{
    /// <summary>
    /// The exception that is thrown when the expiration date of the product in fridge is earlier than today date
    /// </summary>
    public class ExpirationDateEarlierThanTodayException : Exception
    {
        /// <summary>
        /// Initializes a new instance of an exception
        /// </summary>
        public ExpirationDateEarlierThanTodayException()
            : base("Expiration date should not be earlier than today")
        {

        }
    }
}
