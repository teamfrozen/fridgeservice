﻿using FridgeService.WebApi.Extensions;
using System;

namespace FridgeService.WebApi.Exceptions
{
    /// <summary>
    /// The exception that is thrown when an entity already exists in database and cannot be inserted once more
    /// </summary>
    /// <typeparam name="T">Type of entity</typeparam>
    public class EntityAlreadyExistsException<T> : Exception
    {
        /// <summary>
        /// Initializes a new instance of an exception
        /// </summary>
        public EntityAlreadyExistsException()
            : base($"The {typeof(T).Name.PrepareMemberNameForOutput()} already exists")
        {

        }
    }
}
