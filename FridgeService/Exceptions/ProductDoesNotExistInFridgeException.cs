﻿using System;

namespace FridgeService.WebApi.Exceptions
{
    /// <summary>
    /// The exception that is thrown when a product does not exist in fridge
    /// </summary>
    public class ProductDoesNotExistInFridgeException : Exception
    {
        /// <summary>
        /// Initializes a new instance of an exception
        /// </summary>
        public ProductDoesNotExistInFridgeException()
            : base("Product does not exist in fridge now")
        {

        }
    }
}
