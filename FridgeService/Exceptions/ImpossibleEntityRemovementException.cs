﻿using System;

namespace FridgeService.WebApi.Exceptions
{
    /// <summary>
    /// The exception that is thrown than an entity cannot be removed
    /// </summary>
    public class ImpossibleEntityRemovementException : Exception
    {
        /// <summary>
        /// Initializes a new instance of an exception with a message reasoning why the entity cannot be removed
        /// </summary>
        /// <param name="message">Reason for impossibility of removing an entity</param>
        public ImpossibleEntityRemovementException(string message)
            : base(message + ". Removement is impossible")
        {

        }
    }
}
