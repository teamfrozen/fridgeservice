﻿using FridgeService.Domain;
using FridgeService.Domain.Context;
using System;
using System.Threading.Tasks;

namespace FridgeService.WebApi.Extensions
{
    /// <summary>
    /// Db context extensions
    /// </summary>
    public static class AppDbContextExtensions
    {
        /// <summary>
        /// Load additional information for a product in fridge from Db
        /// </summary>
        /// <param name="context">Db context</param>
        /// <param name="productInFridge">Instance of a product in fridge</param>
        public static async Task LoadAdditionalInfoForOutput(this IAppDbContext context, ProductInFridge productInFridge)
        {
            if (productInFridge is null)
            {
                throw new ArgumentNullException(nameof(productInFridge));
            }

            await context.Entry(productInFridge)
                .Reference(x => x.Product)
                .LoadAsync();
            await context.Entry(productInFridge.Product)
                .Reference(x => x.ProductType)
                .LoadAsync();
            await context.Entry(productInFridge.Product.ProductType)
                .Reference(x => x.Measure)
                .LoadAsync();
        }

        /// <summary>
        /// Load additional information for a product type from Db
        /// </summary>
        /// <param name="context">Db context</param>
        /// <param name="productType">Instance of a product type</param>
        public static async Task LoadAdditionalInfoForOutput(this IAppDbContext context, ProductType productType)
        {
            if (productType is null)
            {
                throw new ArgumentNullException(nameof(productType));
            }

            await context.Entry(productType)
                .Reference(x => x.Measure)
                .LoadAsync();
            await context.Entry(productType)
                .Collection(x => x.ProductAliases)
                .LoadAsync();
        }

        /// <summary>
        /// Load additional information for a product from Db
        /// </summary>
        /// <param name="context">Db context</param>
        /// <param name="product">Instance of a product</param>
        public static async Task LoadAdditionalInfoForOutput(this IAppDbContext context, Product product)
        {
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }

            await context.Entry(product)
                .Reference(x => x.ProductType)
                .LoadAsync();
            await context.Entry(product.ProductType)
                .Reference(x => x.Measure)
                .LoadAsync();
        }
    }
}
