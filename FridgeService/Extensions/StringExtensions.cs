﻿using System;
using System.Globalization;
using System.Text;

namespace FridgeService.WebApi.Extensions
{
    /// <summary>
    /// String extensions
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Get a member name appropriate for output to other services or users
        /// </summary>
        /// <param name="sourceMemberName">Member name which should be transformed</param>
        /// <returns>Transformed member name appropriate for output</returns>
        public static string PrepareMemberNameForOutput(this string sourceMemberName)
        {
            var builder = new StringBuilder();
            foreach (char c in sourceMemberName)
            {
                if (Char.IsUpper(c) && builder.Length > 0) builder.Append(' ');
                builder.Append(Char.ToLower(c, CultureInfo.CurrentCulture));
            }

            return builder.ToString();
        }
    }
}
