﻿using AutoMapper;
using FridgeService.Domain;
using FridgeService.Transport.Responses;
using System;
using System.Linq;

namespace FridgeService.WebApi
{
    /// <summary>
    /// AutoMapper profile for mapping (mainly for preparing output)
    /// </summary>
    public class MappingProfile : Profile
    {
        /// <summary>
        /// Initialize AutoMapper profile
        /// </summary>
        public MappingProfile()
        {
            CreateMap<Fridge, FridgeInfo>()
                .ForMember(d => d.Users, opt => opt.MapFrom(s => s.Users.Select(x => x.UserId)));
            CreateMap<User, UserInfo>();
            CreateMap<ProductInFridge, ProductInFridgeInfo>()
                .ForMember(d => d.DepositDate, opt => opt.MapFrom(s => s.DepositDate))
                .ForMember(d => d.ExpirationDate, opt => opt.MapFrom(s => s.ExpirationDate))
                .ForMember(d => d.MeasureName, opt => opt.MapFrom(s => s.Product.ProductType.Measure == null ? string.Empty : s.Product.ProductType.Measure.Name))
                .ForMember(d => d.ProductId, opt => opt.MapFrom(s => s.ProductId))
                .ForMember(d => d.ProductInFridgeId, opt => opt.MapFrom(s => s.ProductInFridgeId))
                .ForMember(d => d.ProductName, opt => opt.MapFrom(s => s.Product.Name))
                .ForMember(d => d.ProductTypeId, opt => opt.MapFrom(s => s.Product.ProductTypeId))
                .ForMember(d => d.ProductTypeName, opt => opt.MapFrom(s => s.Product.ProductType.Name))
                .ForMember(d => d.Quantity, opt => opt.MapFrom(s => s.Product.Quantity))
                .ForMember(d => d.ProductImagePath, opt => opt.MapFrom(s => s.Product.ProductType.ProductImagePath));
            CreateMap<ProductInFridgeInfo, ProductInFridge>()
                .ForMember(d => d.DepositDate, opt => opt.MapFrom(s => s.DepositDate))
                .ForMember(d => d.ExpirationDate, opt => opt.MapFrom(s => s.ExpirationDate))
                .ForMember(d => d.FridgeId, opt => opt.MapFrom(s => Guid.Empty))
                .ForMember(d => d.ProductId, opt => opt.MapFrom(s => s.ProductId))
                .ForMember(d => d.ProductInFridgeId, opt => opt.MapFrom(s => s.ProductInFridgeId));
            CreateMap<ProductType, ProductTypeShortInfo>()
                .ForMember(d => d.ProductTypeId, opt => opt.MapFrom(s => s.ProductTypeId))
                .ForMember(d => d.ProductTypeName, opt => opt.MapFrom(s => s.Name));
            CreateMap<ProductType, ProductTypeDetailedInfo>()
                .ForMember(d => d.AverageShelfLifeDuration, opt => opt.MapFrom(s => s.AvgShelfLifeDuration.HasValue ? new ExpirationTerm(s.AvgShelfLifeDuration.Value) : (ExpirationTerm?)null))
                .ForMember(d => d.MeasureId, opt => opt.MapFrom(s => s.MeasureId))
                .ForMember(d => d.ProductAliases, opt => opt.MapFrom(s => s.ProductAliases.Select(x => x.Name).OrderBy(x => x)))
                .ForMember(d => d.ProductImagePath, opt => opt.MapFrom(s => s.ProductImagePath))
                .ForMember(d => d.ProductTypeId, opt => opt.MapFrom(s => s.ProductTypeId))
                .ForMember(d => d.ProductTypeName, opt => opt.MapFrom(s => s.Name))
                .ForMember(d => d.MeasureName, opt => opt.MapFrom(s => s.Measure.Name));
            CreateMap<Product, ProductShortInfo>()
                .ForMember(d => d.ProductId, opt => opt.MapFrom(s => s.ProductId))
                .ForMember(d => d.ProductName, opt => opt.MapFrom(s => s.Name));
            CreateMap<Product, ProductDetailedInfo>()
                .ForMember(d => d.MeasureId, opt => opt.MapFrom(s => s.ProductType.MeasureId))
                .ForMember(d => d.MeasureName, opt => opt.MapFrom(s => s.ProductType.Measure == null ? string.Empty : s.ProductType.Measure.Name))
                .ForMember(d => d.ProductId, opt => opt.MapFrom(s => s.ProductId))
                .ForMember(d => d.ProductImagePath, opt => opt.MapFrom(s => s.ProductType.ProductImagePath))
                .ForMember(d => d.ProductName, opt => opt.MapFrom(s => s.Name))
                .ForMember(d => d.ProductTypeId, opt => opt.MapFrom(s => s.ProductTypeId))
                .ForMember(d => d.ProductTypeName, opt => opt.MapFrom(s => s.ProductType.Name))
                .ForMember(d => d.Quantity, opt => opt.MapFrom(s => s.Quantity))
                .ForMember(d => d.ShelfLifeDuration, opt => opt.MapFrom(s => s.ShelfLifeDuration.HasValue ? new ExpirationTerm(s.ShelfLifeDuration.Value) : (ExpirationTerm?)null));
            CreateMap<Product, FavoriteProductDetailedInfo>()
                .ForMember(d => d.IsFavorite, opt => opt.MapFrom(s => true))
                .ForMember(d => d.MeasureName, opt => opt.MapFrom(s => s.ProductType.Measure == null ? string.Empty : s.ProductType.Measure.Name))
                .ForMember(d => d.ProductId, opt => opt.MapFrom(s => s.ProductId))
                .ForMember(d => d.ProductImagePath, opt => opt.MapFrom(s => s.ProductType.ProductImagePath))
                .ForMember(d => d.ProductName, opt => opt.MapFrom(s => s.Name))
                .ForMember(d => d.ProductTypeId, opt => opt.MapFrom(s => s.ProductTypeId))
                .ForMember(d => d.ProductTypeName, opt => opt.MapFrom(s => s.ProductType.Name))
                .ForMember(d => d.Quantity, opt => opt.MapFrom(s => s.Quantity));
            CreateMap<Measure, MeasureShortInfo>()
                .ForMember(d => d.MeasureId, opt => opt.MapFrom(s => s.MeasureId))
                .ForMember(d => d.MeasureName, opt => opt.MapFrom(s => s.Name));
        }
    }
}
