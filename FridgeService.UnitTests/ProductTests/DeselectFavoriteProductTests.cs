﻿using FridgeService.Domain;
using FridgeService.Transport.Commands;
using FridgeService.WebApi.Domain.Products.UseCases;
using FridgeService.WebApi.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.UnitTests.ProductTests
{
    [TestClass]
    public class DeselectFavoriteProductTests : AbstractProductTests
    {
        [TestMethod]
        public async Task DeselectExistingFavoriteProduct()
        {
            var user = await context.Users.FirstOrDefaultAsync();
            var sourceProduct = await context.Products
                .Include(x => x.UserFavorites)
                .FirstOrDefaultAsync(y => y.UserFavorites.Any(x => x.UserId == user.UserId));

            var command = new DeselectFavoriteProductCommand(user.UserId, sourceProduct.ProductId);
            var handler = new DeselectFavoriteProductCommandHandler(context);

            var favoriteProduct = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(sourceProduct.ProductId, favoriteProduct.ProductId);
            Assert.IsFalse(favoriteProduct.IsFavorite);
        }

        [TestMethod]
        public async Task DeselectExistingNotFavoriteProduct()
        {
            var user = await context.Users.FirstOrDefaultAsync();
            var sourceProduct = await context.Products
                .Include(x => x.UserFavorites)
                .FirstOrDefaultAsync(y => !y.UserFavorites.Any(x => x.UserId == user.UserId));

            var command = new DeselectFavoriteProductCommand(user.UserId, sourceProduct.ProductId);
            var handler = new DeselectFavoriteProductCommandHandler(context);

            var favoriteProduct = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(sourceProduct.ProductId, favoriteProduct.ProductId);
            Assert.IsFalse(favoriteProduct.IsFavorite);
        }

        [TestMethod]
        public async Task DeselectNonexistentProduct()
        {
            var user = await context.Users.FirstOrDefaultAsync();
            var nonexistentProductId = Guid.NewGuid();
            while (await context.Products.AnyAsync(x => x.ProductId == nonexistentProductId))
            {
                nonexistentProductId = Guid.NewGuid();
            }

            var command = new DeselectFavoriteProductCommand(user.UserId, nonexistentProductId);
            var handler = new DeselectFavoriteProductCommandHandler(context);

            await Assert.ThrowsExceptionAsync<EntityDoesNotExistException<Product>>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task DeselectExistingNotFavoriteProductForNonexistentUser()
        {
            var nonexistentUserId = Guid.NewGuid();
            while (await context.Users.AnyAsync(x => x.UserId == nonexistentUserId))
            {
                nonexistentUserId = Guid.NewGuid();
            }

            var sourceProduct = await context.Products
                .FirstOrDefaultAsync();

            var command = new DeselectFavoriteProductCommand(nonexistentUserId, sourceProduct.ProductId);
            var handler = new DeselectFavoriteProductCommandHandler(context);

            await Assert.ThrowsExceptionAsync<EntityDoesNotExistException<User>>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }
    }
}
