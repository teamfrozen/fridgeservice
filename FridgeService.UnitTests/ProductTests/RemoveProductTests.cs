﻿using FridgeService.Domain;
using FridgeService.Transport.Commands;
using FridgeService.WebApi.Domain.Products.UseCases;
using FridgeService.WebApi.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.UnitTests.ProductTests
{
    [TestClass]
    public class RemoveProductTests : AbstractProductTests
    {
        [TestMethod]
        public async Task RemoveExistingUnusedProduct()
        {
            var sourceProduct = await context.Products
                .Include(x => x.ProductInFridges)
                .Include(x => x.UserFavorites)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => !x.UserFavorites.Any() && !x.ProductInFridges.Any());

            var command = new RemoveProductCommand(sourceProduct.ProductId);
            var handler = new RemoveProductCommandHandler(context);

            await handler.Handle(command, new CancellationToken());

            Assert.IsFalse(await context.Products.AnyAsync(x => x.ProductId == sourceProduct.ProductId));
        }

        [TestMethod]
        public async Task RemoveExistingProductWhichIsFavoriteForSomeone()
        {
            var productsInFridges = await context.ProductsInFridge.ToListAsync();
            context.ProductsInFridge.RemoveRange(productsInFridges);
            await context.SaveChangesAsync();

            var sourceProduct = await context.Products
                .Include(x => x.ProductInFridges)
                .Include(x => x.UserFavorites)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.UserFavorites.Any());

            var command = new RemoveProductCommand(sourceProduct.ProductId);
            var handler = new RemoveProductCommandHandler(context);

            await Assert.ThrowsExceptionAsync<ImpossibleEntityRemovementException>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task RemoveExistingProductWhichIsCurrentlyInFridge()
        {
            var favoriteProducts = await context.FavoriteProducts.ToListAsync();
            context.FavoriteProducts.RemoveRange(favoriteProducts);
            await context.SaveChangesAsync();

            var sourceProduct = await context.Products
                .Include(x => x.ProductInFridges)
                .Include(x => x.UserFavorites)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.ProductInFridges.Any());

            var command = new RemoveProductCommand(sourceProduct.ProductId);
            var handler = new RemoveProductCommandHandler(context);

            await Assert.ThrowsExceptionAsync<ImpossibleEntityRemovementException>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task RemoveNonexistentProduct()
        {
            var nonexistentProductId = Guid.NewGuid();
            while (await context.Products.AnyAsync(x => x.ProductId == nonexistentProductId))
            {
                nonexistentProductId = Guid.NewGuid();
            }

            var command = new RemoveProductCommand(nonexistentProductId);
            var handler = new RemoveProductCommandHandler(context);

            await Assert.ThrowsExceptionAsync<EntityDoesNotExistException<Product>>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }
    }
}
