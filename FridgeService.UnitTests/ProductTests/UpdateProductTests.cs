﻿using FridgeService.Domain;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using FridgeService.WebApi.Domain.Products.UseCases;
using FridgeService.WebApi.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.UnitTests.ProductTests
{
    [TestClass]
    public class UpdateProductTests : AbstractProductTests
    {
        [TestMethod]
        public async Task UpdateExistingProduct()
        {
            var product = await context.Products
                .FirstOrDefaultAsync();
            var targetProductType = await context.ProductTypes
                .Include(x => x.Measure)
                .FirstOrDefaultAsync(x => x.ProductTypeId != product.ProductTypeId);

            var command = new UpdateProductCommand
            {
                ProductId = product.ProductId,
                ProductName = "Milky way",
                Quantity = 120,
                ShelfLifeDuration = new ExpirationTerm(365, 0),
                ProductTypeId = targetProductType.ProductTypeId
            };
            var handler = new UpdateProductCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService);

            var addedProductType = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(command.ProductId, addedProductType.ProductId);
            Assert.AreEqual(command.ProductTypeId, addedProductType.ProductTypeId);
            Assert.AreEqual(targetProductType.MeasureId, addedProductType.MeasureId);
            Assert.AreEqual(targetProductType.Measure.Name, addedProductType.MeasureName);
            Assert.AreEqual(targetProductType.ProductImagePath, addedProductType.ProductImagePath);
            Assert.AreEqual(command.ProductName, addedProductType.ProductName);
            Assert.AreEqual(command.Quantity, addedProductType.Quantity);
            Assert.AreEqual(command.ShelfLifeDuration, addedProductType.ShelfLifeDuration);
            Assert.IsTrue(await context.Products.AnyAsync(x => x.ProductId == addedProductType.ProductId
                && x.ProductTypeId == command.ProductTypeId
                && x.Name == command.ProductName
                && x.Quantity == command.Quantity
                && x.ShelfLifeDuration == command.ShelfLifeDuration.Value.GetTicks()));
        }

        [TestMethod]
        public async Task UpdateExistingProductWithNullType()
        {
            var product = await context.Products
                .AsNoTracking()
                .FirstOrDefaultAsync();
            var inferedProductType = await context.ProductTypes
                .Include(x => x.Measure)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Name == "Milk");

            var command = new UpdateProductCommand
            {
                ProductId = product.ProductId,
                ProductName = "Milky way",
                Quantity = 120,
                ShelfLifeDuration = new ExpirationTerm(365, 0)
            };
            var handler = new UpdateProductCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService);

            var addedProductType = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(command.ProductId, addedProductType.ProductId);
            Assert.AreEqual(inferedProductType.ProductTypeId, addedProductType.ProductTypeId);
            Assert.AreEqual(inferedProductType.MeasureId, addedProductType.MeasureId);
            Assert.AreEqual(inferedProductType.Measure.Name, addedProductType.MeasureName);
            Assert.AreEqual(inferedProductType.ProductImagePath, addedProductType.ProductImagePath);
            Assert.AreEqual(command.ProductName, addedProductType.ProductName);
            Assert.AreEqual(command.Quantity, addedProductType.Quantity);
            Assert.AreEqual(command.ShelfLifeDuration, addedProductType.ShelfLifeDuration);
            Assert.IsTrue(await context.Products.AnyAsync(x => x.ProductId == addedProductType.ProductId
                && x.ProductTypeId == inferedProductType.ProductTypeId
                && x.Name == command.ProductName
                && x.Quantity == command.Quantity
                && x.ShelfLifeDuration == command.ShelfLifeDuration.Value.GetTicks()));
        }

        [TestMethod]
        public async Task UpdateExistingProductWithUnknownType()
        {
            var product = await context.Products
                .AsNoTracking()
                .FirstOrDefaultAsync();
            var inferedProductType = await context.ProductTypes
                .Include(x => x.Measure)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.ProductTypeId == productTypeSettings.Value.UndefinedTypeId);

            var command = new UpdateProductCommand
            {
                ProductId = product.ProductId,
                ProductName = "Nonexistent",
                Quantity = 120,
                ShelfLifeDuration = new ExpirationTerm(365, 0)
            };
            var handler = new UpdateProductCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService);

            var addedProductType = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(command.ProductId, addedProductType.ProductId);
            Assert.AreEqual(inferedProductType.ProductTypeId, addedProductType.ProductTypeId);
            Assert.AreEqual(inferedProductType.MeasureId, addedProductType.MeasureId);
            Assert.AreEqual(inferedProductType.Measure.Name, addedProductType.MeasureName);
            Assert.AreEqual(inferedProductType.ProductImagePath, addedProductType.ProductImagePath);
            Assert.AreEqual(command.ProductName, addedProductType.ProductName);
            Assert.AreEqual(command.Quantity, addedProductType.Quantity);
            Assert.AreEqual(command.ShelfLifeDuration, addedProductType.ShelfLifeDuration);
            Assert.IsTrue(await context.Products.AnyAsync(x => x.ProductId == addedProductType.ProductId
                && x.ProductTypeId == inferedProductType.ProductTypeId
                && x.Name == command.ProductName
                && x.Quantity == command.Quantity
                && x.ShelfLifeDuration == command.ShelfLifeDuration.Value.GetTicks()));
        }

        [TestMethod]
        public async Task UpdateExistingProductWithNegativeQuantity()
        {
            var product = await context.Products
                .AsNoTracking()
                .FirstOrDefaultAsync();
            var command = new UpdateProductCommand
            {
                ProductId = product.ProductId,
                ProductName = "Milky way",
                Quantity = -120,
                ShelfLifeDuration = new ExpirationTerm(365, 0)
            };
            var handler = new UpdateProductCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService);

            await Assert.ThrowsExceptionAsync<NegativeProductQuantityException>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task UpdateExistingProductWithNegativeExpirationTerm()
        {
            var product = await context.Products
                .AsNoTracking()
                .FirstOrDefaultAsync();
            var command = new UpdateProductCommand
            {
                ProductId = product.ProductId,
                ProductName = "Milky way",
                Quantity = 120,
                ShelfLifeDuration = new ExpirationTerm { Days = -15, Hours = -3 }
            };
            var handler = new UpdateProductCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService);

            await Assert.ThrowsExceptionAsync<NegativeShelfLifeException>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task AddNewProductWithNonexistentProductType()
        {
            var nonexistentProductTypeId = Guid.NewGuid();
            while (await context.ProductTypes.AnyAsync(x => x.ProductTypeId == nonexistentProductTypeId))
            {
                nonexistentProductTypeId = Guid.NewGuid();
            }
            var product = await context.Products
                .AsNoTracking()
                .FirstOrDefaultAsync();

            var command = new UpdateProductCommand
            {
                ProductId = product.ProductId,
                ProductName = "Milky way",
                ProductTypeId = nonexistentProductTypeId,
                Quantity = 120,
                ShelfLifeDuration = new ExpirationTerm { Days = 15, Hours = 3 }
            };
            var handler = new UpdateProductCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService);

            await Assert.ThrowsExceptionAsync<EntityDoesNotExistException<ProductType>>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task UpdateProductWithExistingName()
        {
            var product = await context.Products
                .AsNoTracking()
                .FirstOrDefaultAsync();
            var existingProduct = await context.Products.FirstOrDefaultAsync(x => x.ProductId != product.ProductId);

            var command = new UpdateProductCommand
            {
                ProductId = product.ProductId,
                ProductName = existingProduct.Name,
                Quantity = 120,
                ShelfLifeDuration = new ExpirationTerm(365, 0)
            };
            var handler = new UpdateProductCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService);

            await Assert.ThrowsExceptionAsync<EntityAlreadyExistsException<Product>>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task UpdateProductWithEmptyName()
        {
            var product = await context.Products
                .AsNoTracking()
                .FirstOrDefaultAsync();
            var command = new UpdateProductCommand
            {
                ProductId = product.ProductId,
                ProductName = string.Empty,
                Quantity = 120,
                ShelfLifeDuration = new ExpirationTerm(365, 0)
            };
            var handler = new UpdateProductCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService);

            await Assert.ThrowsExceptionAsync<ArgumentEmptyException>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task UpdateNonexistentProduct()
        {
            var nonexistentProductTypeId = Guid.NewGuid();
            while (await context.ProductTypes.AnyAsync(x => x.ProductTypeId == nonexistentProductTypeId))
            {
                nonexistentProductTypeId = Guid.NewGuid();
            }

            var command = new UpdateProductCommand
            {
                ProductId = nonexistentProductTypeId,
                ProductName = "Name",
                Quantity = 120,
                ShelfLifeDuration = new ExpirationTerm(365, 0)
            };
            var handler = new UpdateProductCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService);

            await Assert.ThrowsExceptionAsync<EntityDoesNotExistException<Product>>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }
    }
}
