﻿using FridgeService.Domain;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using FridgeService.UnitTests.ProductTests;
using FridgeService.WebApi.Domain.Products.UseCases;
using FridgeService.WebApi.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.UnitTests.ProductTypes
{
    [TestClass]
    public class AddProductTests : AbstractProductTests
    {
        [TestMethod]
        public async Task AddNewProductWithExistingType()
        {
            var productType = await context.ProductTypes
                .Include(x => x.Measure)
                .FirstOrDefaultAsync();
            var command = new AddProductCommand
            {
                ProductName = "Milky way",
                ProductTypeId = productType.ProductTypeId,
                Quantity = 120,
                ShelfLifeDuration = new ExpirationTerm(365, 0)
            };
            var handler = new AddProductCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService);

            var addedProductType = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(command.ProductTypeId, addedProductType.ProductTypeId);
            Assert.AreEqual(productType.MeasureId, addedProductType.MeasureId);
            Assert.AreEqual(productType.Measure.Name, addedProductType.MeasureName);
            Assert.AreEqual(productType.ProductImagePath, addedProductType.ProductImagePath);
            Assert.AreEqual(command.ProductName, addedProductType.ProductName);
            Assert.AreEqual(command.Quantity, addedProductType.Quantity);
            Assert.AreEqual(command.ShelfLifeDuration, addedProductType.ShelfLifeDuration);
            Assert.IsTrue(await context.Products.AnyAsync(x => x.ProductId == addedProductType.ProductId));
        }

        [TestMethod]
        public async Task AddNewProductWithoutChoosingType()
        {
            var inferedProductType = await context.ProductTypes
                .Include(x => x.Measure)
                .FirstOrDefaultAsync(x => x.Name == "Milk");

            var command = new AddProductCommand
            {
                ProductName = "Milky way",
                Quantity = 120,
                ShelfLifeDuration = new ExpirationTerm(365, 0)
            };
            var handler = new AddProductCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService);

            var addedProductType = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(inferedProductType.ProductTypeId, addedProductType.ProductTypeId);
            Assert.AreEqual(inferedProductType.MeasureId, addedProductType.MeasureId);
            Assert.AreEqual(inferedProductType.Measure.Name, addedProductType.MeasureName);
            Assert.AreEqual(inferedProductType.ProductImagePath, addedProductType.ProductImagePath);
            Assert.AreEqual(command.ProductName, addedProductType.ProductName);
            Assert.AreEqual(command.Quantity, addedProductType.Quantity);
            Assert.AreEqual(command.ShelfLifeDuration, addedProductType.ShelfLifeDuration);
            Assert.IsTrue(await context.Products.AnyAsync(x => x.ProductId == addedProductType.ProductId));
        }

        [TestMethod]
        public async Task AddNewProductWithUnknownType()
        {
            var inferedProductType = await context.ProductTypes
                .Include(x => x.Measure)
                .FirstOrDefaultAsync(x => x.ProductTypeId == productTypeSettings.Value.UndefinedTypeId);

            var command = new AddProductCommand
            {
                ProductName = "Nonexistent",
                Quantity = 120,
                ShelfLifeDuration = new ExpirationTerm(365, 0)
            };
            var handler = new AddProductCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService);

            var addedProductType = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(inferedProductType.ProductTypeId, addedProductType.ProductTypeId);
            Assert.AreEqual(inferedProductType.MeasureId, addedProductType.MeasureId);
            Assert.AreEqual(inferedProductType.Measure.Name, addedProductType.MeasureName);
            Assert.AreEqual(inferedProductType.ProductImagePath, addedProductType.ProductImagePath);
            Assert.AreEqual(command.ProductName, addedProductType.ProductName);
            Assert.AreEqual(command.Quantity, addedProductType.Quantity);
            Assert.AreEqual(command.ShelfLifeDuration, addedProductType.ShelfLifeDuration);
            Assert.IsTrue(await context.Products.AnyAsync(x => x.ProductId == addedProductType.ProductId));
        }

        [TestMethod]
        public async Task AddNewProductWithNegativeQuantity()
        {
            var productType = await context.ProductTypes
                .Include(x => x.Measure)
                .FirstOrDefaultAsync();
            var command = new AddProductCommand
            {
                ProductName = "Milky way",
                ProductTypeId = productType.ProductTypeId,
                Quantity = -120,
                ShelfLifeDuration = new ExpirationTerm(365, 0)
            };
            var handler = new AddProductCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService);

            await Assert.ThrowsExceptionAsync<NegativeProductQuantityException>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task AddNewProductWithNegativeExpirationTerm()
        {
            var productType = await context.ProductTypes
                .Include(x => x.Measure)
                .FirstOrDefaultAsync();
            var command = new AddProductCommand
            {
                ProductName = "Milky way",
                ProductTypeId = productType.ProductTypeId,
                Quantity = 120,
                ShelfLifeDuration = new ExpirationTerm { Days = -15, Hours = -3 }
            };
            var handler = new AddProductCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService);

            await Assert.ThrowsExceptionAsync<NegativeShelfLifeException>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task AddNewProductWithNonexistentProductType()
        {
            var nonexistentProductType = Guid.NewGuid();
            while (await context.ProductTypes.AnyAsync(x => x.ProductTypeId == nonexistentProductType))
            {
                nonexistentProductType = Guid.NewGuid();
            }

            var command = new AddProductCommand
            {
                ProductName = "Milky way",
                ProductTypeId = nonexistentProductType,
                Quantity = 120,
                ShelfLifeDuration = new ExpirationTerm { Days = 15, Hours = 3 }
            };
            var handler = new AddProductCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService);

            await Assert.ThrowsExceptionAsync<EntityDoesNotExistException<ProductType>>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task AddProductWithExistingName()
        {
            var existingProduct = await context.Products.FirstOrDefaultAsync();

            var command = new AddProductCommand
            {
                ProductName = existingProduct.Name,
                Quantity = 120,
                ShelfLifeDuration = new ExpirationTerm(365, 0)
            };
            var handler = new AddProductCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService);

            await Assert.ThrowsExceptionAsync<EntityAlreadyExistsException<Product>>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task AddProductWithEmptyName()
        {
            var command = new AddProductCommand
            {
                ProductName = string.Empty,
                Quantity = 120,
                ShelfLifeDuration = new ExpirationTerm(365, 0)
            };
            var handler = new AddProductCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService);

            await Assert.ThrowsExceptionAsync<ArgumentEmptyException>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }
    }
}
