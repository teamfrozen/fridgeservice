﻿using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using FridgeService.WebApi.Domain.Products.UseCases;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.UnitTests.ProductTests
{
    [TestClass]
    public class GetProductTests : AbstractProductTests
    {
        [TestMethod]
        public async Task GetAllProduct()
        {
            var sourceProducts = await context.Products.ToListAsync();

            var command = new GetProductsCommand();
            var handler = new GetProductsCommandHandler(context, mapper);

            var resultProductTypes = (await handler.Handle(command, new CancellationToken()))
                .ToDictionary(x => x.ProductId);

            Assert.AreEqual(sourceProducts.Count, resultProductTypes.Count);
            foreach (var productType in sourceProducts)
            {
                Assert.IsTrue(resultProductTypes.ContainsKey(productType.ProductId));

                Assert.AreEqual(productType.Name, resultProductTypes[productType.ProductId].ProductName);
            }
        }

        [TestMethod]
        public async Task GetExistingProduct()
        {
            var sourceProduct = await context.Products
                .Include(x => x.ProductType)
                    .ThenInclude(x => x.Measure)
                .FirstOrDefaultAsync();

            var command = new GetProductCommand(sourceProduct.ProductId);
            var handler = new GetProductCommandHandler(context, mapper);

            var resultProductType = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(sourceProduct.ProductId, resultProductType.ProductId);
            Assert.AreEqual(sourceProduct.ProductTypeId, resultProductType.ProductTypeId);
            Assert.AreEqual(sourceProduct.Name, resultProductType.ProductName);
            Assert.AreEqual(sourceProduct.ProductType.Name, resultProductType.ProductTypeName);
            Assert.AreEqual(sourceProduct.Quantity, resultProductType.Quantity);
            Assert.AreEqual(sourceProduct.ProductType.MeasureId, resultProductType.MeasureId);
            Assert.AreEqual(sourceProduct.ProductType.Measure.Name, resultProductType.MeasureName);
            Assert.AreEqual(sourceProduct.ProductType.ProductImagePath, resultProductType.ProductImagePath);
            Assert.AreEqual(new ExpirationTerm(sourceProduct.ShelfLifeDuration.Value), resultProductType.ShelfLifeDuration);
        }

        [TestMethod]
        public async Task GetNonexistentProduct()
        {
            var nonexistentProductId = Guid.NewGuid();
            while (await context.Products.AnyAsync(x => x.ProductId == nonexistentProductId))
            {
                nonexistentProductId = Guid.NewGuid();
            }

            var command = new GetProductCommand(nonexistentProductId);
            var handler = new GetProductCommandHandler(context, mapper);

            var resultProduct = await handler.Handle(command, new CancellationToken());

            Assert.IsNull(resultProduct);
        }
    }
}
