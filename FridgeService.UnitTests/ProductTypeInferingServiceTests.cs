﻿using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.WebApi.Domain.Products.Services;
using FridgeService.WebApi.Exceptions;
using FridgeService.WebApi.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestSupport.EfHelpers;

namespace FridgeService.UnitTests
{
    [TestClass]
    public class ProductTypeInferingServiceTests
    {
        private AppDbContext context;
        private Guid milkProductTypeId;
        private IOptions<ProductTypeSettings> productTypeSettings;
        [TestInitialize]
        public async Task Initialize()
        {
            productTypeSettings = Options.Create(new ProductTypeSettings(new Guid("2d96d7ab-03af-4172-99a4-6855aa5ab02c"), string.Empty));


            var options = SqliteInMemory.CreateOptions<AppDbContext>();
            context = new AppDbContext(options);
            context.Database.EnsureCreated();

            milkProductTypeId = context.ProductTypes.Add(new ProductType
            {
                AvgShelfLifeDuration = 10368000000000,
                Measure = new Measure("l."),
                Name = "Milk",
                ProductAliases = new List<ProductAlias>
                {
                      new ProductAlias { Name = "Milk" },
                      new ProductAlias { Name = "Dairy" }
                },
                ProductImagePath = "PathToMilkImage",
                Products = new List<Product>()
            }).Entity.ProductTypeId;

            context.ProductTypes.Add(new ProductType
            {
                ProductTypeId = productTypeSettings.Value.UndefinedTypeId,
                AvgShelfLifeDuration = 9898949999889,
                Measure = new Measure("pcs"),
                Name = "UndefinedProductType",
                ProductAliases = new List<ProductAlias>
                {
                      new ProductAlias { Name = "Undefined" }
                },
                ProductImagePath = "DefaultPath",
                Products = new List<Product>()
            });
            await context.SaveChangesAsync();
        }

        [TestMethod]
        public async Task GetTypeOfNullString()
        {
            var productTypeInferingService = new ProductTypeInferingService(context, productTypeSettings);

            await Assert.ThrowsExceptionAsync<ArgumentEmptyException>(async ()
                => await productTypeInferingService.GetProductType(null));
        }

        [TestMethod]
        public async Task GetTypeOfUnknownProduct()
        {
            var productTypeInferingService = new ProductTypeInferingService(context, productTypeSettings);

            var productType = await productTypeInferingService.GetProductType("NonexistentType");

            Assert.AreEqual(productTypeSettings.Value.UndefinedTypeId, productType.ProductTypeId);
        }

        [TestMethod]
        public async Task GetTypeOfKnownProduct()
        {
            var productTypeInferingService = new ProductTypeInferingService(context, productTypeSettings);

            var productType = await productTypeInferingService.GetProductType("Dairy product Prostokvashino");

            Assert.AreEqual(milkProductTypeId, productType.ProductTypeId);
        }

        [TestMethod]
        public async Task GetTypeOfKnownProductWithDifferentCharacterCase()
        {
            var productTypeInferingService = new ProductTypeInferingService(context, productTypeSettings);

            var productType = await productTypeInferingService.GetProductType("Kungur milk - for true men");

            Assert.AreEqual(milkProductTypeId, productType.ProductTypeId);
        }
    }
}
