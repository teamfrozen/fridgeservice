#!/usr/bin/env bash

coberturaFile=$(head -n 2 TestResults/**/coverage.cobertura.xml | tail -n 1)

coverage=$(echo "$coberturaFile" | grep -oP '<coverage\sline-rate="\K\d+.\d+')

coverageForGitlab=$(echo "$coverage 100" | awk '{print $1 * $2}')

echo "Test coverage: $coverageForGitlab"