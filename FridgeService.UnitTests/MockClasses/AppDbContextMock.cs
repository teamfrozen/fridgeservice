﻿using FridgeService.Domain;
using FridgeService.Domain.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.UnitTests.MockClasses
{
    public class AppDbContextMock : IAppDbContext
    {
        public DatabaseFacade Database => null;

        public DbSet<Fridge> Fridges { get; set; }
        public DbSet<Measure> Measures { get; set; }
        public DbSet<ProductAlias> ProductAliases { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductInFridge> ProductsInFridge { get; set; }
        public DbSet<ProductType> ProductTypes { get; set; }
        public DbSet<UserFavoriteProduct> FavoriteProducts { get; set; }
        public DbSet<User> Users { get; set; }

        public AppDbContextMock()
        {
            Fridges = new DbSetMock<Fridge>();
            Measures = new DbSetMock<Measure>();
            ProductAliases = new DbSetMock<ProductAlias>();
            Products = new DbSetMock<Product>();
            ProductsInFridge = new DbSetMock<ProductInFridge>();
            ProductTypes = new DbSetMock<ProductType>();
            FavoriteProducts = new DbSetMock<UserFavoriteProduct>();
            Users = new DbSetMock<User>();
        }

        public EntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class
        {
            return null;
        }

        public Task<int> SaveChangesAsync(CancellationToken token = default)
        {
            return Task.FromResult(1);
        }
    }
}
