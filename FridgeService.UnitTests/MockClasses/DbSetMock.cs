﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace FridgeService.UnitTests.MockClasses
{
    public class DbSetMock<TEntity> : DbSet<TEntity>
        where TEntity : class
    {
        public override EntityEntry<TEntity> Add(TEntity item)
        {
            return null;
        }

        public override EntityEntry<TEntity> Remove(TEntity item)
        {
            return null;
        }

        public override EntityEntry<TEntity> Update(TEntity item)
        {
            return null;
        }
    }
}
