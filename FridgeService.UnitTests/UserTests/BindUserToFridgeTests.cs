﻿using FridgeService.Domain;
using FridgeService.Transport.Commands;
using FridgeService.WebApi.Domain.Users.UseCases;
using FridgeService.WebApi.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.UnitTests.UserTests
{
    [TestClass]
    public class BindUserToFridgeTests : AbstractUsersTest
    {
        [TestMethod]
        public async Task BindUserFromUnsharedFridgeToExistingFridgeWithoutUsers()
        {
            var targetFridge = context.Fridges.Add(new Fridge()).Entity;
            await context.SaveChangesAsync();
            context.Entry(targetFridge).State = EntityState.Detached;

            var user = await context.Users
                .Include(x => x.Fridge)
                    .ThenInclude(x => x.Users)
                .Where(x => x.Fridge.Users.Count == 1)
                .FirstOrDefaultAsync();

            var command = new BindUserToExistingFridgeCommand
            {
                UserId = user.UserId,
                FridgeId = targetFridge.FridgeId
            };
            var handler = new BindUserToExistingFridgeCommandHandler(context, mapper, fridgeStore);

            var fridgeInfo = await handler.Handle(command, new CancellationToken());

            Assert.IsTrue(fridgeInfo.FridgeId == targetFridge.FridgeId && fridgeInfo.Users.Contains(user.UserId));
            Assert.IsTrue(context.Users.Any(x => x.UserId == user.UserId && x.FridgeId == targetFridge.FridgeId));
        }

        [TestMethod]
        public async Task BindUserFromUnsharedFridgeToExistingFridgeWithExistingUsers()
        {
            var user = await context.Users
                .Include(x => x.Fridge)
                    .ThenInclude(x => x.Users)
                .Where(x => x.Fridge.Users.Count == 1)
                .FirstOrDefaultAsync();
            var targetFridge = await context.Fridges
                .Include(x => x.Users)
                .FirstOrDefaultAsync(x => x.FridgeId != user.FridgeId);
            var sourceUserCount = targetFridge.Users.Count;

            var command = new BindUserToExistingFridgeCommand
            {
                UserId = user.UserId,
                FridgeId = targetFridge.FridgeId
            };
            var handler = new BindUserToExistingFridgeCommandHandler(context, mapper, fridgeStore);

            var fridgeInfo = await handler.Handle(command, new CancellationToken());

            Assert.IsTrue(fridgeInfo.FridgeId == targetFridge.FridgeId && fridgeInfo.Users.Contains(user.UserId));
            Assert.IsTrue(context.Users.Any(x => x.UserId == user.UserId && x.FridgeId == targetFridge.FridgeId));
            Assert.AreEqual(sourceUserCount + 1, fridgeInfo.Users.Count());
        }

        [TestMethod]
        public async Task BindUserFromSharedFridgeToExistingFridge()
        {
            var user = await context.Users
                .Include(x => x.Fridge)
                    .ThenInclude(x => x.Users)
                .Where(x => x.Fridge.Users.Count > 1)
                .FirstOrDefaultAsync();
            var targetFridge = await context.Fridges
                .Include(x => x.Users)
                .FirstOrDefaultAsync(x => x.FridgeId != user.FridgeId);

            var oldFridgeId = user.FridgeId;

            var command = new BindUserToExistingFridgeCommand
            {
                UserId = user.UserId,
                FridgeId = targetFridge.FridgeId
            };
            var handler = new BindUserToExistingFridgeCommandHandler(context, mapper, fridgeStore);

            var fridgeInfo = await handler.Handle(command, new CancellationToken());

            Assert.IsTrue(fridgeInfo.Users.Contains(user.UserId));
            Assert.IsTrue(context.Users.Any(x => x.UserId == user.UserId && x.FridgeId == fridgeInfo.FridgeId));
            Assert.IsTrue(context.Fridges.Any(x => x.FridgeId == oldFridgeId));
        }

        [TestMethod]
        public async Task BindUserToNonexistentFridge()
        {
            var user = await context.Users.FirstOrDefaultAsync();
            var nonexistentFridgeId = Guid.NewGuid();
            while (await context.Fridges.AnyAsync(x => x.FridgeId == nonexistentFridgeId))
            {
                nonexistentFridgeId = Guid.NewGuid();
            }

            var command = new BindUserToExistingFridgeCommand
            {
                UserId = user.UserId,
                FridgeId = nonexistentFridgeId
            };
            var handler = new BindUserToExistingFridgeCommandHandler(context, mapper, fridgeStore);

            await Assert.ThrowsExceptionAsync<EntityDoesNotExistException<Fridge>>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task BindNonexistentUserToFridge()
        {
            var targetFridge = await context.Fridges.FirstOrDefaultAsync();

            var nonexistentUserId = Guid.NewGuid();
            while (await context.Users.AnyAsync(x => x.UserId == nonexistentUserId))
            {
                nonexistentUserId = Guid.NewGuid();
            }

            var command = new BindUserToExistingFridgeCommand
            {
                UserId = nonexistentUserId,
                FridgeId = targetFridge.FridgeId
            };
            var handler = new BindUserToExistingFridgeCommandHandler(context, mapper, fridgeStore);

            await Assert.ThrowsExceptionAsync<EntityDoesNotExistException<User>>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task BindUserFromFridgeWithProductsToNewFridge()
        {
            var userId = await context.Users
                .Include(x => x.Fridge)
                    .ThenInclude(x => x.ProductsInFridge)
                .Where(x => x.Fridge.ProductsInFridge.Any())
                .Select(x => x.UserId).FirstOrDefaultAsync();

            var command = new BindUserToNewFridgeCommand(userId);
            var handler = new BindUserToNewFridgeCommandHandler(context, mapper, fridgeStore);

            var fridgeInfo = await handler.Handle(command, new CancellationToken());

            Assert.IsTrue(fridgeInfo.Users.Contains(userId));
            Assert.IsTrue(context.Users.Any(x => x.UserId == userId && x.FridgeId == fridgeInfo.FridgeId));
        }

        [TestMethod]
        public async Task BindUserFromSharedFridgeToNewFridge()
        {
            var user = await context.Users
                .Include(x => x.Fridge)
                    .ThenInclude(x => x.Users)
                .Where(x => x.Fridge.Users.Count > 1)
                .FirstOrDefaultAsync();
            var oldFridgeId = user.FridgeId;

            var command = new BindUserToNewFridgeCommand(user.UserId);
            var handler = new BindUserToNewFridgeCommandHandler(context, mapper, fridgeStore);

            var fridgeInfo = await handler.Handle(command, new CancellationToken());

            Assert.IsTrue(fridgeInfo.Users.Contains(user.UserId));
            Assert.IsTrue(context.Users.Any(x => x.UserId == user.UserId && x.FridgeId == fridgeInfo.FridgeId));
            Assert.IsTrue(context.Fridges.Any(x => x.FridgeId == oldFridgeId));
        }

        [TestMethod]
        public async Task BindNonexistentUserToNewFridge()
        {
            var nonexistentUserId = Guid.NewGuid();
            while (await context.Users.AnyAsync(x => x.UserId == nonexistentUserId))
            {
                nonexistentUserId = Guid.NewGuid();
            }

            var command = new BindUserToNewFridgeCommand(nonexistentUserId);
            var handler = new BindUserToNewFridgeCommandHandler(context, mapper, fridgeStore);

            await Assert.ThrowsExceptionAsync<EntityDoesNotExistException<User>>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }
    }
}
