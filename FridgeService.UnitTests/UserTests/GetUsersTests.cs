﻿using FridgeService.Transport.Commands;
using FridgeService.WebApi.Domain.Products.UseCases;
using FridgeService.WebApi.Domain.Users.UseCases;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.UnitTests.UserTests
{
    [TestClass]
    public class GetUsersTests : AbstractUsersTest
    {
        [TestMethod]
        public async Task GetAllUsers()
        {
            var sourceUsers = await context.Users.ToListAsync();

            var command = new GetAllUsersCommand();
            var handler = new GetAllUsersCommandHandler(context, mapper);

            var resultUsers = (await handler.Handle(command, new CancellationToken()))
                .ToDictionary(x => x.UserId);

            Assert.AreEqual(sourceUsers.Count, resultUsers.Count);
            foreach (var user in sourceUsers)
            {
                Assert.IsTrue(resultUsers.ContainsKey(user.UserId));

                Assert.AreEqual(user.FridgeId, resultUsers[user.UserId].FridgeId);
            }
        }

        [TestMethod]
        public async Task GetSingleUser()
        {
            var sourceUser = await context.Users.FirstOrDefaultAsync();

            var command = new GetUserCommand(sourceUser.UserId);
            var handler = new GetUserCommandHandler(context, mapper);

            var resultUser = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(sourceUser.UserId, resultUser.UserId);
            Assert.AreEqual(sourceUser.FridgeId, resultUser.FridgeId);
        }

        [TestMethod]
        public async Task GetNonexistentUser()
        {
            var nonexistentUserId = Guid.NewGuid();
            while (await context.Users.AnyAsync(x => x.UserId == nonexistentUserId))
            {
                nonexistentUserId = Guid.NewGuid();
            }

            var command = new GetUserCommand(nonexistentUserId);
            var handler = new GetUserCommandHandler(context, mapper);

            var resultUser = await handler.Handle(command, new CancellationToken());

            Assert.IsNull(resultUser);
        }

        [TestMethod]
        public async Task GetNonemptyUserFavorites()
        {
            var sourceUser = await context.Users
                .Include(x => x.FavoriteProducts)
                    .ThenInclude(x => x.Product)
                        .ThenInclude(x => x.ProductType)
                            .ThenInclude(x => x.Measure)
                .FirstOrDefaultAsync(x => x.FavoriteProducts.Any());

            var command = new GetUserFavoritesCommand(sourceUser.UserId);
            var handler = new GetUserFavoritesCommandHandler(context, mapper);

            var resultFavoriteProducts = (await handler.Handle(command, new CancellationToken()))
                .ToDictionary(x => x.ProductId);

            Assert.AreEqual(sourceUser.FavoriteProducts.Count, resultFavoriteProducts.Count);
            foreach (var favoriteProduct in sourceUser.FavoriteProducts)
            {
                Assert.IsTrue(resultFavoriteProducts.ContainsKey(favoriteProduct.ProductId));

                Assert.AreEqual(favoriteProduct.Product.Name, resultFavoriteProducts[favoriteProduct.ProductId].ProductName);
                Assert.AreEqual(favoriteProduct.Product.ProductTypeId, resultFavoriteProducts[favoriteProduct.ProductId].ProductTypeId);
                Assert.AreEqual(favoriteProduct.Product.Quantity, resultFavoriteProducts[favoriteProduct.ProductId].Quantity);
                Assert.AreEqual(favoriteProduct.Product.ProductType.Name, resultFavoriteProducts[favoriteProduct.ProductId].ProductTypeName);
                Assert.AreEqual(favoriteProduct.Product.ProductType.Measure.Name, resultFavoriteProducts[favoriteProduct.ProductId].MeasureName);
                Assert.AreEqual(favoriteProduct.Product.ProductType.ProductImagePath, resultFavoriteProducts[favoriteProduct.ProductId].ProductImagePath);
                Assert.IsTrue(resultFavoriteProducts[favoriteProduct.ProductId].IsFavorite);
            }
        }

        [TestMethod]
        public async Task GetEmptyUserFavorites()
        {
            var sourceUser = await context.Users
                .Include(x => x.FavoriteProducts)
                    .ThenInclude(x => x.Product)
                        .ThenInclude(x => x.ProductType)
                            .ThenInclude(x => x.Measure)
                .FirstOrDefaultAsync(x => !x.FavoriteProducts.Any());

            var command = new GetUserFavoritesCommand(sourceUser.UserId);
            var handler = new GetUserFavoritesCommandHandler(context, mapper);

            var resultFavoriteProducts = (await handler.Handle(command, new CancellationToken()));

            Assert.IsFalse(resultFavoriteProducts.Any());
        }

        [TestMethod]
        public async Task GetFavoritesForNonexistentUser()
        {
            var nonexistentUserId = Guid.NewGuid();
            while (await context.Users.AnyAsync(x => x.UserId == nonexistentUserId))
            {
                nonexistentUserId = Guid.NewGuid();
            }

            var command = new GetUserFavoritesCommand(nonexistentUserId);
            var handler = new GetUserFavoritesCommandHandler(context, mapper);

            var resultFavoriteProducts = await handler.Handle(command, new CancellationToken());

            Assert.IsNull(resultFavoriteProducts);
        }

        [TestMethod]
        public async Task GetNonemptyFavoriteOutOfStockProducts()
        {
            var sourceUser = await context.Users
                .Include(x => x.FavoriteProducts)
                .Include(x => x.Fridge)
                    .ThenInclude(x => x.ProductsInFridge)
                .FirstOrDefaultAsync(x => x.FavoriteProducts.Select(t => t.ProductId).Except(x.Fridge.ProductsInFridge.Select(y => y.ProductId)).Any());
            var sourceOutOfStockFavoriteProducts = await context.Products
                .Include(x => x.ProductType)
                    .ThenInclude(x => x.Measure)
                .Where(x => sourceUser.FavoriteProducts
                    .Select(t => t.ProductId)
                    .Except(sourceUser.Fridge.ProductsInFridge.Select(y => y.ProductId))
                .Contains(x.ProductId))
                .ToListAsync();


            var command = new GetUserOutOfStockFavoritesCommand(sourceUser.UserId);
            var handler = new GetUserOutOfStockFavoritesCommandHandler(context, mapper, fridgeStore);

            var resultOutOfStockFavoriteProducts = (await handler.Handle(command, new CancellationToken()))
                .ToDictionary(x => x.ProductId);

            Assert.AreEqual(sourceOutOfStockFavoriteProducts.Count, resultOutOfStockFavoriteProducts.Count);
            foreach (var favoriteProduct in sourceOutOfStockFavoriteProducts)
            {
                Assert.IsTrue(resultOutOfStockFavoriteProducts.ContainsKey(favoriteProduct.ProductId));

                Assert.AreEqual(favoriteProduct.Name, resultOutOfStockFavoriteProducts[favoriteProduct.ProductId].ProductName);
                Assert.AreEqual(favoriteProduct.ProductTypeId, resultOutOfStockFavoriteProducts[favoriteProduct.ProductId].ProductTypeId);
                Assert.AreEqual(favoriteProduct.Quantity, resultOutOfStockFavoriteProducts[favoriteProduct.ProductId].Quantity);
                Assert.AreEqual(favoriteProduct.ProductType.Name, resultOutOfStockFavoriteProducts[favoriteProduct.ProductId].ProductTypeName);
                Assert.AreEqual(favoriteProduct.ProductType.Measure.Name, resultOutOfStockFavoriteProducts[favoriteProduct.ProductId].MeasureName);
                Assert.AreEqual(favoriteProduct.ProductType.ProductImagePath, resultOutOfStockFavoriteProducts[favoriteProduct.ProductId].ProductImagePath);
                Assert.IsTrue(resultOutOfStockFavoriteProducts[favoriteProduct.ProductId].IsFavorite);
            }
        }

        [TestMethod]
        public async Task GetEmptyFavoriteOutOfStockProducts()
        {
            var sourceUser = await context.Users
                .Include(x => x.FavoriteProducts)
                .Include(x => x.Fridge)
                    .ThenInclude(x => x.ProductsInFridge)
                .FirstOrDefaultAsync(x => !x.FavoriteProducts.Select(t => t.ProductId).Except(x.Fridge.ProductsInFridge.Select(y => y.ProductId)).Any());

            var command = new GetUserOutOfStockFavoritesCommand(sourceUser.UserId);
            var handler = new GetUserOutOfStockFavoritesCommandHandler(context, mapper, fridgeStore);

            var resultOutOfStockFavoriteProducts = await handler.Handle(command, new CancellationToken());

            Assert.IsFalse(resultOutOfStockFavoriteProducts.Any());
        }

        [TestMethod]
        public async Task GetFavoriteOutOfStockProductsForNonexistentUser()
        {
            var nonexistentUserId = Guid.NewGuid();
            while (await context.Users.AnyAsync(x => x.UserId == nonexistentUserId))
            {
                nonexistentUserId = Guid.NewGuid();
            }

            var command = new GetUserOutOfStockFavoritesCommand(nonexistentUserId);
            var handler = new GetUserOutOfStockFavoritesCommandHandler(context, mapper, fridgeStore);

            var resultOutOfStockFavoriteProducts = await handler.Handle(command, new CancellationToken());

            Assert.IsNull(resultOutOfStockFavoriteProducts);
        }
    }
}
