﻿using FridgeService.Domain;
using FridgeService.Transport.Commands;
using FridgeService.WebApi.Domain.Users.UseCases;
using FridgeService.WebApi.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.UnitTests.UserTests
{
    [TestClass]
    public class RemoveUserTests : AbstractUsersTest
    {
        [TestMethod]
        public async Task RemoveUserFromUnsharedFridgeWithProducts()
        {
            var user = await context.Fridges
                .Include(x => x.ProductsInFridge)
                .Include(x => x.Users)
                .Where(x => x.ProductsInFridge.Any() && x.Users.Count == 1)
                .Select(x => x.Users.FirstOrDefault())
                .FirstOrDefaultAsync();

            var command = new RemoveUserCommand(user.UserId);
            var handler = new RemoveUserCommandHandler(context, fridgeStore);

            await handler.Handle(command, new CancellationToken());

            Assert.IsFalse(context.Users.Any(x => x.UserId == user.UserId));
            Assert.IsFalse(context.Fridges.Any(x => x.FridgeId == user.FridgeId));
            Assert.IsFalse(context.ProductsInFridge.Any(x => x.FridgeId == user.FridgeId));
            Assert.IsFalse(context.FavoriteProducts.Any(x => x.UserId == user.UserId));
        }

        [TestMethod]
        public async Task RemoveUserFromSharedFridge()
        {
            var user = await context.Fridges
                .Include(x => x.Users)
                .Where(x => x.Users.Count > 1)
                .Select(x => x.Users.FirstOrDefault())
                .FirstOrDefaultAsync();

            var command = new RemoveUserCommand(user.UserId);
            var handler = new RemoveUserCommandHandler(context, fridgeStore);

            await handler.Handle(command, new CancellationToken());

            Assert.IsFalse(context.Users.Any(x => x.UserId == user.UserId));
            Assert.IsTrue(context.Fridges.Any(x => x.FridgeId == user.FridgeId));
            Assert.IsFalse(context.FavoriteProducts.Any(x => x.UserId == user.UserId));
        }

        [TestMethod]
        public async Task RemoveNonexistentUser()
        {
            var nonexistentUserId = Guid.NewGuid();
            while (await context.Users.AnyAsync(x => x.UserId == nonexistentUserId))
            {
                nonexistentUserId = Guid.NewGuid();
            }

            var command = new RemoveUserCommand(nonexistentUserId);
            var handler = new RemoveUserCommandHandler(context, fridgeStore);

            await Assert.ThrowsExceptionAsync<EntityDoesNotExistException<User>>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }
    }
}
