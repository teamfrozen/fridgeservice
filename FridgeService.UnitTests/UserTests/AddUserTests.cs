﻿using FridgeService.Domain;
using FridgeService.Transport.Commands;
using FridgeService.WebApi.Domain.Users.UseCases;
using FridgeService.WebApi.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.UnitTests.UserTests
{
    [TestClass]
    public class AddUserTests : AbstractUsersTest
    {
        [TestMethod]
        public async Task AddUserToNewFridge()
        {
            var nonexistentUserId = Guid.NewGuid();
            while (await context.Users.AnyAsync(x => x.UserId == nonexistentUserId))
            {
                nonexistentUserId = Guid.NewGuid();
            }

            var command = new AddUserCommand
            {
                UserId = nonexistentUserId
            };
            var handler = new AddUserCommandHandler(context, mapper);

            var addedUser = await handler.Handle(command, new CancellationToken());

            Assert.IsNotNull(addedUser);
            Assert.AreEqual(command.UserId, addedUser.UserId);
            Assert.IsTrue(context.Fridges.Any(x => x.FridgeId == addedUser.FridgeId));
            Assert.IsTrue(context.Users.Any(x => x.UserId == addedUser.UserId && x.FridgeId == addedUser.FridgeId));
        }

        [TestMethod]
        public async Task AddUserToExistingFridge()
        {
            var fridge = await context.Fridges
                .Include(x => x.Users)
                .FirstOrDefaultAsync();
            var sourceUserCount = fridge.Users.Count;

            var nonexistentUserId = Guid.NewGuid();
            while (await context.Users.AnyAsync(x => x.UserId == nonexistentUserId))
            {
                nonexistentUserId = Guid.NewGuid();
            }

            var command = new AddUserCommand
            {
                UserId = nonexistentUserId,
                FridgeId = fridge.FridgeId
            };
            var handler = new AddUserCommandHandler(context, mapper);

            var addedUser = await handler.Handle(command, new CancellationToken());

            Assert.IsNotNull(addedUser);
            Assert.AreEqual(command.UserId, addedUser.UserId);
            Assert.AreEqual(command.FridgeId, addedUser.FridgeId);
            Assert.AreEqual(sourceUserCount + 1, fridge.Users.Count);
            Assert.IsTrue(context.Users.Any(x => x.UserId == addedUser.UserId && x.FridgeId == addedUser.FridgeId));
        }

        [TestMethod]
        public async Task AddUserToNonexistentFridge()
        {
            var nonexistentFridgeId = Guid.NewGuid();
            while (await context.Fridges.AnyAsync(x => x.FridgeId == nonexistentFridgeId))
            {
                nonexistentFridgeId = Guid.NewGuid();
            }

            var command = new AddUserCommand
            {
                UserId = Guid.NewGuid(),
                FridgeId = nonexistentFridgeId
            };
            var handler = new AddUserCommandHandler(context, mapper);

            await Assert.ThrowsExceptionAsync<EntityDoesNotExistException<Fridge>>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task AddNonexistentUserToFridge()
        {
            var existingUser = await context.Users
                .FirstOrDefaultAsync();

            var command = new AddUserCommand
            {
                UserId = existingUser.UserId
            };
            var handler = new AddUserCommandHandler(context, mapper);

            await Assert.ThrowsExceptionAsync<EntityAlreadyExistsException<User>>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }
    }
}
