﻿using FridgeService.Domain;
using FridgeService.WebApi.Domain.Fridges;
using FridgeService.WebApi.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FridgeService.UnitTests.FridgeTests
{
    [TestClass]
    public class FridgeStoreTests : AbstractFridgeTests
    {
        [TestMethod]
        public async Task AddProductWithAlreadyExpiredExpirationDateToFridgeAggregate()
        {
            await Assert.ThrowsExceptionAsync<ExpirationDateEarlierThanTodayException>(async ()
                => await fridgeStore.AddProductToFridge(fridgeAggregate, new Product(), DateTime.Today.Subtract(new TimeSpan(1, 0, 0, 0))));
        }

        [TestMethod]
        public async Task AddProductToNullFridgeAggregate()
        {
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(async ()
                => await fridgeStore.AddProductToFridge(null, new Product(), DateTime.Today));
        }

        [TestMethod]
        public async Task AddNullProductToFridgeAggregate()
        {
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(async ()
                => await fridgeStore.AddProductToFridge(fridgeAggregate, null, DateTime.Today.Subtract(new TimeSpan(1, 0, 0, 0))));
        }


        [TestMethod]
        public async Task AddProductWithTodayExpirationDateToFridgeAggregate()
        {
            var productToAdd = await context.Products.FirstOrDefaultAsync();

            var addedProduct = await fridgeStore.AddProductToFridge(fridgeAggregate, productToAdd, DateTime.Today);

            Assert.IsNotNull(addedProduct);
            Assert.AreEqual(fridgeAggregate.FridgeId, addedProduct.FridgeId);
            Assert.AreEqual(productToAdd.ProductId, addedProduct.ProductId);
            Assert.AreEqual(DateTime.Today, addedProduct.DepositDate.Date);
            Assert.AreEqual(DateTime.Today, addedProduct.ExpirationDate.Date);
            Assert.IsTrue(await context.ProductsInFridge.AnyAsync(x => x.ProductInFridgeId == addedProduct.ProductInFridgeId));
        }

        [TestMethod]
        public async Task AddProductWithNormalExpirationDateToFridgeAggregate()
        {
            memoryCache.Set(fridgeAggregate.FridgeId, fridgeAggregate);
            var initialProductCount = fridgeAggregate.Products.Count;
            var productToAdd = await context.Products.FirstOrDefaultAsync();

            var addedProduct = await fridgeStore.AddProductToFridge(fridgeAggregate, productToAdd, DateTime.Today.AddDays(5));

            Assert.IsNotNull(addedProduct);
            Assert.AreEqual(fridgeAggregate.FridgeId, addedProduct.FridgeId);
            Assert.AreEqual(productToAdd.ProductId, addedProduct.ProductId);
            Assert.AreEqual(DateTime.Today, addedProduct.DepositDate.Date);
            Assert.AreEqual(DateTime.Today.AddDays(5), addedProduct.ExpirationDate.Date);
            Assert.IsTrue(await context.ProductsInFridge.AnyAsync(x => x.ProductInFridgeId == addedProduct.ProductInFridgeId));
            Assert.AreEqual(initialProductCount + 1, fridgeAggregate.Products.Count);
            Assert.AreEqual(initialProductCount + 1, ((FridgeAggregate)memoryCache.Get(fridgeAggregate.FridgeId)).Products.Count);
        }

        [DataRow(2)]
        [DataRow(10)]
        [DataTestMethod]
        public async Task AddMultipleProductsWithNormalExpirationDateToFridgeAggregate(int productCount)
        {
            var initialProductCountInFridge = fridgeAggregate.Products.Count;
            var initialProductCountInDb = await context.ProductsInFridge.CountAsync();
            var productToAdd = await context.Products.FirstOrDefaultAsync();

            var addedProducts = new List<ProductInFridge>();
            for (int i = 0; i < productCount; i++)
            {
                addedProducts.Add(await fridgeStore.AddProductToFridge(fridgeAggregate, productToAdd, DateTime.Today.AddDays(5)));
            }

            Assert.AreEqual(productCount, addedProducts.Count);
            Assert.AreEqual(initialProductCountInFridge + productCount, fridgeAggregate.Products.Count);
            Assert.AreEqual(initialProductCountInDb + productCount, await context.ProductsInFridge.CountAsync());
        }

        [TestMethod]
        public async Task GetFridgeAggregateFromDb()
        {
            var resultFridge = await fridgeStore.GetAsync(fridgeAggregate.FridgeId);

            Assert.AreEqual(fridgeAggregate.FridgeId, resultFridge.FridgeId);
            Assert.AreEqual(fridgeAggregate.Products.Count, resultFridge.Products.Count);
            Assert.IsTrue(memoryCache.TryGetValue(fridgeAggregate.FridgeId, out _));
            Assert.AreEqual(resultFridge, memoryCache.Get(fridgeAggregate.FridgeId));
        }

        [TestMethod]
        public async Task GetFridgeAggregateFromCache()
        {
            var sourceFridge = await fridgeStore.GetAsync(fridgeAggregate.FridgeId);

            var resultFridge = await fridgeStore.GetAsync(fridgeAggregate.FridgeId);

            Assert.AreEqual(sourceFridge, resultFridge);
            Assert.AreEqual(sourceFridge, memoryCache.Get(fridgeAggregate.FridgeId));
        }

        [TestMethod]
        public async Task GetNonexistentFridgeAggregate()
        {
            var nonexistentFridge = Guid.NewGuid();
            while (await context.Fridges.AnyAsync(x => x.FridgeId == nonexistentFridge))
            {
                nonexistentFridge = Guid.NewGuid();
            }

            await Assert.ThrowsExceptionAsync<EntityDoesNotExistException<Fridge>>(async ()
               => await fridgeStore.GetAsync(nonexistentFridge));
        }

        [TestMethod]
        public async Task GetEmptyFridgeAggregate()
        {
            var productsToRemove = await context.ProductsInFridge
                .Where(x => x.FridgeId == fridgeAggregate.FridgeId)
                .ToListAsync();
            context.ProductsInFridge.RemoveRange(productsToRemove);
            await context.SaveChangesAsync();

            var resultFridge = await fridgeStore.GetAsync(fridgeAggregate.FridgeId);

            Assert.AreEqual(fridgeAggregate.FridgeId, resultFridge.FridgeId);
            Assert.AreEqual(0, resultFridge.Products.Count);
        }

        [TestMethod]
        public async Task RemoveExistingProductFromFridgeAggregate()
        {
            memoryCache.Set(fridgeAggregate.FridgeId, fridgeAggregate);
            var productToRemove = fridgeAggregate.Products.First().Value;
            var initialProductCount = fridgeAggregate.Products.Count;
            var initialProductCountInDb = await context.ProductsInFridge.CountAsync();

            await fridgeStore.RemoveProductFromFridgeAsync(fridgeAggregate, productToRemove.ProductInFridgeId);

            Assert.AreEqual(initialProductCount - 1, fridgeAggregate.Products.Count);
            Assert.AreEqual(initialProductCountInDb - 1, await context.ProductsInFridge.CountAsync());
            Assert.IsFalse(fridgeAggregate.Products.TryGetValue(productToRemove.ProductInFridgeId, out _));
            Assert.IsFalse(context.ProductsInFridge.Any(x => x.ProductInFridgeId == productToRemove.ProductInFridgeId));
            Assert.AreEqual(initialProductCount - 1, ((FridgeAggregate)memoryCache.Get(fridgeAggregate.FridgeId)).Products.Count);
        }

        [TestMethod]
        public async Task RemoveNonexistentProductFromFridgeAggregate()
        {
            var initialProductCountInFridge = fridgeAggregate.Products;
            var initialProductCountInDb = await context.ProductsInFridge.CountAsync();

            var nonexistentProductInFridge = Guid.NewGuid();
            while (await context.ProductsInFridge.AnyAsync(x => x.ProductInFridgeId == nonexistentProductInFridge))
            {
                nonexistentProductInFridge = Guid.NewGuid();
            }

            await Assert.ThrowsExceptionAsync<ProductDoesNotExistInFridgeException>(async ()
                => await fridgeStore.RemoveProductFromFridgeAsync(fridgeAggregate, nonexistentProductInFridge));
            Assert.AreEqual(initialProductCountInFridge, fridgeAggregate.Products);
            Assert.AreEqual(initialProductCountInDb, await context.ProductsInFridge.CountAsync());
        }

        [TestMethod]
        public async Task UpdateProductExpirationDateOfNonexistentProduct()
        {
            var nonexistentProductInFridge = Guid.NewGuid();
            while (await context.ProductsInFridge.AnyAsync(x => x.ProductInFridgeId == nonexistentProductInFridge))
            {
                nonexistentProductInFridge = Guid.NewGuid();
            }

            await Assert.ThrowsExceptionAsync<ProductDoesNotExistInFridgeException>(async ()
                => await fridgeStore.UpdateProductExpirationDateAsync(fridgeAggregate, nonexistentProductInFridge, DateTime.Today));
        }

        [TestMethod]
        public async Task UpdateProductExpirationDateToPastDate()
        {
            var productToUpdate = fridgeAggregate.Products.First().Value;
            var initialExpirationDate = productToUpdate.ExpirationDate;

            await Assert.ThrowsExceptionAsync<ExpirationDateEarlierThanTodayException>(async ()
                => await fridgeStore.UpdateProductExpirationDateAsync(fridgeAggregate, productToUpdate.ProductInFridgeId, DateTime.Today.Subtract(new TimeSpan(1, 0, 0, 0))));

            Assert.AreEqual(initialExpirationDate, fridgeAggregate.Products.First().Value.ExpirationDate);
        }

        [TestMethod]
        public async Task UpdateProductExpirationDateToFutureDate()
        {
            var productToUpdate = fridgeAggregate.Products.First().Value;

            var resultProduct = await fridgeStore.UpdateProductExpirationDateAsync(fridgeAggregate, productToUpdate.ProductInFridgeId, DateTime.Today.AddDays(10));

            Assert.AreEqual(DateTime.Today.AddDays(10), fridgeAggregate.Products.First().Value.ExpirationDate);
            Assert.AreEqual(DateTime.Today.AddDays(10), (await context.ProductsInFridge.FirstOrDefaultAsync(x => x.ProductInFridgeId == productToUpdate.ProductInFridgeId)).ExpirationDate);
        }

        [TestMethod]
        public async Task RemoveExistingFridgeAggregate()
        {
            await fridgeStore.RemoveFridge(fridgeAggregate);

            Assert.IsFalse(await context.Fridges.AnyAsync(x => x.FridgeId == fridgeAggregate.FridgeId));
            Assert.IsFalse(await context.ProductsInFridge.AnyAsync(x => x.FridgeId == fridgeAggregate.FridgeId));
        }

        [TestMethod]
        public async Task RemoveAlreadyRemovedFridgeAggregate()
        {
            await fridgeStore.RemoveFridge(fridgeAggregate);

            await Assert.ThrowsExceptionAsync<EntityDoesNotExistException<Fridge>>(async ()
                => await fridgeStore.RemoveFridge(fridgeAggregate));
        }
    }
}
