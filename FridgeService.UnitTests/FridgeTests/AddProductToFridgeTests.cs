﻿using FridgeService.Domain;
using FridgeService.Transport.Commands;
using FridgeService.Transport.RequestModels;
using FridgeService.WebApi.Domain.Fridges.UseCases;
using FridgeService.WebApi.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.UnitTests.FridgeTests
{
    [TestClass]
    public class AddProductToFridgeTests : AbstractFridgeTests
    {
        [TestMethod]
        public async Task AddChosenSingleProductToFridge()
        {
            var chosenProductType = await context.ProductTypes
                .Include(x => x.Measure)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.ProductTypeId == productTypeSettings.Value.UndefinedTypeId);
            var chosenProduct = await context.Products
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Name == "TestProduct");
            var productToAdd = new ProductToBeAddedManuallyToFridge
            {
                ProductId = chosenProduct.ProductId,
                ProductTypeId = chosenProductType.ProductTypeId,
                ExpirationDate = DateTime.Today.AddDays(5)
            };
            var command = new AddProductManuallyToFridgeCommand(productToAdd, userId);
            var handler = new AddProductManuallyToFridgeCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService, fridgeStore);

            var addedProduct = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(chosenProduct.Name, addedProduct.ProductName);
            Assert.AreEqual(productToAdd.ProductTypeId, addedProduct.ProductTypeId);
            Assert.AreEqual(productToAdd.ExpirationDate, addedProduct.ExpirationDate);
            Assert.AreEqual(chosenProductType.Name, addedProduct.ProductTypeName);
            Assert.AreEqual(chosenProductType.Measure.Name, addedProduct.MeasureName);
            Assert.AreEqual(chosenProduct.ProductId, addedProduct.ProductId);
        }

        [TestMethod]
        public async Task AddChosenSingleProductWithPastDateToFridge()
        {
            var chosenProductType = await context.ProductTypes
                .Include(x => x.Measure)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.ProductTypeId == productTypeSettings.Value.UndefinedTypeId);
            var chosenProduct = await context.Products
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Name == "TestProduct");
            var productToAdd = new ProductToBeAddedManuallyToFridge
            {
                ProductId = chosenProduct.ProductId,
                ProductTypeId = chosenProductType.ProductTypeId,
                ExpirationDate = DateTime.Today.Subtract(new TimeSpan(3, 0, 0, 0))
            };
            var command = new AddProductManuallyToFridgeCommand(productToAdd, userId);
            var handler = new AddProductManuallyToFridgeCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService, fridgeStore);

            await Assert.ThrowsExceptionAsync<ExpirationDateEarlierThanTodayException>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task AddChosenSingleProductToNonexistentFridge()
        {
            var chosenProductType = await context.ProductTypes
                .Include(x => x.Measure)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.ProductTypeId == productTypeSettings.Value.UndefinedTypeId);
            var chosenProduct = await context.Products
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Name == "TestProduct");
            var productToAdd = new ProductToBeAddedManuallyToFridge
            {
                ProductId = chosenProduct.ProductId,
                ProductTypeId = chosenProductType.ProductTypeId
            };
            var nonexistentUser = Guid.NewGuid();
            while (await context.Users.AnyAsync(x => x.UserId == nonexistentUser))
            {
                nonexistentUser = Guid.NewGuid();
            }
            var command = new AddProductManuallyToFridgeCommand(productToAdd, nonexistentUser);
            var handler = new AddProductManuallyToFridgeCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService, fridgeStore);

            await Assert.ThrowsExceptionAsync<EntityDoesNotExistException<User>>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task AddExistingSingleProductToFridge()
        {
            var chosenProductType = await context.ProductTypes
                .Include(x => x.Measure)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.ProductTypeId == productTypeSettings.Value.UndefinedTypeId);
            var productToAdd = new ProductToBeAddedManuallyToFridge
            {
                ProductName = "TestProduct",
                ProductTypeId = chosenProductType.ProductTypeId,
                ExpirationDate = DateTime.Today.AddDays(5)
            };
            var command = new AddProductManuallyToFridgeCommand(productToAdd, userId);
            var handler = new AddProductManuallyToFridgeCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService, fridgeStore);

            var addedProduct = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(productToAdd.ProductName, addedProduct.ProductName);
            Assert.AreEqual(productToAdd.ProductTypeId, addedProduct.ProductTypeId);
            Assert.AreEqual(productToAdd.ExpirationDate, addedProduct.ExpirationDate);
            Assert.AreEqual(chosenProductType.Name, addedProduct.ProductTypeName);
            Assert.AreEqual(chosenProductType.Measure.Name, addedProduct.MeasureName);
            Assert.IsTrue(await context.Products.AnyAsync(x => x.ProductTypeId == chosenProductType.ProductTypeId && x.ProductId == addedProduct.ProductId));
            Assert.IsTrue(addedProduct.IsFavorite);
        }

        [TestMethod]
        public async Task AddNewSingleProductToFridge()
        {
            var chosenProductType = await context.ProductTypes
                .Include(x => x.Measure)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Name == "Milk");
            var productToAdd = new ProductToBeAddedManuallyToFridge
            {
                ProductName = "MilkNew",
                ProductTypeId = chosenProductType.ProductTypeId,
                ExpirationDate = DateTime.Today.AddDays(5),
                Quantity = 10
            };
            var command = new AddProductManuallyToFridgeCommand(productToAdd, userId);
            var handler = new AddProductManuallyToFridgeCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService, fridgeStore);

            var addedProduct = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(productToAdd.ProductName, addedProduct.ProductName);
            Assert.AreEqual(productToAdd.ProductTypeId, addedProduct.ProductTypeId);
            Assert.AreEqual(productToAdd.ExpirationDate, addedProduct.ExpirationDate);
            Assert.AreEqual(chosenProductType.Name, addedProduct.ProductTypeName);
            Assert.AreEqual(chosenProductType.Measure.Name, addedProduct.MeasureName);
            Assert.AreEqual(productToAdd.Quantity, addedProduct.Quantity);
            Assert.IsTrue(await context.Products.AnyAsync(x => x.ProductTypeId == chosenProductType.ProductTypeId && x.ProductId == addedProduct.ProductId));
        }

        [TestMethod]
        public async Task AddNewSingleProductWithUnchosenTypeToFridge()
        {
            var inferedProductType = await context.ProductTypes
                .Include(x => x.Measure)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Name == "Milk");
            var productToAdd = new ProductToBeAddedManuallyToFridge
            {
                ProductName = "MilkNew",
                ExpirationDate = DateTime.Today.AddDays(5)
            };
            var command = new AddProductManuallyToFridgeCommand(productToAdd, userId);
            var handler = new AddProductManuallyToFridgeCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService, fridgeStore);

            var addedProduct = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(productToAdd.ProductName, addedProduct.ProductName);
            Assert.AreEqual(inferedProductType.ProductTypeId, addedProduct.ProductTypeId);
            Assert.AreEqual(productToAdd.ExpirationDate, addedProduct.ExpirationDate);
            Assert.AreEqual(inferedProductType.Name, addedProduct.ProductTypeName);
            Assert.AreEqual(inferedProductType.Measure.Name, addedProduct.MeasureName);
            Assert.IsTrue(await context.Products.AnyAsync(x => x.ProductTypeId == inferedProductType.ProductTypeId && x.ProductId == addedProduct.ProductId));
        }

        [TestMethod]
        public async Task AddNewSingleProductWithUnchosenTypeAndExpirationDateToFridge()
        {
            var inferedProductType = await context.ProductTypes
                .Include(x => x.Measure)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Name == "Milk");
            var productToAdd = new ProductToBeAddedManuallyToFridge
            {
                ProductName = "MilkNew"
            };
            var command = new AddProductManuallyToFridgeCommand(productToAdd, userId);
            var handler = new AddProductManuallyToFridgeCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService, fridgeStore);

            var addedProduct = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(productToAdd.ProductName, addedProduct.ProductName);
            Assert.AreEqual(inferedProductType.ProductTypeId, addedProduct.ProductTypeId);
            Assert.AreEqual(DateTime.Today.AddTicks(inferedProductType.AvgShelfLifeDuration.Value), addedProduct.ExpirationDate);
            Assert.AreEqual(inferedProductType.Name, addedProduct.ProductTypeName);
            Assert.AreEqual(inferedProductType.Measure.Name, addedProduct.MeasureName);
            Assert.IsTrue(await context.Products.AnyAsync(x => x.ProductTypeId == inferedProductType.ProductTypeId && x.ProductId == addedProduct.ProductId));
        }

        [TestMethod]
        public async Task AddNewSingleUndefinedProductToFridge()
        {
            var inferedProductType = await context.ProductTypes
                .Include(x => x.Measure)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.ProductTypeId == productTypeSettings.Value.UndefinedTypeId);
            var productToAdd = new ProductToBeAddedManuallyToFridge
            {
                ProductName = "UndefinedName"
            };
            var command = new AddProductManuallyToFridgeCommand(productToAdd, userId);
            var handler = new AddProductManuallyToFridgeCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService, fridgeStore);

            var addedProduct = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(productToAdd.ProductName, addedProduct.ProductName);
            Assert.AreEqual(inferedProductType.ProductTypeId, addedProduct.ProductTypeId);
            Assert.AreEqual(DateTime.Today.AddTicks(inferedProductType.AvgShelfLifeDuration.Value), addedProduct.ExpirationDate);
            Assert.AreEqual(inferedProductType.Name, addedProduct.ProductTypeName);
            Assert.AreEqual(inferedProductType.Measure.Name, addedProduct.MeasureName);
            Assert.IsTrue(await context.Products.AnyAsync(x => x.ProductTypeId == inferedProductType.ProductTypeId && x.ProductId == addedProduct.ProductId));
        }

        [TestMethod]
        public async Task AddSingleProductFromReceiptToFridge()
        {
            var inferedProductTypeForMilk = await context.ProductTypes
                .Include(x => x.Measure)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Name == "Milk");
            var undefinedProductType = await context.ProductTypes
                .Include(x => x.Measure)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.ProductTypeId == productTypeSettings.Value.UndefinedTypeId);
            var productsToBeAdded = new List<ProductToBeAddedFromReceiptToFridge>
            {
                new ProductToBeAddedFromReceiptToFridge
                {
                    ProductName = "Milk Prostokvashino",
                    Quantity = 1.5,
                    ExpirationDate = DateTime.Today.AddDays(3)
                },
                new ProductToBeAddedFromReceiptToFridge
                {
                    ProductName = "Undefined"
                },
                new ProductToBeAddedFromReceiptToFridge
                {
                    ProductName = "TestProduct",
                    ExpirationDate = DateTime.Today
                },
                new ProductToBeAddedFromReceiptToFridge
                {
                    ProductName = "OutdatedProduct",
                    ExpirationDate = DateTime.Today.Subtract(new TimeSpan(1,0,0,0))
                },
                new ProductToBeAddedFromReceiptToFridge()
            };

            var command = new AddProductFromReceiptToFridgeCommand(productsToBeAdded, userId);
            var handler = new AddProductFromReceiptToFridgeCommandHandler(context, mapper, shelfLifeInferingService, productTypeInferingService, fridgeStore);

            var resultOfAddingProducts = await handler.Handle(command, new CancellationToken());

            Assert.IsTrue(resultOfAddingProducts.Errors.Count == 2);
            Assert.IsTrue(resultOfAddingProducts.AddedProducts.Count == 3);

            Assert.AreEqual(inferedProductTypeForMilk.ProductTypeId, resultOfAddingProducts.AddedProducts[0].ProductTypeId);
            Assert.IsTrue(context.Products.Any(x => x.ProductId == resultOfAddingProducts.AddedProducts[0].ProductId
                                                    && x.Quantity == productsToBeAdded[0].Quantity
                                                    && x.Name == productsToBeAdded[0].ProductName));
            Assert.IsFalse(resultOfAddingProducts.AddedProducts[0].IsFavorite);

            Assert.AreEqual(undefinedProductType.ProductTypeId, resultOfAddingProducts.AddedProducts[1].ProductTypeId);
            Assert.IsTrue(context.Products.Any(x => x.ProductId == resultOfAddingProducts.AddedProducts[0].ProductId
                                                    && x.Name == productsToBeAdded[0].ProductName));
            Assert.IsFalse(resultOfAddingProducts.AddedProducts[1].IsFavorite);

            Assert.AreEqual(undefinedProductType.ProductTypeId, resultOfAddingProducts.AddedProducts[2].ProductTypeId);
            Assert.IsTrue(resultOfAddingProducts.AddedProducts[2].IsFavorite);
        }
    }
}
