﻿using FridgeService.Transport.Commands;
using FridgeService.WebApi.Domain.Fridges.UseCases;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.UnitTests.FridgeTests
{
    [TestClass]
    public class GetFridgeTests : AbstractFridgeTests
    {
        [TestMethod]
        public async Task GetAllFridges()
        {
            var fridges = await context.Fridges
                .Include(x => x.Users)
                .OrderBy(x => x.FridgeId)
                .AsNoTracking()
                .ToListAsync();
            var command = new GetAllFridgesCommand();
            var handler = new GetAllFridgesCommandHandler(context);

            var resultedFridges = (await handler.Handle(command, new CancellationToken()))
                .ToDictionary(x => x.FridgeId);

            Assert.AreEqual(fridges.Count, resultedFridges.Count);
            foreach (var fridge in fridges)
            {
                Assert.IsTrue(resultedFridges.ContainsKey(fridge.FridgeId));
                Assert.AreEqual(fridge.Users.Count, resultedFridges[fridge.FridgeId].Users.Count());
                Assert.AreEqual(fridge.Users.Count, fridge.Users.Select(x => x.UserId).Intersect(resultedFridges[fridge.FridgeId].Users).Count());
            }
        }

        [TestMethod]
        public async Task GetFridgeOfExistingUser()
        {
            var fridge = await context.Fridges
                .Include(x => x.Users)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.FridgeId == fridgeId);
            var command = new GetFridgeByUserCommand(userId);
            var handler = new GetFridgeByUserCommandHandler(context, mapper);

            var resultedFridge = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(fridge.FridgeId, resultedFridge.FridgeId);
            Assert.AreEqual(fridge.Users.Count, resultedFridge.Users.Count());
            Assert.AreEqual(fridge.Users.Count, fridge.Users.Select(x => x.UserId).Intersect(resultedFridge.Users).Count());
        }

        [TestMethod]
        public async Task GetFridgeOfNonexistentUser()
        {
            var nonexistentUserId = Guid.NewGuid();
            while (await context.Users.AnyAsync(x => x.UserId == nonexistentUserId))
            {
                nonexistentUserId = Guid.NewGuid();
            }

            var command = new GetFridgeByUserCommand(nonexistentUserId);
            var handler = new GetFridgeByUserCommandHandler(context, mapper);

            var resultedFridge = await handler.Handle(command, new CancellationToken());

            Assert.IsNull(resultedFridge);
        }

        [TestMethod]
        public async Task GetExistingFridge()
        {
            var fridge = await context.Fridges
                .Include(x => x.Users)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.FridgeId == fridgeId);
            var command = new GetFridgeCommand(fridgeId);
            var handler = new GetFridgeCommandHandler(context, mapper);

            var resultedFridge = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(fridge.FridgeId, resultedFridge.FridgeId);
            Assert.AreEqual(fridge.Users.Count, resultedFridge.Users.Count());
            Assert.AreEqual(fridge.Users.Count, fridge.Users.Select(x => x.UserId).Intersect(resultedFridge.Users).Count());
        }

        [TestMethod]
        public async Task GetNonexistent()
        {
            var nonexistentFridgeId = Guid.NewGuid();
            while (await context.Fridges.AnyAsync(x => x.FridgeId == nonexistentFridgeId))
            {
                nonexistentFridgeId = Guid.NewGuid();
            }

            var command = new GetFridgeCommand(nonexistentFridgeId);
            var handler = new GetFridgeCommandHandler(context, mapper);

            var resultedFridge = await handler.Handle(command, new CancellationToken());

            Assert.IsNull(resultedFridge);
        }
    }
}
