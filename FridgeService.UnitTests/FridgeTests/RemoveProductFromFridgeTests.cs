﻿using FridgeService.Transport.Commands;
using FridgeService.WebApi.Domain.Fridges.UseCases;
using FridgeService.WebApi.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.UnitTests.FridgeTests
{
    [TestClass]
    public class RemoveProductFromFridgeTests : AbstractFridgeTests
    {
        [TestMethod]
        public async Task RemoveExistingProductFromFridge()
        {
            var productToRemove = await context.ProductsInFridge.FirstAsync(x => x.FridgeId == fridgeId);
            var command = new RemoveProductFromFridgeCommand(productToRemove.ProductInFridgeId, userId);
            var handler = new RemoveProductFromFridgeCommandHandler(context, fridgeStore);

            await handler.Handle(command, new CancellationToken());

            Assert.IsFalse(await context.ProductsInFridge.AnyAsync(x => x.ProductInFridgeId == productToRemove.ProductInFridgeId));
        }

        [TestMethod]
        public async Task RemoveNonexistentProductFromFridge()
        {
            var nonexistentProductId = Guid.NewGuid();
            while (await context.ProductsInFridge.AnyAsync(x => x.ProductInFridgeId == nonexistentProductId))
            {
                nonexistentProductId = Guid.NewGuid();
            }

            var command = new RemoveProductFromFridgeCommand(nonexistentProductId, userId);
            var handler = new RemoveProductFromFridgeCommandHandler(context, fridgeStore);

            await Assert.ThrowsExceptionAsync<ProductDoesNotExistInFridgeException>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

    }
}
