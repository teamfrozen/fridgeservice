﻿using FridgeService.Domain;
using FridgeService.Transport.Commands;
using FridgeService.WebApi.Domain.Fridges.UseCases;
using FridgeService.WebApi.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.UnitTests.FridgeTests
{
    [TestClass]
    public class UpdateProductExpirationDateTests : AbstractFridgeTests
    {
        [TestMethod]
        public async Task UpdateProductExpirationDateToPastDate()
        {
            var productToUpdate = await context.ProductsInFridge.FirstAsync(x => x.FridgeId == fridgeId);
            var command = new UpdateFridgeProductExpirationDateCommand(userId, productToUpdate.ProductInFridgeId, DateTime.Today.Subtract(new TimeSpan(1, 0, 0, 0)));
            var handler = new UpdateFridgeProductExpirationDateCommandHandler(context, mapper, fridgeStore);

            await Assert.ThrowsExceptionAsync<ExpirationDateEarlierThanTodayException>(async ()
               => await handler.Handle(command, new CancellationToken()));

        }

        [TestMethod]
        public async Task UpdateProductExpirationDateToFutureDate()
        {
            var productToUpdate = await context.ProductsInFridge.FirstAsync(x => x.FridgeId == fridgeId);
            var command = new UpdateFridgeProductExpirationDateCommand(userId, productToUpdate.ProductInFridgeId, DateTime.Today.AddDays(3));
            var handler = new UpdateFridgeProductExpirationDateCommandHandler(context, mapper, fridgeStore);

            var updatedProduct = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(productToUpdate.ProductInFridgeId, updatedProduct.ProductInFridgeId);
            Assert.AreEqual(DateTime.Today.AddDays(3), updatedProduct.ExpirationDate);
            Assert.IsTrue(updatedProduct.IsFavorite);
        }

        [TestMethod]
        public async Task UpdateNonexistentProductExpirationDate()
        {
            var nonexistentProductId = Guid.NewGuid();
            while (await context.ProductsInFridge.AnyAsync(x => x.ProductInFridgeId == nonexistentProductId))
            {
                nonexistentProductId = Guid.NewGuid();
            }

            var command = new UpdateFridgeProductExpirationDateCommand(userId, nonexistentProductId, DateTime.Today.AddDays(3));
            var handler = new UpdateFridgeProductExpirationDateCommandHandler(context, mapper, fridgeStore);

            await Assert.ThrowsExceptionAsync<ProductDoesNotExistInFridgeException>(async ()
               => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task UpdateProductExpirationDateByNonexistentUser()
        {
            var nonexistentUserId = Guid.NewGuid();
            while (await context.Users.AnyAsync(x => x.UserId == nonexistentUserId))
            {
                nonexistentUserId = Guid.NewGuid();
            }

            var productToUpdate = await context.ProductsInFridge.FirstAsync(x => x.FridgeId == fridgeId);
            var command = new UpdateFridgeProductExpirationDateCommand(nonexistentUserId, productToUpdate.ProductInFridgeId, DateTime.Today.AddDays(3));
            var handler = new UpdateFridgeProductExpirationDateCommandHandler(context, mapper, fridgeStore);

            await Assert.ThrowsExceptionAsync<EntityDoesNotExistException<User>>(async ()
               => await handler.Handle(command, new CancellationToken()));
        }
    }
}
