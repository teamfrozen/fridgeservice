﻿using FridgeService.Transport.Commands;
using FridgeService.WebApi.Domain.Fridges.UseCases;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.UnitTests.FridgeTests
{
    [TestClass]
    public class GetFridgeContentsTests : AbstractFridgeTests
    {
        [TestMethod]
        public async Task GetProductsInExistingUserFridge()
        {
            var getFridgeContentsCommand = new GetFridgeContentsCommand(userId);
            var handler = new GetFridgeContentsHandler(context, mapper, fridgeStore);

            var resultProducts = await handler.Handle(getFridgeContentsCommand, new CancellationToken());

            foreach (var productInFridge in context.ProductsInFridge
                .Include(x => x.Product)
                    .ThenInclude(x => x.ProductType)
                        .ThenInclude(x => x.Measure)
                .Where(x => x.FridgeId == fridgeId))
            {
                var resultProduct = resultProducts.FirstOrDefault(x => x.ProductInFridgeId == productInFridge.ProductInFridgeId);
                Assert.IsNotNull(resultProduct);
                Assert.AreEqual(productInFridge.ProductId, resultProduct.ProductId);
                Assert.AreEqual(productInFridge.Product.ProductTypeId, resultProduct.ProductTypeId);
                Assert.AreEqual(productInFridge.Product.ProductType.Measure.Name, resultProduct.MeasureName);
                Assert.AreEqual(productInFridge.Product.ProductType.ProductImagePath, resultProduct.ProductImagePath);
                Assert.AreEqual(productInFridge.Product.Name, resultProduct.ProductName);
                Assert.AreEqual(productInFridge.Product.ProductType.Name, resultProduct.ProductTypeName);
                Assert.AreEqual(productInFridge.DepositDate, resultProduct.DepositDate);
                Assert.AreEqual(productInFridge.ExpirationDate, resultProduct.ExpirationDate);
                Assert.AreEqual(productInFridge.Product.Quantity, resultProduct.Quantity);

                var isInFavorites = await context.FavoriteProducts.AnyAsync(x => x.UserId == userId && x.ProductId == resultProduct.ProductId);
                Assert.AreEqual(isInFavorites, resultProduct.IsFavorite);
            }
        }

        [TestMethod]
        public async Task GetProductsFromEmptyExistingUserFridge()
        {
            var productsToRemove = await context.ProductsInFridge.Where(x => x.FridgeId == fridgeId).ToListAsync();
            context.ProductsInFridge.RemoveRange(productsToRemove);
            await context.SaveChangesAsync();

            var getFridgeContentsCommand = new GetFridgeContentsCommand(userId);
            var handler = new GetFridgeContentsHandler(context, mapper, fridgeStore);

            var resultProducts = await handler.Handle(getFridgeContentsCommand, new CancellationToken());

            Assert.IsFalse(resultProducts.Any());
        }

        [TestMethod]
        public async Task GetProductsFromNonexistentUserFridge()
        {
            var nonexistentUser = Guid.NewGuid();
            while (await context.Users.AnyAsync(x => x.UserId == nonexistentUser))
            {
                nonexistentUser = Guid.NewGuid();
            }

            var getFridgeContentsCommand = new GetFridgeContentsCommand(nonexistentUser);
            var handler = new GetFridgeContentsHandler(context, mapper, fridgeStore);

            var resultProducts = await handler.Handle(getFridgeContentsCommand, new CancellationToken());

            Assert.IsNull(resultProducts);
        }

        [TestMethod]
        public async Task GetExistingProductInFridge()
        {
            var productInFridge = await context.ProductsInFridge
                .Include(x => x.Product)
                    .ThenInclude(x => x.ProductType)
                        .ThenInclude(x => x.Measure)
                .FirstOrDefaultAsync();

            var getProductInFridgeCommand = new GetProductInFridgeCommand(userId, productInFridge.ProductInFridgeId);
            var handler = new GetProductInFridgeCommandHandler(context, mapper);

            var resultProduct = await handler.Handle(getProductInFridgeCommand, new CancellationToken());

            Assert.AreEqual(productInFridge.ProductInFridgeId, resultProduct.ProductInFridgeId);
            Assert.AreEqual(productInFridge.Product.Name, resultProduct.ProductName);
            Assert.AreEqual(productInFridge.Product.ProductTypeId, resultProduct.ProductTypeId);
            Assert.AreEqual(productInFridge.ExpirationDate, resultProduct.ExpirationDate);
            Assert.AreEqual(productInFridge.DepositDate, resultProduct.DepositDate);
            Assert.AreEqual(productInFridge.Product.ProductType.Measure.Name, resultProduct.MeasureName);
            Assert.AreEqual(productInFridge.ProductId, resultProduct.ProductId);
            Assert.AreEqual(productInFridge.Product.Quantity, resultProduct.Quantity);
            Assert.IsTrue(resultProduct.IsFavorite);
        }
    }
}
