﻿using AutoMapper;
using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.UnitTests.MockClasses;
using FridgeService.WebApi;
using FridgeService.WebApi.Domain.Fridges;
using FridgeService.WebApi.Domain.Fridges.Services;
using FridgeService.WebApi.Domain.Product_Types.Services;
using FridgeService.WebApi.Domain.Products.Services;
using FridgeService.WebApi.Options;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestSupport.EfHelpers;

namespace FridgeService.UnitTests.ProductTypesTests
{
    [TestClass]
    public class AbstractProductTypesTests
    {
        protected AppDbContext context;
        protected FridgeStore fridgeStore;
        protected IMapper mapper;
        protected IMemoryCache memoryCache;
        protected IOptions<ProductTypeSettings> productTypeSettings;
        protected IOptions<ExpirationSettings> expirationSettings;
        protected ProductTypeInferingService productTypeInferingService;
        protected ShelfLifeInferingService shelfLifeInferingService;
        protected ImageUploadingService imageUploadingService;

        [TestInitialize]
        public async Task Initialize()
        {
            productTypeSettings = Options.Create(new ProductTypeSettings(new Guid("2d96d7ab-03af-4172-99a4-6855aa5ab02c"), "DefaultImage"));
            expirationSettings = Options.Create(new ExpirationSettings(14));

            var options = SqliteInMemory.CreateOptions<AppDbContext>();
            context = new AppDbContext(options);
            context.Database.EnsureCreated();

            var fridge = context.Fridges.Add(new Fridge()).Entity;
            context.ProductTypes.Add(new ProductType
            {
                AvgShelfLifeDuration = 10368000000000,
                Measure = new Measure("l."),
                Name = "Milk",
                ProductAliases = new List<ProductAlias>
                {
                      new ProductAlias { Name = "Milk" },
                      new ProductAlias { Name = "Dairy" }
                },
                ProductImagePath = "PathToMilkImage",
                Products = new List<Product>()
            });

            var productType = context.ProductTypes.Add(GetUndefinedProductType());
            var product = context.Products.Add(new Product
            {
                Name = "TestProduct",
                ProductType = productType.Entity,
                Quantity = 55,
                ShelfLifeDuration = 1000000000000
            }).Entity;
            context.ProductsInFridge.Add(new ProductInFridge
            {
                Product = product,
                DepositDate = DateTime.Today.Subtract(new TimeSpan(3, 0, 0, 0)),
                ExpirationDate = DateTime.Today.AddDays(3),
                FridgeId = fridge.FridgeId
            });

            var userId = context.Users.Add(new User { FridgeId = fridge.FridgeId }).Entity.UserId;
            context.FavoriteProducts.Add(new UserFavoriteProduct
            {
                UserId = userId,
                ProductId = product.ProductId
            });

            await context.SaveChangesAsync();

            memoryCache = new MemoryCache(new MemoryCacheOptions());
            fridgeStore = new FridgeStore(memoryCache, context);

            var mapperConfiguration = new MapperConfiguration(cfg => cfg.AddProfile(new MappingProfile()));
            mapper = new Mapper(mapperConfiguration);

            productTypeInferingService = new ProductTypeInferingService(context, productTypeSettings);
            shelfLifeInferingService = new ShelfLifeInferingService(expirationSettings);

            var webHostEnvironmentMock = new WebHostEnvironmentMock();
            imageUploadingService = new ImageUploadingService(productTypeSettings, webHostEnvironmentMock);
        }

        private ProductType GetUndefinedProductType()
        {
            return new ProductType
            {
                ProductTypeId = productTypeSettings.Value.UndefinedTypeId,
                AvgShelfLifeDuration = 10368000000000,
                Measure = new Measure("шт."),
                Name = "TestProductType",
                ProductAliases = new List<ProductAlias>
                {
                      new ProductAlias { Name = "Test" },
                      new ProductAlias { Name = "ProductType" }
                },
                ProductImagePath = "DefaultPath",
                Products = new List<Product>()
            };
        }
    }
}
