﻿using FridgeService.Domain;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using FridgeService.WebApi.Domain.Product_Types.UseCases;
using FridgeService.WebApi.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.UnitTests.ProductTypesTests
{
    [TestClass]
    public class UpdateProductTypeTests : AbstractProductTypesTests
    {
        [TestMethod]
        public async Task UpdateProductTypeWithoutAliasIntersectionsWithDefaultImage()
        {
            var sourceType = await context.ProductTypes
                .AsNoTracking()
                .FirstOrDefaultAsync();

            var selectedMeasure = await context.Measures.FirstOrDefaultAsync();
            var command = new UpdateProductTypeCommand
            {
                ProductTypeId = sourceType.ProductTypeId,
                ProductTypeName = "Updated Product Type",
                AverageShelfLifeDuration = new ExpirationTerm
                {
                    Days = 10,
                    Hours = 5
                },
                MeasureId = selectedMeasure.MeasureId,
                ProductAliases = new List<string>
                {
                    "New Product", "New Type"
                }
            };
            var handler = new UpdateProductTypeCommandHandler(context, mapper, memoryCache, shelfLifeInferingService, imageUploadingService);

            var updatedProductType = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(sourceType.ProductTypeId, updatedProductType.ProductTypeId);
            Assert.AreEqual(command.ProductTypeName, updatedProductType.ProductTypeName);
            Assert.AreEqual(command.AverageShelfLifeDuration, updatedProductType.AverageShelfLifeDuration);
            Assert.AreEqual(command.MeasureId, updatedProductType.MeasureId);
            Assert.AreEqual(selectedMeasure.Name, updatedProductType.MeasureName);
            Assert.AreEqual(sourceType.ProductImagePath, updatedProductType.ProductImagePath);
            Assert.AreEqual(command.ProductAliases.Count(), updatedProductType.ProductAliases.Count());
            Assert.IsTrue(await context.ProductTypes.AnyAsync(x => x.ProductTypeId == updatedProductType.ProductTypeId));

            foreach (var alias in command.ProductAliases)
            {
                Assert.IsTrue(updatedProductType.ProductAliases.Contains(alias));
                Assert.IsTrue(await context.ProductAliases.AnyAsync(x => x.Name == alias));
            }
        }

        [TestMethod]
        public async Task UpdateProductTypeWithAliasIntersectionsWithItself()
        {
            var sourceType = await context.ProductTypes
                .AsNoTracking()
                .FirstOrDefaultAsync();

            var selectedMeasure = await context.Measures.FirstOrDefaultAsync();
            var command = new UpdateProductTypeCommand
            {
                ProductTypeId = sourceType.ProductTypeId,
                ProductTypeName = "Updated Product Type",
                AverageShelfLifeDuration = new ExpirationTerm
                {
                    Days = 10,
                    Hours = 5
                },
                MeasureId = selectedMeasure.MeasureId,
                ProductAliases = new List<string>
                {
                    "Milk", "New Type"
                }
            };
            var handler = new UpdateProductTypeCommandHandler(context, mapper, memoryCache, shelfLifeInferingService, imageUploadingService);

            var updatedProductType = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(sourceType.ProductTypeId, updatedProductType.ProductTypeId);
            Assert.AreEqual(command.ProductTypeName, updatedProductType.ProductTypeName);
            Assert.AreEqual(command.AverageShelfLifeDuration, updatedProductType.AverageShelfLifeDuration);
            Assert.AreEqual(command.MeasureId, updatedProductType.MeasureId);
            Assert.AreEqual(selectedMeasure.Name, updatedProductType.MeasureName);
            Assert.AreEqual(sourceType.ProductImagePath, updatedProductType.ProductImagePath);
            Assert.AreEqual(command.ProductAliases.Count(), updatedProductType.ProductAliases.Count());
            Assert.IsTrue(await context.ProductTypes.AnyAsync(x => x.ProductTypeId == updatedProductType.ProductTypeId));

            foreach (var alias in command.ProductAliases)
            {
                Assert.IsTrue(updatedProductType.ProductAliases.Contains(alias));
                Assert.IsTrue(await context.ProductAliases.AnyAsync(x => x.Name == alias));
            }
        }

        [TestMethod]
        public async Task UpdateProductTypeWithAliasIntersectionsWithOtherTypes()
        {
            var sourceType = await context.ProductTypes
                .AsNoTracking()
                .FirstOrDefaultAsync();

            var selectedMeasure = await context.Measures.FirstOrDefaultAsync();
            var command = new UpdateProductTypeCommand
            {
                ProductTypeId = sourceType.ProductTypeId,
                ProductTypeName = "New Product Type",
                AverageShelfLifeDuration = new ExpirationTerm
                {
                    Days = 10,
                    Hours = 5
                },
                MeasureId = selectedMeasure.MeasureId,
                ProductAliases = new List<string>
                {
                    "Test", "New Type"
                }
            };
            var handler = new UpdateProductTypeCommandHandler(context, mapper, memoryCache, shelfLifeInferingService, imageUploadingService);

            await Assert.ThrowsExceptionAsync<AliasesConnectedToOtherProductTypesException>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task UpdateProductNameToExisting()
        {
            var sourceType = await context.ProductTypes
                .AsNoTracking()
                .FirstOrDefaultAsync();

            var command = new UpdateProductTypeCommand
            {
                ProductTypeId = sourceType.ProductTypeId,
                ProductTypeName = "TestProductType",
                AverageShelfLifeDuration = new ExpirationTerm
                {
                    Days = 10,
                    Hours = 5
                },
                MeasureName = "New Measure",
                ProductAliases = new List<string>
                {
                    "New Product", "New Type"
                }
            };
            var handler = new UpdateProductTypeCommandHandler(context, mapper, memoryCache, shelfLifeInferingService, imageUploadingService);

            await Assert.ThrowsExceptionAsync<EntityAlreadyExistsException<ProductType>>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task UpdateProductTypeWithNullAlias()
        {
            var sourceType = await context.ProductTypes
                .AsNoTracking()
                .FirstOrDefaultAsync();

            var selectedMeasure = await context.Measures.FirstOrDefaultAsync();
            var command = new UpdateProductTypeCommand
            {
                ProductTypeId = sourceType.ProductTypeId,
                ProductTypeName = "New Product Type",
                AverageShelfLifeDuration = new ExpirationTerm
                {
                    Days = 10,
                    Hours = 5
                },
                MeasureId = selectedMeasure.MeasureId,
            };
            var handler = new UpdateProductTypeCommandHandler(context, mapper, memoryCache, shelfLifeInferingService, imageUploadingService);

            await Assert.ThrowsExceptionAsync<ArgumentEmptyException>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task UpdateProductTypeWithNegativeShelfLife()
        {
            var sourceType = await context.ProductTypes
                .AsNoTracking()
                .FirstOrDefaultAsync();

            var selectedMeasure = await context.Measures.FirstOrDefaultAsync();
            var command = new UpdateProductTypeCommand
            {
                ProductTypeId = sourceType.ProductTypeId,
                ProductTypeName = "New Product Type",
                AverageShelfLifeDuration = new ExpirationTerm
                {
                    Days = -10,
                    Hours = 5
                },
                MeasureId = selectedMeasure.MeasureId,
                ProductAliases = new List<string>
                {
                    "New Product", "New Type"
                }
            };
            var handler = new UpdateProductTypeCommandHandler(context, mapper, memoryCache, shelfLifeInferingService, imageUploadingService);

            await Assert.ThrowsExceptionAsync<NegativeShelfLifeException>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task UpdateProductTypeWithNewMeasure()
        {
            var sourceType = await context.ProductTypes
                .AsNoTracking()
                .FirstOrDefaultAsync();

            var command = new UpdateProductTypeCommand
            {
                ProductTypeId = sourceType.ProductTypeId,
                ProductTypeName = "New Product Type",
                AverageShelfLifeDuration = new ExpirationTerm
                {
                    Days = 10,
                    Hours = 5
                },
                MeasureName = "New Measure",
                ProductAliases = new List<string>
                {
                    "New Product", "New Type"
                }
            };
            var handler = new UpdateProductTypeCommandHandler(context, mapper, memoryCache, shelfLifeInferingService, imageUploadingService);

            var addedProductType = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(command.ProductTypeName, addedProductType.ProductTypeName);
            Assert.AreEqual(command.AverageShelfLifeDuration, addedProductType.AverageShelfLifeDuration);
            Assert.AreEqual(command.MeasureName, addedProductType.MeasureName);
            Assert.AreEqual(command.ProductAliases.Count(), addedProductType.ProductAliases.Count());
            Assert.IsTrue(await context.ProductTypes.AnyAsync(x => x.ProductTypeId == addedProductType.ProductTypeId));
            Assert.IsTrue(await context.Measures.AnyAsync(x => x.Name == command.MeasureName && x.MeasureId == addedProductType.MeasureId));

            foreach (var alias in command.ProductAliases)
            {
                Assert.IsTrue(addedProductType.ProductAliases.Contains(alias));
                Assert.IsTrue(await context.ProductAliases.AnyAsync(x => x.Name == alias));
            }
        }

        [TestMethod]
        public async Task UpdateProductTypeWithCustomImage()
        {
            var uploadCommand = new UploadProductImageCommand(new FormFile(new MemoryStream(), 0, 64, "imageTest", "image.png"));
            var uploadHandler = new UploadProductImageCommandHandler(memoryCache);

            var uploadedImage = await uploadHandler.Handle(uploadCommand, new CancellationToken());

            var sourceType = await context.ProductTypes
                .AsNoTracking()
                .FirstOrDefaultAsync();
            var selectedMeasure = await context.Measures.FirstOrDefaultAsync();
            var addProductTypeCommand = new UpdateProductTypeCommand
            {
                ProductTypeId = sourceType.ProductTypeId,
                ProductTypeName = "New Product Type",
                AverageShelfLifeDuration = new ExpirationTerm
                {
                    Days = 10,
                    Hours = 5
                },
                MeasureId = selectedMeasure.MeasureId,
                ProductAliases = new List<string>
                {
                    "New Product", "New Type"
                },
                ProductImageId = uploadedImage
            };
            var addProductTypeHandler = new UpdateProductTypeCommandHandler(context, mapper, memoryCache, shelfLifeInferingService, imageUploadingService);

            var addedProductType = await addProductTypeHandler.Handle(addProductTypeCommand, new CancellationToken());

            Assert.AreEqual(addProductTypeCommand.ProductTypeName, addedProductType.ProductTypeName);
            Assert.AreEqual(addProductTypeCommand.AverageShelfLifeDuration, addedProductType.AverageShelfLifeDuration);
            Assert.AreEqual(addProductTypeCommand.MeasureId, addedProductType.MeasureId);
            Assert.AreEqual(selectedMeasure.Name, addedProductType.MeasureName);
            Assert.AreEqual(addProductTypeCommand.ProductAliases.Count(), addedProductType.ProductAliases.Count());
            Assert.AreEqual("Images/" + addProductTypeCommand.ProductTypeName + ".png", addedProductType.ProductImagePath);
            Assert.IsTrue(await context.ProductTypes.AnyAsync(x => x.ProductTypeId == addedProductType.ProductTypeId));

            foreach (var alias in addProductTypeCommand.ProductAliases)
            {
                Assert.IsTrue(addedProductType.ProductAliases.Contains(alias));
                Assert.IsTrue(await context.ProductAliases.AnyAsync(x => x.Name == alias));
            }
        }

        [TestMethod]
        public async Task UpdateProductTypeWithUnexistentImage()
        {
            var sourceType = await context.ProductTypes
                .AsNoTracking()
                .FirstOrDefaultAsync();
            var selectedMeasure = await context.Measures.FirstOrDefaultAsync();
            var command = new UpdateProductTypeCommand
            {
                ProductTypeId = sourceType.ProductTypeId,
                ProductTypeName = "New Product Type",
                AverageShelfLifeDuration = new ExpirationTerm
                {
                    Days = 10,
                    Hours = 5
                },
                MeasureId = selectedMeasure.MeasureId,
                ProductAliases = new List<string>
                {
                    "New Product", "New Type"
                },
                ProductImageId = Guid.NewGuid()
            };
            var handler = new UpdateProductTypeCommandHandler(context, mapper, memoryCache, shelfLifeInferingService, imageUploadingService);

            await Assert.ThrowsExceptionAsync<ProductImageNotInCacheException>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }
    }
}
