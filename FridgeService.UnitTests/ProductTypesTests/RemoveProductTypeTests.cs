﻿using FridgeService.Domain;
using FridgeService.Transport.Commands;
using FridgeService.WebApi.Domain.Product_Types.UseCases;
using FridgeService.WebApi.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.UnitTests.ProductTypesTests
{
    [TestClass]
    public class RemoveProductTypeTests : AbstractProductTypesTests
    {
        [TestMethod]
        public async Task RemoveExistingProductType()
        {
            var sourceType = await context.ProductTypes
                .Include(x => x.ProductAliases)
                .AsNoTracking()
                .FirstOrDefaultAsync();

            var command = new RemoveProductTypeCommand(sourceType.ProductTypeId);
            var handler = new RemoveProductTypeCommandHandler(context);

            await handler.Handle(command, new CancellationToken());

            Assert.IsFalse(await context.ProductTypes.AnyAsync(x => x.ProductTypeId == sourceType.ProductTypeId));
            Assert.IsFalse(await context.ProductAliases.AnyAsync(x => sourceType.ProductAliases.Select(y => y.AliasId).Contains(x.AliasId)));
        }

        [TestMethod]
        public async Task RemoveNonexistentProductType()
        {
            var nonexistentProductTypeId = Guid.NewGuid();
            while (await context.ProductTypes.AnyAsync(x => x.ProductTypeId == nonexistentProductTypeId))
            {
                nonexistentProductTypeId = Guid.NewGuid();
            }

            var command = new RemoveProductTypeCommand(nonexistentProductTypeId);
            var handler = new RemoveProductTypeCommandHandler(context);

            await Assert.ThrowsExceptionAsync<EntityDoesNotExistException<ProductType>>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task RemoveProductTypeConnectedWithExistingProductsInFridges()
        {
            var favoriteProducts = await context.FavoriteProducts.ToListAsync();
            context.FavoriteProducts.RemoveRange(favoriteProducts);
            await context.SaveChangesAsync();

            var typeWithExistingProductsInFridges = await context.ProductTypes
                .Include(x => x.ProductAliases)
                .Include(x => x.Products)
                    .ThenInclude(x => x.ProductInFridges)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Products.Any(y => y.ProductInFridges.Any()));

            var command = new RemoveProductTypeCommand(typeWithExistingProductsInFridges.ProductTypeId);
            var handler = new RemoveProductTypeCommandHandler(context);

            await Assert.ThrowsExceptionAsync<ImpossibleEntityRemovementException>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task RemoveProductTypeConnectedWithExistingProductsInFavorites()
        {
            var productsInFridges = await context.ProductsInFridge.ToListAsync();
            context.ProductsInFridge.RemoveRange(productsInFridges);
            await context.SaveChangesAsync();

            var typeWithExistingProductsInFridges = await context.ProductTypes
                .Include(x => x.ProductAliases)
                .Include(x => x.Products)
                    .ThenInclude(x => x.UserFavorites)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Products.Any(y => y.UserFavorites.Any()));

            var command = new RemoveProductTypeCommand(typeWithExistingProductsInFridges.ProductTypeId);
            var handler = new RemoveProductTypeCommandHandler(context);

            await Assert.ThrowsExceptionAsync<ImpossibleEntityRemovementException>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }
    }
}
