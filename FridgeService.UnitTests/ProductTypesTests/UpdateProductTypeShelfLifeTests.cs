﻿using FridgeService.Domain;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using FridgeService.WebApi.Domain.Product_Types.UseCases;
using FridgeService.WebApi.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.UnitTests.ProductTypesTests
{
    [TestClass]
    public class UpdateProductTypeShelfLifeTests : AbstractProductTypesTests
    {
        [TestMethod]
        public async Task UpdateProductTypeShelfLifeWithPositiveExpirationTerm()
        {
            var sourceType = await context.ProductTypes
                .AsNoTracking()
                .FirstOrDefaultAsync();

            var command = new UpdateProductTypeShelfLifeCommand(sourceType.ProductTypeId, new ExpirationTerm(10, 5));
            var handler = new UpdateProductTypeShelfLifeCommandHandler(context, mapper);

            var updatedProductType = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(sourceType.ProductTypeId, updatedProductType.ProductTypeId);
            Assert.AreEqual(command.AverageShelfLifeDuration, updatedProductType.AverageShelfLifeDuration);
        }

        [TestMethod]
        public async Task UpdateProductTypeShelfLifeWithNegativeExpirationTerm()
        {
            var sourceType = await context.ProductTypes
                .AsNoTracking()
                .FirstOrDefaultAsync();

            var command = new UpdateProductTypeShelfLifeCommand(sourceType.ProductTypeId, new ExpirationTerm { Days = -10, Hours = 5 });
            var handler = new UpdateProductTypeShelfLifeCommandHandler(context, mapper);

            await Assert.ThrowsExceptionAsync<NegativeShelfLifeException>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task UpdateUnexistentProductTypeShelfLife()
        {
            var nonexistentProductTypeId = Guid.NewGuid();
            while (await context.ProductTypes.AnyAsync(x => x.ProductTypeId == nonexistentProductTypeId))
            {
                nonexistentProductTypeId = Guid.NewGuid();
            }

            var command = new UpdateProductTypeShelfLifeCommand(nonexistentProductTypeId, new ExpirationTerm { Days = 10, Hours = 5 });
            var handler = new UpdateProductTypeShelfLifeCommandHandler(context, mapper);

            await Assert.ThrowsExceptionAsync<EntityDoesNotExistException<ProductType>>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }
    }
}
