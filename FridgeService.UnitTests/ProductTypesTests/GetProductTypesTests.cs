﻿using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using FridgeService.WebApi.Domain.Product_Types.UseCases;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.UnitTests.ProductTypesTests
{
    [TestClass]
    public class GetProductTypesTests : AbstractProductTypesTests
    {
        [TestMethod]
        public async Task GetAllProductTypes()
        {
            var sourceProductTypes = await context.ProductTypes.ToListAsync();

            var command = new GetAllProductTypesCommand();
            var handler = new GetAllProductTypesCommandHandler(context, mapper);

            var resultProductTypes = (await handler.Handle(command, new CancellationToken()))
                .ToDictionary(x => x.ProductTypeId);

            Assert.AreEqual(sourceProductTypes.Count, resultProductTypes.Count);
            foreach (var productType in sourceProductTypes)
            {
                Assert.IsTrue(resultProductTypes.ContainsKey(productType.ProductTypeId));

                Assert.AreEqual(productType.Name, resultProductTypes[productType.ProductTypeId].ProductTypeName);
            }
        }

        [TestMethod]
        public async Task GetExistingProductType()
        {
            var sourceProductType = await context.ProductTypes
                .Include(x => x.Measure)
                .Include(x => x.ProductAliases)
                .FirstOrDefaultAsync();

            var command = new GetProductTypeCommand(sourceProductType.ProductTypeId);
            var handler = new GetProductTypeCommandHandler(context, mapper);

            var resultProductType = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(sourceProductType.ProductTypeId, resultProductType.ProductTypeId);
            Assert.AreEqual(sourceProductType.MeasureId, resultProductType.MeasureId);
            Assert.AreEqual(sourceProductType.Measure.Name, resultProductType.MeasureName);
            Assert.AreEqual(sourceProductType.ProductImagePath, resultProductType.ProductImagePath);
            Assert.AreEqual(new ExpirationTerm(sourceProductType.AvgShelfLifeDuration.Value), resultProductType.AverageShelfLifeDuration);

            foreach (var alias in sourceProductType.ProductAliases.Select(x => x.Name))
            {
                Assert.IsTrue(resultProductType.ProductAliases.Contains(alias));
            }
        }

        [TestMethod]
        public async Task GetNonexistentProductType()
        {
            var nonexistentProductTypeId = Guid.NewGuid();
            while (await context.ProductTypes.AnyAsync(x => x.ProductTypeId == nonexistentProductTypeId))
            {
                nonexistentProductTypeId = Guid.NewGuid();
            }

            var command = new GetProductTypeCommand(nonexistentProductTypeId);
            var handler = new GetProductTypeCommandHandler(context, mapper);

            var resultProductType = await handler.Handle(command, new CancellationToken());

            Assert.IsNull(resultProductType);
        }
    }
}
