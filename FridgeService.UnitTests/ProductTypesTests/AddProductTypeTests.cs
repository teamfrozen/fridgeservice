﻿using FridgeService.Domain;
using FridgeService.Transport.Commands;
using FridgeService.Transport.Responses;
using FridgeService.WebApi.Domain.Product_Types.UseCases;
using FridgeService.WebApi.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FridgeService.UnitTests.ProductTypesTests
{
    [TestClass]
    public class AddProductTypeTests : AbstractProductTypesTests
    {
        [TestMethod]
        public async Task AddNewProductTypeWithoutAliasIntersectionsWithDefaultImage()
        {
            var selectedMeasure = await context.Measures.FirstOrDefaultAsync();
            var command = new AddProductTypeCommand
            {
                ProductTypeName = "New Product Type",
                AverageShelfLifeDuration = new ExpirationTerm
                {
                    Days = 10,
                    Hours = 5
                },
                MeasureId = selectedMeasure.MeasureId,
                ProductAliases = new List<string>
                {
                    "New Product", "New Type"
                }
            };
            var handler = new AddProductTypeCommandHandler(context, mapper, memoryCache, shelfLifeInferingService, imageUploadingService);

            var addedProductType = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(command.ProductTypeName, addedProductType.ProductTypeName);
            Assert.AreEqual(command.AverageShelfLifeDuration, addedProductType.AverageShelfLifeDuration);
            Assert.AreEqual(command.MeasureId, addedProductType.MeasureId);
            Assert.AreEqual(selectedMeasure.Name, addedProductType.MeasureName);
            Assert.AreEqual(productTypeSettings.Value.DefaultImage, addedProductType.ProductImagePath);
            Assert.AreEqual(command.ProductAliases.Count(), addedProductType.ProductAliases.Count());
            Assert.IsTrue(await context.ProductTypes.AnyAsync(x => x.ProductTypeId == addedProductType.ProductTypeId));

            foreach (var alias in command.ProductAliases)
            {
                Assert.IsTrue(addedProductType.ProductAliases.Contains(alias));
                Assert.IsTrue(await context.ProductAliases.AnyAsync(x => x.Name == alias));
            }
        }

        [TestMethod]
        public async Task AddNewProductTypeWithAliasIntersectionsWithDefaultImage()
        {
            var selectedMeasure = await context.Measures.FirstOrDefaultAsync();
            var command = new AddProductTypeCommand
            {
                ProductTypeName = "New Product Type",
                AverageShelfLifeDuration = new ExpirationTerm
                {
                    Days = 10,
                    Hours = 5
                },
                MeasureId = selectedMeasure.MeasureId,
                ProductAliases = new List<string>
                {
                    "Test", "New Type"
                }
            };
            var handler = new AddProductTypeCommandHandler(context, mapper, memoryCache, shelfLifeInferingService, imageUploadingService);

            await Assert.ThrowsExceptionAsync<AliasesConnectedToOtherProductTypesException>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task AddExistingProductType()
        {
            var command = new AddProductTypeCommand
            {
                ProductTypeName = "TestProductType",
                AverageShelfLifeDuration = new ExpirationTerm
                {
                    Days = 10,
                    Hours = 5
                },
                MeasureName = "New Measure",
                ProductAliases = new List<string>
                {
                    "New Product", "New Type"
                }
            };
            var handler = new AddProductTypeCommandHandler(context, mapper, memoryCache, shelfLifeInferingService, imageUploadingService);

            await Assert.ThrowsExceptionAsync<EntityAlreadyExistsException<ProductType>>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task AddNewProductTypeWithNullAliasWithDefaultImage()
        {
            var selectedMeasure = await context.Measures.FirstOrDefaultAsync();
            var command = new AddProductTypeCommand
            {
                ProductTypeName = "New Product Type",
                AverageShelfLifeDuration = new ExpirationTerm
                {
                    Days = 10,
                    Hours = 5
                },
                MeasureId = selectedMeasure.MeasureId,
            };
            var handler = new AddProductTypeCommandHandler(context, mapper, memoryCache, shelfLifeInferingService, imageUploadingService);

            await Assert.ThrowsExceptionAsync<ArgumentEmptyException>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task AddNewProductTypeWithNegativeShelfLifeWithDefaultImage()
        {
            var selectedMeasure = await context.Measures.FirstOrDefaultAsync();
            var command = new AddProductTypeCommand
            {
                ProductTypeName = "New Product Type",
                AverageShelfLifeDuration = new ExpirationTerm
                {
                    Days = -10,
                    Hours = 5
                },
                MeasureId = selectedMeasure.MeasureId,
                ProductAliases = new List<string>
                {
                    "New Product", "New Type"
                }
            };
            var handler = new AddProductTypeCommandHandler(context, mapper, memoryCache, shelfLifeInferingService, imageUploadingService);

            await Assert.ThrowsExceptionAsync<NegativeShelfLifeException>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }

        [TestMethod]
        public async Task AddNewProductTypeWithNewMeasureWithDefaultImage()
        {
            var command = new AddProductTypeCommand
            {
                ProductTypeName = "New Product Type",
                AverageShelfLifeDuration = new ExpirationTerm
                {
                    Days = 10,
                    Hours = 5
                },
                MeasureName = "New Measure",
                ProductAliases = new List<string>
                {
                    "New Product", "New Type"
                }
            };
            var handler = new AddProductTypeCommandHandler(context, mapper, memoryCache, shelfLifeInferingService, imageUploadingService);

            var addedProductType = await handler.Handle(command, new CancellationToken());

            Assert.AreEqual(command.ProductTypeName, addedProductType.ProductTypeName);
            Assert.AreEqual(command.AverageShelfLifeDuration, addedProductType.AverageShelfLifeDuration);
            Assert.AreEqual(command.MeasureName, addedProductType.MeasureName);
            Assert.AreEqual(productTypeSettings.Value.DefaultImage, addedProductType.ProductImagePath);
            Assert.AreEqual(command.ProductAliases.Count(), addedProductType.ProductAliases.Count());
            Assert.IsTrue(await context.ProductTypes.AnyAsync(x => x.ProductTypeId == addedProductType.ProductTypeId));
            Assert.IsTrue(await context.Measures.AnyAsync(x => x.Name == command.MeasureName && x.MeasureId == addedProductType.MeasureId));

            foreach (var alias in command.ProductAliases)
            {
                Assert.IsTrue(addedProductType.ProductAliases.Contains(alias));
                Assert.IsTrue(await context.ProductAliases.AnyAsync(x => x.Name == alias));
            }
        }

        [TestMethod]
        public async Task AddNewProductTypeWithCustomImage()
        {
            var uploadCommand = new UploadProductImageCommand(new FormFile(new MemoryStream(), 0, 64, "imageTest", "image.png"));
            var uploadHandler = new UploadProductImageCommandHandler(memoryCache);

            var uploadedImage = await uploadHandler.Handle(uploadCommand, new CancellationToken());

            var selectedMeasure = await context.Measures.FirstOrDefaultAsync();
            var addProductTypeCommand = new AddProductTypeCommand
            {
                ProductTypeName = "New Product Type",
                AverageShelfLifeDuration = new ExpirationTerm
                {
                    Days = 10,
                    Hours = 5
                },
                MeasureId = selectedMeasure.MeasureId,
                ProductAliases = new List<string>
                {
                    "New Product", "New Type"
                },
                ProductImageId = uploadedImage
            };
            var addProductTypeHandler = new AddProductTypeCommandHandler(context, mapper, memoryCache, shelfLifeInferingService, imageUploadingService);

            var addedProductType = await addProductTypeHandler.Handle(addProductTypeCommand, new CancellationToken());

            Assert.AreEqual(addProductTypeCommand.ProductTypeName, addedProductType.ProductTypeName);
            Assert.AreEqual(addProductTypeCommand.AverageShelfLifeDuration, addedProductType.AverageShelfLifeDuration);
            Assert.AreEqual(addProductTypeCommand.MeasureId, addedProductType.MeasureId);
            Assert.AreEqual(selectedMeasure.Name, addedProductType.MeasureName);
            Assert.AreEqual(addProductTypeCommand.ProductAliases.Count(), addedProductType.ProductAliases.Count());
            Assert.AreEqual("Images/" + addProductTypeCommand.ProductTypeName + ".png", addedProductType.ProductImagePath);
            Assert.IsTrue(await context.ProductTypes.AnyAsync(x => x.ProductTypeId == addedProductType.ProductTypeId));

            foreach (var alias in addProductTypeCommand.ProductAliases)
            {
                Assert.IsTrue(addedProductType.ProductAliases.Contains(alias));
                Assert.IsTrue(await context.ProductAliases.AnyAsync(x => x.Name == alias));
            }
        }

        [TestMethod]
        public async Task AddNewProductTypeWithUnexistentImage()
        {
            var selectedMeasure = await context.Measures.FirstOrDefaultAsync();
            var command = new AddProductTypeCommand
            {
                ProductTypeName = "New Product Type",
                AverageShelfLifeDuration = new ExpirationTerm
                {
                    Days = 10,
                    Hours = 5
                },
                MeasureId = selectedMeasure.MeasureId,
                ProductAliases = new List<string>
                {
                    "New Product", "New Type"
                },
                ProductImageId = Guid.NewGuid()
            };
            var handler = new AddProductTypeCommandHandler(context, mapper, memoryCache, shelfLifeInferingService, imageUploadingService);

            await Assert.ThrowsExceptionAsync<ProductImageNotInCacheException>(async ()
                => await handler.Handle(command, new CancellationToken()));
        }
    }
}
