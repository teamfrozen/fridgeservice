﻿using FridgeService.Domain;
using FridgeService.WebApi.Domain.Fridges.Services;
using FridgeService.WebApi.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FridgeService.UnitTests
{
    [TestClass]
    public class ShelfLifeInferingServiceTests
    {
        private IOptions<ExpirationSettings> expirationSettings;

        [TestInitialize]
        public async Task Initialize()
        {
            expirationSettings = Options.Create(new ExpirationSettings(14));
        }

        [TestMethod]
        public void GetExpirationTermOfNullProduct()
        {
            var shelfLifeInferingService = new ShelfLifeInferingService(expirationSettings);

            Assert.ThrowsException<ArgumentNullException>(() =>
                shelfLifeInferingService.GetExpirationTerm((Product)null));
        }

        [TestMethod]
        public void GetExpirationDateOfNullProduct()
        {
            var shelfLifeInferingService = new ShelfLifeInferingService(expirationSettings);

            Assert.ThrowsException<ArgumentNullException>(() =>
                shelfLifeInferingService.GetExpirationDate((Product)null));
        }

        [TestMethod]
        public void GetExpirationTermOfNullProductType()
        {
            var shelfLifeInferingService = new ShelfLifeInferingService(expirationSettings);

            Assert.ThrowsException<ArgumentNullException>(() =>
                shelfLifeInferingService.GetExpirationTerm((ProductType)null));
        }

        [TestMethod]
        public void GetDefaultExpirationTerm()
        {
            var shelfLifeInferingService = new ShelfLifeInferingService(expirationSettings);

            var defaultExpirationTerm = shelfLifeInferingService.GetDefaultExpirationTerm();

            Assert.AreEqual(expirationSettings.Value.DefaultExpirationTermInDays, defaultExpirationTerm.Days);
        }

        [TestMethod]
        public void GetExpirationTermOfProductWithExpirationDate()
        {
            var shelfLifeInferingService = new ShelfLifeInferingService(expirationSettings);
            var product = new Product { ShelfLifeDuration = TimeSpan.TicksPerDay * 3 + TimeSpan.TicksPerHour * 12 };

            var expirationTerm = shelfLifeInferingService.GetExpirationTerm(product);

            Assert.AreEqual(product.ShelfLifeDuration, expirationTerm.GetTicks());
        }

        [TestMethod]
        public void GetExpirationTermOfProductWithoutExpirationDateWithNullProductType()
        {
            var shelfLifeInferingService = new ShelfLifeInferingService(expirationSettings);
            var product = new Product();

            var expirationTerm = shelfLifeInferingService.GetExpirationTerm(product);

            Assert.AreEqual(expirationSettings.Value.DefaultExpirationTermInDays, expirationTerm.Days);
        }

        [TestMethod]
        public void GetExpirationTermOfProductWithoutExpirationDateWithProductTypeWithoutAverageShelfLife()
        {
            var shelfLifeInferingService = new ShelfLifeInferingService(expirationSettings);
            var product = new Product { ProductType = new ProductType() };

            var expirationTerm = shelfLifeInferingService.GetExpirationTerm(product);

            Assert.AreEqual(expirationSettings.Value.DefaultExpirationTermInDays, expirationTerm.Days);
        }

        [TestMethod]
        public void GetExpirationTermOfProductWithoutExpirationDateWithProductTypeWithAverageShelfLife()
        {
            var shelfLifeInferingService = new ShelfLifeInferingService(expirationSettings);
            var product = new Product { ProductType = new ProductType { AvgShelfLifeDuration = TimeSpan.TicksPerDay * 3 + TimeSpan.TicksPerHour * 12 } };

            var expirationTerm = shelfLifeInferingService.GetExpirationTerm(product);

            Assert.AreEqual(product.ProductType.AvgShelfLifeDuration, expirationTerm.GetTicks());
        }

        [TestMethod]
        public void GetExpirationTermOfProductTypeWithExpirationDate()
        {
            var shelfLifeInferingService = new ShelfLifeInferingService(expirationSettings);
            var productType = new ProductType { AvgShelfLifeDuration = TimeSpan.TicksPerDay * 3 + TimeSpan.TicksPerHour * 12 };

            var expirationTerm = shelfLifeInferingService.GetExpirationTerm(productType);

            Assert.AreEqual(productType.AvgShelfLifeDuration, expirationTerm.GetTicks());
        }

        [TestMethod]
        public void GetExpirationTermOfProductTypeWithoutExpirationDate()
        {
            var shelfLifeInferingService = new ShelfLifeInferingService(expirationSettings);
            var productType = new ProductType();

            var expirationTerm = shelfLifeInferingService.GetExpirationTerm(productType);

            Assert.AreEqual(expirationSettings.Value.DefaultExpirationTermInDays, expirationTerm.Days);
        }
    }
}
