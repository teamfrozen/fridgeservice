﻿using FridgeService.UnitTests.MockClasses;
using FridgeService.WebApi.Domain.Product_Types.Models;
using FridgeService.WebApi.Domain.Product_Types.Services;
using FridgeService.WebApi.Exceptions;
using FridgeService.WebApi.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace FridgeService.UnitTests
{
    [TestClass]
    public class ImageUploadingServiceTests
    {
        private const string DefaultImage = "DefaultImage";
        private IOptions<ProductTypeSettings> productTypeSettings;
        private WebHostEnvironmentMock webHostEnvironmentMock;

        [TestInitialize]
        public async Task Initialize()
        {
            productTypeSettings = Options.Create(new ProductTypeSettings(Guid.Empty, DefaultImage));

            webHostEnvironmentMock = new WebHostEnvironmentMock();
        }

        [TestMethod]
        public async Task UploadNullImage()
        {
            var imageUploadingService = new ImageUploadingService(productTypeSettings, webHostEnvironmentMock);

            var imagePath = await imageUploadingService.UploadProductTypeImage(null, "productTypeName");

            Assert.AreEqual(DefaultImage, imagePath);
        }

        [TestMethod]
        public async Task UploadImageWithNullProductName()
        {
            var imageUploadingService = new ImageUploadingService(productTypeSettings, webHostEnvironmentMock);

            await Assert.ThrowsExceptionAsync<ArgumentEmptyException>(async ()
                => await imageUploadingService.UploadProductTypeImage(new ImageInfo { ImageName = "testimage.png", ImageContents = new byte[1] }, null));
        }

        [TestMethod]
        public async Task UploadImageWithUnsupportedExtension()
        {
            var imageUploadingService = new ImageUploadingService(productTypeSettings, webHostEnvironmentMock);

            await Assert.ThrowsExceptionAsync<IncorrectFileExtensionException>(async ()
                => await imageUploadingService.UploadProductTypeImage(new ImageInfo { ImageName = "testimage.docs", ImageContents = new byte[1] }, "productTypeName"));
        }

        [TestMethod]
        public async Task UploadCorrectImage()
        {
            var imageUploadingService = new ImageUploadingService(productTypeSettings, webHostEnvironmentMock);
            var fileSize = 1213;

            var imagePath = await imageUploadingService.UploadProductTypeImage(new ImageInfo { ImageName = "testimage.png", ImageContents = new byte[fileSize] }, "productTypeName");

            Assert.AreEqual("Images/productTypeName.png", imagePath);
            Assert.AreEqual(fileSize, File.ReadAllBytes(webHostEnvironmentMock.DefaultRootPath + imagePath).Length);
        }

        [TestMethod]
        public async Task UploadUpdatedImage()
        {
            var imageUploadingService = new ImageUploadingService(productTypeSettings, webHostEnvironmentMock);
            var sourceImagePath = await imageUploadingService.UploadProductTypeImage(new ImageInfo { ImageName = "testimage.png", ImageContents = new byte[1] }, "productTypeName");
            var updatedFileSize = 5012;

            var targetImagePath = await imageUploadingService.UploadProductTypeImage(new ImageInfo { ImageName = "testUpdImage.png", ImageContents = new byte[updatedFileSize] }, "productTypeName");

            Assert.AreEqual(sourceImagePath, targetImagePath);
            Assert.AreEqual(updatedFileSize, File.ReadAllBytes(webHostEnvironmentMock.DefaultRootPath + targetImagePath).Length);
        }
    }
}
