﻿using AutoMapper;
using FridgeService.Domain;
using FridgeService.Domain.Context;
using FridgeService.Transport.Commands;
using FridgeService.WebApi;
using FridgeService.WebApi.Domain.Measures.UseCases;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TestSupport.EfHelpers;

namespace FridgeService.UnitTests.MeasureTests
{
    [TestClass]
    public class GetMeasuresTest
    {
        protected AppDbContext context;
        protected IMapper mapper;

        [TestInitialize]
        public async Task Initialize()
        {
            var options = SqliteInMemory.CreateOptions<AppDbContext>();
            context = new AppDbContext(options);
            context.Database.EnsureCreated();

            context.ProductTypes.Add(new ProductType
            {
                AvgShelfLifeDuration = 10368000000000,
                Measure = new Measure("l."),
                Name = "Milk",
                ProductAliases = new List<ProductAlias>
                {
                      new ProductAlias { Name = "Milk" },
                      new ProductAlias { Name = "Dairy" }
                },
                ProductImagePath = "PathToMilkImage",
                Products = new List<Product>()
            });

            var productType = context.ProductTypes.Add(new ProductType
            {
                AvgShelfLifeDuration = 10368000000000,
                Measure = new Measure("шт."),
                Name = "TestProductType",
                ProductAliases = new List<ProductAlias>
                {
                      new ProductAlias { Name = "Test" },
                      new ProductAlias { Name = "ProductType" }
                },
                ProductImagePath = "DefaultPath",
                Products = new List<Product>()
            });

            await context.SaveChangesAsync();

            var mapperConfiguration = new MapperConfiguration(cfg => cfg.AddProfile(new MappingProfile()));
            mapper = new Mapper(mapperConfiguration);
        }

        [TestMethod]
        public async Task GetAllMeasuresTest()
        {
            var allMeasures = await context.Measures
                .AsNoTracking()
                .ToListAsync();

            var getMeasuresCommand = new GetAllMeasuresCommand();
            var handler = new GetAllMeasuresCommandHandler(context, mapper);

            var resultProduct = (await handler.Handle(getMeasuresCommand, new CancellationToken()))
                .ToDictionary(x => x.MeasureId);

            Assert.AreEqual(allMeasures.Count, resultProduct.Count);
            foreach (var measure in allMeasures)
            {
                Assert.IsTrue(resultProduct.ContainsKey(measure.MeasureId));
                Assert.AreEqual(measure.Name, resultProduct[measure.MeasureId].MeasureName);
            }
        }
    }
}
