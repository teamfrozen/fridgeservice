﻿using FridgeService.Transport.Responses;
using MediatR;
using System;
using System.Collections.Generic;

namespace FridgeService.Transport.Commands
{
    public class GetFridgeContentsCommand : IRequest<IEnumerable<ProductInFridgeInfo>>
    {
        public Guid UserId { get; set; }
        public GetFridgeContentsCommand(Guid userId)
        {
            UserId = userId;
        }
    }
}
