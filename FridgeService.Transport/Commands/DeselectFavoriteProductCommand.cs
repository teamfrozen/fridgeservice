﻿using FridgeService.Transport.Responses;
using MediatR;
using System;

namespace FridgeService.Transport.Commands
{
    /// <summary>
    /// Command to remove a product from user favorites
    /// </summary>
    public class DeselectFavoriteProductCommand : IRequest<FavoriteProductShortInfo>
    {
        /// <summary>
        /// Id of the user
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Product Id to remove from user favorites
        /// </summary>
        public Guid ProductId { get; set; }

        public DeselectFavoriteProductCommand(Guid userId, Guid productId)
        {
            UserId = userId;
            ProductId = productId;
        }
    }
}
