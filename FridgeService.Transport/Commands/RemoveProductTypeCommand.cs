﻿using MediatR;
using System;

namespace FridgeService.Transport.Commands
{
    public class RemoveProductTypeCommand : IRequest<Unit>
    {
        public Guid ProductTypeId { get; set; }
        public RemoveProductTypeCommand(Guid productTypeId)
        {
            ProductTypeId = productTypeId;
        }
    }
}
