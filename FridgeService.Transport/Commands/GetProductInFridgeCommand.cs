﻿using FridgeService.Transport.Responses;
using MediatR;
using System;

namespace FridgeService.Transport.Commands
{
    public class GetProductInFridgeCommand : IRequest<ProductInFridgeInfo>
    {
        public Guid UserId { get; set; }
        public Guid ProductInFridgeId { get; set; }
        public GetProductInFridgeCommand(Guid userId, Guid productInFridgeId)
        {
            UserId = userId;
            ProductInFridgeId = productInFridgeId;
        }
    }
}
