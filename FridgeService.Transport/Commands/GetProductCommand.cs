﻿using FridgeService.Transport.Responses;
using MediatR;
using System;

namespace FridgeService.Transport.Commands
{
    public class GetProductCommand : IRequest<ProductDetailedInfo>
    {
        public Guid ProductId { get; set; }
        public GetProductCommand(Guid productId)
        {
            ProductId = productId;
        }
    }
}
