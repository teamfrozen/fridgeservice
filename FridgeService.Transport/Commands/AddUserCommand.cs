﻿using FridgeService.Transport.Responses;
using MediatR;
using System;

namespace FridgeService.Transport.Commands
{
    /// <summary>
    /// Command to add a user
    /// </summary>
    public class AddUserCommand : IRequest<UserInfo>
    {
        /// <summary>
        /// Id of the user
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Id of the existing fridge, if user is added to specific fridge (if null, a new fridge is created for a user)
        /// </summary>
        public Guid? FridgeId { get; set; }
    }
}
