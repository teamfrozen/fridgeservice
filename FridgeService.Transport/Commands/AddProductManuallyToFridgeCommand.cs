﻿using FridgeService.Transport.RequestModels;
using FridgeService.Transport.Responses;
using MediatR;
using System;

namespace FridgeService.Transport.Commands
{
    /// <summary>
    /// It is advised to make it two-path:
    /// 1. If a user chooses a product from the list - in this case, product type is also selected
    /// 2. If a user enteres a product manually - in this case, product type can either be chosen or not chosen
    /// In both cases expiration date can be either selected or not selected
    /// </summary>
    public class AddProductManuallyToFridgeCommand : IRequest<ProductInFridgeInfo>
    {

        /// <summary>
        /// Name of the product to be added (entered by user)
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// Id of a user who adds a product
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Id of the product to be added (may be empty if user enters a name manually and not choosing from the available list)
        /// </summary>
        public Guid? ProductId { get; set; }

        /// <summary>
        /// If of the product type (may be empty if user enters a name manually and not choosing from the available list)
        /// </summary>
        public Guid? ProductTypeId { get; set; }

        /// <summary>
        /// Chosen expiration date of the product (may be empty - will be determined automatically)
        /// </summary>
        public DateTime? ExpirationDate { get; set; }

        /// <summary>
        /// Quantity of the product (in litres, grammes and etc.)
        /// </summary>
        public double? Quantity { get; set; }

        public AddProductManuallyToFridgeCommand(ProductToBeAddedManuallyToFridge productDto, Guid userId)
        {
            ProductId = productDto.ProductId;
            ProductName = productDto.ProductName;
            ProductTypeId = productDto.ProductTypeId;
            ExpirationDate = productDto.ExpirationDate;
            Quantity = productDto.Quantity;
            UserId = userId;
        }
    }
}
