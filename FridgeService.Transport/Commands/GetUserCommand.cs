﻿using FridgeService.Transport.Responses;
using MediatR;
using System;

namespace FridgeService.Transport.Commands
{
    public class GetUserCommand : IRequest<UserInfo>
    {
        public Guid UserId { get; set; }
        public GetUserCommand(Guid userId)
        {
            UserId = userId;
        }
    }
}
