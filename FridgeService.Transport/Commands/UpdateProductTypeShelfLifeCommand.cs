﻿using FridgeService.Transport.Responses;
using MediatR;
using System;

namespace FridgeService.Transport.Commands
{
    /// <summary>
    /// Command to update product type average shelf life
    /// </summary>
    public class UpdateProductTypeShelfLifeCommand : IRequest<ProductTypeDetailedInfo>
    {
        /// <summary>
        /// Id of the product type
        /// </summary>
        public Guid ProductTypeId { get; set; }

        /// <summary>
        /// New (updated) average shelf life duration of the product type
        /// </summary>
        public ExpirationTerm AverageShelfLifeDuration { get; set; }

        public UpdateProductTypeShelfLifeCommand(Guid productTypeId, ExpirationTerm avgShelfLifeDuration)
        {
            ProductTypeId = productTypeId;
            AverageShelfLifeDuration = avgShelfLifeDuration;
        }
    }
}
