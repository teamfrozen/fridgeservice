﻿using FridgeService.Transport.Responses;
using MediatR;
using System;

namespace FridgeService.Transport.Commands
{
    public class GetFridgeByUserCommand : IRequest<FridgeInfo>
    {
        public Guid UserId { get; set; }
        public GetFridgeByUserCommand(Guid userId)
        {
            UserId = userId;
        }
    }
}
