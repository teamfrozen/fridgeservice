﻿using FridgeService.Transport.Responses;
using MediatR;
using System;

namespace FridgeService.Transport.Commands
{
    public class AddProductCommand : IRequest<ProductDetailedInfo>
    {
        /// <summary>
        /// Name of the product
        /// </summary>
        public string ProductName { get; set; }
        /// <summary>
        /// Quantity of a product (in litres, grammes, etc.)
        /// </summary>
        public double? Quantity { get; set; }
        /// <summary>
        /// Shelf life of the product
        /// </summary>
        public ExpirationTerm? ShelfLifeDuration { get; set; }

        /// <summary>
        /// Id of a product type
        /// </summary>
        public Guid? ProductTypeId { get; set; }
    }
}
