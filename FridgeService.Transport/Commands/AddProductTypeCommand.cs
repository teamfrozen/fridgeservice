﻿using FridgeService.Transport.Responses;
using MediatR;
using System;
using System.Collections.Generic;

namespace FridgeService.Transport.Commands
{
    /// <summary>
    /// Command to update product type
    /// </summary>
    public class AddProductTypeCommand : IRequest<ProductTypeDetailedInfo>
    {
        /// <summary>
        /// Name of the product type
        /// </summary>
        public string ProductTypeName { get; set; }

        /// <summary>
        /// Id of the product image previously uploaded
        /// </summary>
        public Guid? ProductImageId { get; set; }

        /// <summary>
        /// Product aliases for the product type (if some intersect with existing product types, throws corresponding exception)
        /// </summary>
        public IEnumerable<string> ProductAliases { get; set; }

        /// <summary>
        /// Average shelf life duration for all products of this type (if empty, default is set)
        /// </summary>
        public ExpirationTerm? AverageShelfLifeDuration { get; set; }

        /// <summary>
        /// Name of measure of the product type
        /// </summary>
        public string MeasureName { get; set; }

        /// <summary>
        /// Id of the measure is chosen from the list (null if measure name is manually typed)
        /// </summary>
        public Guid? MeasureId { get; set; }
    }
}
