﻿using MediatR;
using System;

namespace FridgeService.Transport.Commands
{
    public class RemoveProductFromFridgeCommand : IRequest<Unit>
    {
        public Guid ProductInFridgeId { get; set; }
        public Guid UserId { get; set; }

        public RemoveProductFromFridgeCommand(Guid productInFridgeId, Guid userId)
        {
            ProductInFridgeId = productInFridgeId;
            UserId = userId;
        }
    }
}
