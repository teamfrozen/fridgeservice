﻿using FridgeService.Transport.Responses;
using MediatR;
using System;

namespace FridgeService.Transport.Commands
{
    /// <summary>
    /// Command to select a product to be favorite for a user
    /// </summary>
    public class SelectFavoriteProductCommand : IRequest<FavoriteProductShortInfo>
    {
        /// <summary>
        /// Id of the user
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Id of the product to be favorite
        /// </summary>
        public Guid ProductId { get; set; }

        public SelectFavoriteProductCommand(Guid userId, Guid productId)
        {
            UserId = userId;
            ProductId = productId;
        }
    }
}
