﻿using MediatR;
using Microsoft.AspNetCore.Http;
using System;

namespace FridgeService.Transport.Commands
{
    public class UploadProductImageCommand : IRequest<Guid>
    {
        public IFormFile ProductTypeImage { get; set; }
        public UploadProductImageCommand(IFormFile image)
        {
            ProductTypeImage = image;
        }
    }
}
