﻿using FridgeService.Transport.Responses;
using MediatR;
using System;

namespace FridgeService.Transport.Commands
{
    public class GetProductTypeCommand : IRequest<ProductTypeDetailedInfo>
    {
        public Guid ProductTypeId { get; set; }
        public GetProductTypeCommand(Guid productTypeId)
        {
            ProductTypeId = productTypeId;
        }
    }
}
