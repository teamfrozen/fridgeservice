﻿using FridgeService.Transport.Responses;
using MediatR;
using System.Collections.Generic;

namespace FridgeService.Transport.Commands
{
    public class GetAllProductTypesCommand : IRequest<IEnumerable<ProductTypeShortInfo>>
    {
    }
}
