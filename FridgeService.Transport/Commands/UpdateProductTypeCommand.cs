﻿using FridgeService.Transport.Responses;
using MediatR;
using System;
using System.Collections.Generic;

namespace FridgeService.Transport.Commands
{
    public class UpdateProductTypeCommand : IRequest<ProductTypeDetailedInfo>
    {
        public Guid ProductTypeId { get; set; }
        public Guid? ProductImageId { get; set; }
        public string ProductTypeName { get; set; }
        public IEnumerable<string> ProductAliases { get; set; }
        public ExpirationTerm? AverageShelfLifeDuration { get; set; }
        public string MeasureName { get; set; }
        public Guid? MeasureId { get; set; }
    }
}
