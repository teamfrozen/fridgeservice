﻿using FridgeService.Transport.Responses;
using MediatR;
using System;
using System.Collections.Generic;

namespace FridgeService.Transport.Commands
{
    public class GetUserOutOfStockFavoritesCommand : IRequest<IEnumerable<FavoriteProductDetailedInfo>>
    {
        public Guid UserId { get; set; }
        public GetUserOutOfStockFavoritesCommand(Guid userId)
        {
            UserId = userId;
        }
    }
}
