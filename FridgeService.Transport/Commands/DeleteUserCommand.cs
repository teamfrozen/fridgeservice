﻿using MediatR;
using System;

namespace FridgeService.Transport.Commands
{
    public class RemoveUserCommand : IRequest<Unit>
    {
        public Guid UserId { get; set; }

        public RemoveUserCommand(Guid userId)
        {
            UserId = userId;
        }
    }
}
