﻿using FridgeService.Transport.Responses;
using MediatR;
using System;

namespace FridgeService.Transport.Commands
{
    /// <summary>
    /// Command to update fridge product expiration date
    /// </summary>
    public class UpdateFridgeProductExpirationDateCommand : IRequest<ProductInFridgeInfo>
    {
        /// <summary>
        /// Id of the product in the fridge
        /// </summary>
        public Guid ProductInFridgeId { get; set; }

        /// <summary>
        /// New (updated) expiration date of the product
        /// </summary>
        public DateTime TargetExpirationDate { get; set; }

        /// <summary>
        /// Id of the user who performs an action
        /// </summary>
        public Guid UserId { get; set; }

        public UpdateFridgeProductExpirationDateCommand(Guid userId, Guid productInFridgeId, DateTime targetExpirationDate)
        {
            UserId = userId;
            ProductInFridgeId = productInFridgeId;
            TargetExpirationDate = targetExpirationDate;
        }
    }
}
