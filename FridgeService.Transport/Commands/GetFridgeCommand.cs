﻿using FridgeService.Transport.Responses;
using MediatR;
using System;

namespace FridgeService.Transport.Commands
{
    public class GetFridgeCommand : IRequest<FridgeInfo>
    {
        public Guid FridgeId { get; set; }
        public GetFridgeCommand(Guid fridgeId)
        {
            FridgeId = fridgeId;
        }
    }
}
