﻿using FridgeService.Transport.Responses;
using MediatR;
using System;
using System.Collections.Generic;

namespace FridgeService.Transport.Commands
{
    public class GetProductsCommand : IRequest<IEnumerable<ProductShortInfo>>
    {
        public Guid? ProductTypeId { get; set; }
        public GetProductsCommand()
        {

        }
        public GetProductsCommand(Guid? productTypeId)
        {
            ProductTypeId = productTypeId;
        }
    }
}
