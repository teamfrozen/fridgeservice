﻿using FridgeService.Transport.Responses;
using MediatR;
using System;
using System.Collections.Generic;

namespace FridgeService.Transport.Commands
{
    public class GetUserFavoritesCommand : IRequest<IEnumerable<FavoriteProductDetailedInfo>>
    {
        public Guid UserId { get; set; }
        public GetUserFavoritesCommand(Guid userId)
        {
            UserId = userId;
        }
    }
}
