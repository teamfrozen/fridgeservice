﻿using FridgeService.Transport.Responses;
using MediatR;
using System;

namespace FridgeService.Transport.Commands
{
    public class BindUserToExistingFridgeCommand : IRequest<FridgeInfo>
    {
        public Guid UserId { get; set; }
        public Guid FridgeId { get; set; }
    }
}
