﻿using MediatR;
using System;

namespace FridgeService.Transport.Commands
{
    public class RemoveProductCommand : IRequest<Unit>
    {
        public Guid ProductId { get; set; }

        public RemoveProductCommand(Guid productId)
        {
            ProductId = productId;
        }
    }
}
