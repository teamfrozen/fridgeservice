﻿using FridgeService.Transport.Responses;
using MediatR;
using System;

namespace FridgeService.Transport.Commands
{
    public class BindUserToNewFridgeCommand : IRequest<FridgeInfo>
    {
        public Guid UserId { get; set; }
        public BindUserToNewFridgeCommand(Guid userId)
        {
            UserId = userId;
        }
    }
}
