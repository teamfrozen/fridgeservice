﻿using FridgeService.Transport.RequestModels;
using FridgeService.Transport.Responses;
using MediatR;
using System;
using System.Collections.Generic;

namespace FridgeService.Transport.Commands
{
    public class AddProductFromReceiptToFridgeCommand : IRequest<ResultOfAddingProductsFromReceipt>
    {
        /// <summary>
        /// Products to be added
        /// </summary>
        public ICollection<ProductToBeAddedFromReceiptToFridge> ProductsToBeAdded { get; set; }

        /// <summary>
        /// Id of a user who adds a product
        /// </summary>
        public Guid UserId { get; set; }

        public AddProductFromReceiptToFridgeCommand(ICollection<ProductToBeAddedFromReceiptToFridge> productsInfo, Guid userId)
        {
            ProductsToBeAdded = productsInfo;
            UserId = userId;
        }
    }
}
