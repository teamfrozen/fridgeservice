﻿using System;

namespace FridgeService.Transport.Responses
{
    public class ProductDetailedInfo
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        /// <summary>
        /// Quantity of a product (in litres, grammes, etc.)
        /// </summary>
        public double? Quantity { get; set; }

        /// <summary>
        /// Shelf life of the product
        /// </summary>
        public ExpirationTerm? ShelfLifeDuration { get; set; }

        /// <summary>
        /// Id of a product type
        /// </summary>
        public Guid? ProductTypeId { get; set; }

        /// <summary>
        /// Name of a product type
        /// </summary>
        public string ProductTypeName { get; set; }

        /// <summary>
        /// Image of a product type of the product
        /// </summary>
        public string ProductImagePath { get; set; }

        /// <summary>
        /// Measure name of a product type
        /// </summary>
        public string MeasureName { get; set; }
        public Guid? MeasureId { get; set; }
    }
}
