﻿using System;

namespace FridgeService.Transport.Responses
{
    /// <summary>
    /// Information about the product in fridge
    /// </summary>
    public class ProductInFridgeInfo
    {
        /// <summary>
        /// Id of a product in the fridge
        /// </summary>
        public Guid ProductInFridgeId { get; set; }

        /// <summary>
        /// Date of deposit a product into the fridge
        /// </summary>
        public DateTime DepositDate { get; set; }

        /// <summary>
        /// Expiration date of a product in the fridge
        /// </summary>
        public DateTime ExpirationDate { get; set; }

        /// <summary>
        /// Name of the product
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// Id of a product
        /// </summary>
        public Guid ProductId { get; set; }

        /// <summary>
        /// Quantity of a product (in litres, grammes, etc.)
        /// </summary>
        public double? Quantity { get; set; }

        /// <summary>
        /// Id of a product type
        /// </summary>
        public Guid? ProductTypeId { get; set; }

        /// <summary>
        /// Name of a product type
        /// </summary>
        public string ProductTypeName { get; set; }

        /// <summary>
        /// Image of a product type of the product
        /// </summary>
        public string ProductImagePath { get; set; }

        /// <summary>
        /// Measure name of a product type
        /// </summary>
        public string MeasureName { get; set; }

        /// <summary>
        /// Flag showing if a product is favorite for a user
        /// </summary>
        public bool IsFavorite { get; set; }
    }
}
