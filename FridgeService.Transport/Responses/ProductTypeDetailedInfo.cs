﻿using System;
using System.Collections.Generic;

namespace FridgeService.Transport.Responses
{
    /// <summary>
    /// Detailed information about the product type
    /// </summary>
    public class ProductTypeDetailedInfo
    {
        /// <summary>
        /// Id of the product type
        /// </summary>
        public Guid ProductTypeId { get; set; }

        /// <summary>
        /// Name of the product type
        /// </summary>
        public string ProductTypeName { get; set; }

        /// <summary>
        /// Average shelf life duration of the product type
        /// </summary>
        public ExpirationTerm? AverageShelfLifeDuration { get; set; }

        /// <summary>
        /// Id of measure, if measure is set
        /// </summary>
        public Guid? MeasureId { get; set; }

        /// <summary>
        /// Measure name
        /// </summary>
        public string MeasureName { get; set; }

        /// <summary>
        /// URL to get product image - contains only path in terms of service (it is needed to add a prefix of service address to get image)
        /// </summary>
        public string ProductImagePath { get; set; }

        /// <summary>
        /// List of all product type aliases
        /// </summary>
        public IEnumerable<string> ProductAliases { get; set; }
    }
}
