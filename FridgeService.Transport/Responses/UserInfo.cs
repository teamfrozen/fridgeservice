﻿using System;

namespace FridgeService.Transport.Responses
{
    public class UserInfo
    {
        public Guid UserId { get; set; }
        public Guid FridgeId { get; set; }
    }
}
