﻿using System;

namespace FridgeService.Transport.Responses
{
    /// <summary>
    /// Short information about a measure
    /// </summary>
    public class MeasureShortInfo
    {
        /// <summary>
        /// Id of the measure
        /// </summary>
        public Guid MeasureId { get; set; }

        /// <summary>
        /// Name of the measure
        /// </summary>
        public string MeasureName { get; set; }
    }
}
