﻿using System;

namespace FridgeService.Transport.Responses
{
    /// <summary>
    /// Short info about a product
    /// </summary>
    public class ProductShortInfo
    {

        /// <summary>
        /// Id of the product
        /// </summary>
        public Guid ProductId { get; set; }

        /// <summary>
        /// Name of the product
        /// </summary>
        public string ProductName { get; set; }
    }
}
