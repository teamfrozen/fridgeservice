﻿using System.Collections.Generic;

namespace FridgeService.Transport.Responses
{
    public class ResultOfAddingProductsFromReceipt
    {
        public IList<ProductInFridgeInfo> AddedProducts { get; set; }
        public IList<string> Errors { get; set; }
    }
}
