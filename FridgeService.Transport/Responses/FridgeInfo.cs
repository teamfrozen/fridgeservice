﻿using System;
using System.Collections.Generic;

namespace FridgeService.Transport.Responses
{
    public class FridgeInfo
    {
        public Guid FridgeId { get; set; }
        public IEnumerable<Guid> Users { get; set; }
    }
}
