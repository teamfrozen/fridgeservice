﻿using System;

namespace FridgeService.Transport.Responses
{
    /// <summary>
    /// Short info about a product type
    /// </summary>
    public class ProductTypeShortInfo
    {
        /// <summary>
        /// Id of a product type
        /// </summary>
        public Guid ProductTypeId { get; set; }

        /// <summary>
        /// Name of the product type
        /// </summary>
        public string ProductTypeName { get; set; }
    }
}
