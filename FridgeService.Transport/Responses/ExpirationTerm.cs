﻿using System;

namespace FridgeService.Transport.Responses
{
    public struct ExpirationTerm
    {
        /// <summary>
        /// Represents the number of ticks in 1 day. This field is constant.
        /// </summary>
        public const long TicksPerDay = 864000000000;

        /// <summary>
        /// Represents the number of ticks in 1 hour. This field is constant.
        /// </summary>
        public const long TicksPerHour = 36000000000;

        public long Days { get; set; }
        public long Hours { get; set; }
        public long GetTicks() => Days * TicksPerDay + Hours * TicksPerHour;

        public ExpirationTerm(int days, int hours)
        {
            if (days < 0 || hours < 0)
            {
                throw new ArgumentException("Expiration term has to be positive");
            }

            Days = days;
            Hours = hours;
        }

        public ExpirationTerm(long ticks)
        {
            if (ticks < 0)
            {
                throw new ArgumentException("Expiration term has to be positive");
            }

            Days = ticks / TicksPerDay;
            Hours = (ticks % TicksPerDay) / TicksPerHour;
        }
    }
}
