﻿using System;

namespace FridgeService.Transport.Responses
{
    /// <summary>
    /// Information about the current state of a product favoriteness to a user
    /// </summary>
    public class FavoriteProductShortInfo
    {
        /// <summary>
        /// Id of the product
        /// </summary>
        public Guid ProductId { get; set; }

        /// <summary>
        /// Flag - is a product favorite for a user
        /// </summary>
        public bool IsFavorite { get; set; }
        public FavoriteProductShortInfo()
        {

        }
        public FavoriteProductShortInfo(Guid productId, bool isFavorite)
        {
            ProductId = productId;
            IsFavorite = isFavorite;
        }
    }
}
