﻿using System;

namespace FridgeService.Transport.RequestModels
{
    public class ProductToBeAddedFromReceiptToFridge
    {
        /// <summary>
        /// Name of the product to be added (entered by user)
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// Chosen expiration date of the product (may be empty - will be determined automatically)
        /// </summary>
        public DateTime? ExpirationDate { get; set; }

        /// <summary>
        /// Quantity of the product (in litres, grammes and etc.)
        /// </summary>
        public double? Quantity { get; set; }
    }
}
