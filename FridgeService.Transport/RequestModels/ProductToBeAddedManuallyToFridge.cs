﻿using System;

namespace FridgeService.Transport.RequestModels
{
    public class ProductToBeAddedManuallyToFridge
    {
        /// <summary>
        /// Name of the product to be added (entered by user)
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// Id of the product to be added (may be empty if user enters a name manually and not choosing from the available list)
        /// </summary>
        public Guid? ProductId { get; set; }

        /// <summary>
        /// If of the product type (may be empty if user enters a name manually and not choosing from the available list)
        /// </summary>
        public Guid? ProductTypeId { get; set; }

        /// <summary>
        /// Chosen expiration date of the product (may be empty - will be determined automatically)
        /// </summary>
        public DateTime? ExpirationDate { get; set; }

        /// <summary>
        /// Quantity of the product (in litres, grammes and etc.)
        /// </summary>
        public double? Quantity { get; set; }
    }
}
