USE [master]
GO
/****** Object:  Database [FridgeDb]    Script Date: 05.11.2021 17:38:55 ******/
CREATE DATABASE [FridgeDb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'FridgeDb', FILENAME = N'/var/opt/mssql/data/FridgeDb.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'FridgeDb_log', FILENAME = N'/var/opt/mssql/data/FridgeDb_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [FridgeDb] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [FridgeDb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [FridgeDb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [FridgeDb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [FridgeDb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [FridgeDb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [FridgeDb] SET ARITHABORT OFF 
GO
ALTER DATABASE [FridgeDb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [FridgeDb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [FridgeDb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [FridgeDb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [FridgeDb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [FridgeDb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [FridgeDb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [FridgeDb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [FridgeDb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [FridgeDb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [FridgeDb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [FridgeDb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [FridgeDb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [FridgeDb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [FridgeDb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [FridgeDb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [FridgeDb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [FridgeDb] SET RECOVERY FULL 
GO
ALTER DATABASE [FridgeDb] SET  MULTI_USER 
GO
ALTER DATABASE [FridgeDb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [FridgeDb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [FridgeDb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [FridgeDb] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [FridgeDb] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [FridgeDb] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'FridgeDb', N'ON'
GO
ALTER DATABASE [FridgeDb] SET QUERY_STORE = OFF
GO
USE [FridgeDb]
GO
/****** Object:  User [smart-fridge]    Script Date: 05.11.2021 17:38:55 ******/
CREATE LOGIN smart_fridge WITH PASSWORD = N'smart1_fridge2_pass3', DEFAULT_DATABASE=[FridgeDb]
GO
    CREATE USER smart_fridge FOR LOGIN smart_fridge
GO
ALTER ROLE [db_owner] ADD MEMBER smart_fridge
GO
ALTER ROLE [db_accessadmin] ADD MEMBER smart_fridge
GO
ALTER ROLE [db_securityadmin] ADD MEMBER smart_fridge
GO
ALTER ROLE [db_ddladmin] ADD MEMBER smart_fridge
GO
ALTER ROLE [db_backupoperator] ADD MEMBER smart_fridge
GO
ALTER ROLE [db_datareader] ADD MEMBER smart_fridge
GO
ALTER ROLE [db_datawriter] ADD MEMBER smart_fridge
GO
ALTER SERVER ROLE sysadmin ADD MEMBER smart_fridge
GO
/****** Object:  Table [dbo].[Favorite_Product]    Script Date: 05.11.2021 17:38:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Favorite_Product](
	[user_product_id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[user_id] [uniqueidentifier] NOT NULL,
	[product_id] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_User_Product] PRIMARY KEY CLUSTERED 
(
	[user_product_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Fridge]    Script Date: 05.11.2021 17:38:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Fridge](
	[fridge_id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
 CONSTRAINT [PK_Fridge] PRIMARY KEY CLUSTERED 
(
	[fridge_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Measure]    Script Date: 05.11.2021 17:38:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Measure](
	[measure_id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[name] [nvarchar](40) NOT NULL,
 CONSTRAINT [PK_Unit_Of_Measure] PRIMARY KEY CLUSTERED 
(
	[measure_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 05.11.2021 17:38:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[product_id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[product_type_id] [uniqueidentifier] NULL,
	[quantity] [float] NULL,
	[shelf_life_duration] [bigint] NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[product_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product_Alias]    Script Date: 05.11.2021 17:38:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product_Alias](
	[alias_id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[product_type_id] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Product_Alias] PRIMARY KEY CLUSTERED 
(
	[alias_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product_In_Fridge]    Script Date: 05.11.2021 17:38:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product_In_Fridge](
	[product_in_fridge_id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[fridge_id] [uniqueidentifier] NOT NULL,
	[product_id] [uniqueidentifier] NOT NULL,
	[deposit_date] [datetime] NOT NULL,
	[expiration_date] [datetime] NOT NULL,
 CONSTRAINT [PK_Product_In_Fridge] PRIMARY KEY CLUSTERED 
(
	[product_in_fridge_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product_Type]    Script Date: 05.11.2021 17:38:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product_Type](
	[product_type_id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[name] [nvarchar](80) NOT NULL,
	[avg_shelf_life_duration] [bigint] NULL,
	[measure_id] [uniqueidentifier] NULL,
	[product_image_path] [nvarchar](max) NULL,
 CONSTRAINT [PK_Product_Type] PRIMARY KEY CLUSTERED 
(
	[product_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 05.11.2021 17:38:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[user_id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[fridge_id] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Favorite_Product] ADD  CONSTRAINT [DF_User_Product_user_product_id]  DEFAULT (newid()) FOR [user_product_id]
GO
ALTER TABLE [dbo].[Fridge] ADD  CONSTRAINT [DF_Fridge_fridge_id]  DEFAULT (newid()) FOR [fridge_id]
GO
ALTER TABLE [dbo].[Measure] ADD  CONSTRAINT [DF_Measure_measure_id]  DEFAULT (newid()) FOR [measure_id]
GO
ALTER TABLE [dbo].[Product] ADD  CONSTRAINT [DF_Product_product_id]  DEFAULT (newid()) FOR [product_id]
GO
ALTER TABLE [dbo].[Product_Alias] ADD  CONSTRAINT [DF_Product_Alias_alias_id]  DEFAULT (newid()) FOR [alias_id]
GO
ALTER TABLE [dbo].[Product_In_Fridge] ADD  CONSTRAINT [DF_Product_In_Fridge_product_in_fridge_id]  DEFAULT (newid()) FOR [product_in_fridge_id]
GO
ALTER TABLE [dbo].[Product_Type] ADD  CONSTRAINT [DF_Product_Type_product_type_id]  DEFAULT (newid()) FOR [product_type_id]
GO
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_user_id]  DEFAULT (newid()) FOR [user_id]
GO
ALTER TABLE [dbo].[Favorite_Product]  WITH CHECK ADD  CONSTRAINT [FK_User_Product_Product] FOREIGN KEY([product_id])
REFERENCES [dbo].[Product] ([product_id])
GO
ALTER TABLE [dbo].[Favorite_Product] CHECK CONSTRAINT [FK_User_Product_Product]
GO
ALTER TABLE [dbo].[Favorite_Product]  WITH CHECK ADD  CONSTRAINT [FK_User_Product_User] FOREIGN KEY([user_id])
REFERENCES [dbo].[User] ([user_id])
GO
ALTER TABLE [dbo].[Favorite_Product] CHECK CONSTRAINT [FK_User_Product_User]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Product_Type] FOREIGN KEY([product_type_id])
REFERENCES [dbo].[Product_Type] ([product_type_id])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Product_Type]
GO
ALTER TABLE [dbo].[Product_Alias]  WITH CHECK ADD  CONSTRAINT [FK_Product_Alias_Product_Type] FOREIGN KEY([product_type_id])
REFERENCES [dbo].[Product_Type] ([product_type_id])
GO
ALTER TABLE [dbo].[Product_Alias] CHECK CONSTRAINT [FK_Product_Alias_Product_Type]
GO
ALTER TABLE [dbo].[Product_In_Fridge]  WITH CHECK ADD  CONSTRAINT [FK_Product_In_Fridge_Fridge] FOREIGN KEY([fridge_id])
REFERENCES [dbo].[Fridge] ([fridge_id])
GO
ALTER TABLE [dbo].[Product_In_Fridge] CHECK CONSTRAINT [FK_Product_In_Fridge_Fridge]
GO
ALTER TABLE [dbo].[Product_In_Fridge]  WITH CHECK ADD  CONSTRAINT [FK_Product_In_Fridge_Product] FOREIGN KEY([product_id])
REFERENCES [dbo].[Product] ([product_id])
GO
ALTER TABLE [dbo].[Product_In_Fridge] CHECK CONSTRAINT [FK_Product_In_Fridge_Product]
GO
ALTER TABLE [dbo].[Product_Type]  WITH CHECK ADD  CONSTRAINT [FK_Product_Type_Unit_Of_Measure] FOREIGN KEY([measure_id])
REFERENCES [dbo].[Measure] ([measure_id])
GO
ALTER TABLE [dbo].[Product_Type] CHECK CONSTRAINT [FK_Product_Type_Unit_Of_Measure]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Fridge] FOREIGN KEY([fridge_id])
REFERENCES [dbo].[Fridge] ([fridge_id])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Fridge]
GO
BEGIN
   IF NOT EXISTS (SELECT * FROM dbo.Product_Type
                   WHERE product_type_id = '7da51d9e-e352-4712-94f4-574a5166910f')
   BEGIN
       INSERT INTO dbo.Product_Type (product_type_id, name, avg_shelf_life_duration, product_image_path)
       VALUES ('7da51d9e-e352-4712-94f4-574a5166910f', 'Undefined', 10368000000000, 'api/Images/DefaultImage.png')
   END
END
GO
USE [master]
GO
ALTER DATABASE [FridgeDb] SET  READ_WRITE 
GO
